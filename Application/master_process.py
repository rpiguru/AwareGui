# -*- coding: iso8859-15 -*-
import multiprocessing
from global_config import configdata
import subprocess


class MasterProcess(multiprocessing.Process):

    def run(self):
        while True:
            kivy_proc = subprocess.Popen(['python3', 'app.py'])
            stdout, stderr = kivy_proc.communicate()
            kivy_proc.wait()

            p = subprocess.Popen(['chromium-browser', '--kiosk', '-no-sandbox', configdata['dashboard_url']],
                                 stdout=subprocess.PIPE, stderr=subprocess.PIPE)
            output, error = p.communicate()
            p.wait()


if __name__ == '__main__':

    mp = MasterProcess()

    mp.start()

    mp.join()
