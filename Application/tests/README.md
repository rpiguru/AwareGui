# How to test Kivy app
=======================================================================

( refer: https://github.com/KeyWeeUsr/KivyUnitTest )

- Create a custom testing module in this directory and name it as you like.

- Inherit `TestAwareGui` class and create a test function with the same name of the file like this:

        def test_<module_name>(self):
            logger.debug('Starting AwareGui tester(<module_name>)...')
            self.app = AwareHubApp()
            Clock.schedule_once(self.run_test, 0.000001)
            self.app.run()

    `TestAwareGui` class is in `base.py`

- Create `custom_test` function and add tasks as you like.

- Enjoy!