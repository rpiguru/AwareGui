import sys
import os

appdir = os.path.abspath(os.path.join(os.path.abspath(os.path.dirname(__file__)), os.pardir))

if appdir not in sys.path:
    sys.path.append(appdir)

from tests.base import *


class TestWeather(TestAwareGui):

    def test_weather(self):
        logger.debug('Starting AwareGui tester (Weather)...')
        self.app = AwareHubApp()
        Clock.schedule_once(self.run_test, 0.000001)
        self.app.run()

    def custom_test(self):
        self.add_task(target=partial(self.t_go_to_screen, 'weather'), delay=1)
        self.add_task(target=self.t_check_weather, delay=1, loop=True, timeout=60)
        self.add_task(target=self.stop_test, delay=1)

    @task
    def t_check_weather(self, timeout=20, *args):
        """
        This task is iterative task
        :param args:
        :return:
        """
        if self.app.cur_screen_inst.success:
            logger.debug('Successfully got weather data.')
            return False  # Cancel this looping schedule
        else:
            if time.time() - self.start_time < timeout:
                return True  # Continue schedule
            else:
                return False


if __name__ == '__main__':
    unittest.main()
