import sys
import os

appdir = os.path.abspath(os.path.join(os.path.abspath(os.path.dirname(__file__)), os.pardir))

if appdir not in sys.path:
    sys.path.append(appdir)

from tests.base import *


class TestHubState(TestAwareGui):

    # exit_after_test = False

    def test_hub_state(self):
        logger.debug('Starting AwareGui tester (Hub Status)...')
        self.app = AwareHubApp()
        Clock.schedule_once(self.run_test, 0.000001)
        self.app.run()

    def custom_test(self):
        self.add_task(target=partial(self.t_go_to_screen, 'hub_state'), delay=1)


if __name__ == '__main__':
    unittest.main()
