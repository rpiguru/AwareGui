import sys
import os

appdir = os.path.abspath(os.path.join(os.path.abspath(os.path.dirname(__file__)), os.pardir))

if appdir not in sys.path:
    sys.path.append(appdir)

from tests.base import *


class TestSensorState(TestAwareGui):

    def test_sensor_state(self):
        logger.debug('Starting AwareGui tester (Sensor State)...')
        self.app = AwareHubApp()
        Clock.schedule_once(self.run_test, 0.000001)
        self.app.run()

    def custom_test(self):
        self.add_task(target=partial(self.t_go_to_screen, 'sensor_state'), delay=1)


if __name__ == '__main__':
    unittest.main()
