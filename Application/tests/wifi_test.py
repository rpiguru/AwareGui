import sys
import os

appdir = os.path.abspath(os.path.join(os.path.abspath(os.path.dirname(__file__)), os.pardir))

if appdir not in sys.path:
    sys.path.append(appdir)

from tests.base import *
from global_config import configdata
import utils.net
from kivy.core.window import Window


class TestWiFi(TestAwareGui):

    def test_wifi_test(self):
        logger.debug('Starting AwareGui tester (WiFi)...')
        self.app = AwareHubApp()
        Clock.schedule_once(self.run_test, 0.000001)
        self.app.run()

    def custom_test(self):
        ssid, pwd = configdata['test_wifi_ssid'], configdata['test_wifi_pwd']
        self.add_task(target=partial(self.t_go_to_screen, 'settings'), delay=.5)  # Move to `settings` page
        self.add_task(target=partial(self.t_go_to_settings_tab, 'wifi'), delay=.5)  # Move to `wifi` tab
        self.add_task(target=partial(self.t_open_wifi_dlg, ssid), delay=3)
        self.add_task(target=partial(self.t_connect_wifi, pwd), delay=3)
        self.add_task(target=partial(self.t_check_wifi), delay=1, loop=True, timeout=30)

    @task
    def t_open_wifi_dlg(self, ssid='', *args):
        """
        Open wifi connecting dialog
        :param ssid:
        :param args:
        :return:
        """
        cur_tab = self.app.cur_screen_inst.cur_tab
        try:
            self.assertEqual('wifi', cur_tab.name, 'Current tab is not `wifi` tab')
        except AssertionError:
            self.verificationErrors.append(traceback.format_exc(limit=15))

        try:
            if cur_tab.ap_list is not None:
                self.assertIn(ssid, [ap['ssid'] for ap in cur_tab.ap_list], 'Not found destination SSID')
            else:
                self.verificationErrors.append('Error, failed to get surrounding WiFi HotSpots...')
        except AssertionError:
            self.verificationErrors.append(traceback.format_exc(limit=15))

        ap, mac = utils.net.get_current_ap()
        if ssid == ap:
            logger.debug('Already connected to {}'.format(ssid))
        else:
            logger.debug('Trying to connect to {}'.format(ssid))
            for child in cur_tab.ids.ml_right.children + cur_tab.ids.ml_left.children:
                if child.id == ssid:
                    child.dispatch('on_release')
                    break

    @task
    def t_connect_wifi(self, pwd='', *args):
        cur_tab = self.app.cur_screen_inst.cur_tab
        if cur_tab.popup:
            cur_tab.popup.ids.txt_pwd.text = pwd
            cur_tab.popup.ids.btn_yes.dispatch('on_release')

    @task
    def t_check_wifi(self, timeout=30, *args):
        """
        This task is iterative
        :param timeout:
        :param args:
        :return:
        """
        if self.app.running_popup:
            self.app.running_popup.dismiss()            # Close wifi info popup
        Window.release_all_keyboards()                  # Close `dock` keyboard
        cur_tab = self.app.cur_screen_inst.cur_tab
        if cur_tab.ip is not None:
            logger.debug('Successfully connect to SSID')
            return False
        else:
            if time.time() - self.start_time < timeout:
                return True
            else:
                return False


if __name__ == '__main__':
    unittest.main()
