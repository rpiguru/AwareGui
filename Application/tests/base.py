# -*- coding: iso8859-15 -*-

"""
    Unit test module of AwareGui App. ( refer: https://github.com/KeyWeeUsr/KivyUnitTest )

    Usage:
        - Create a custom testing module in this directory and name it as you like.
        - Inherit `TestAwareGui` class and create a test function with the same name of the file like this:
            def test_<module_name>(self):
               logger.debug('Starting AwareGui tester(<module_name>)...')
               self.app = AwareHubApp()
               Clock.schedule_once(self.run_test, 0.000001)
               self.app.run()
        - Create `custom_test` function and add tasks as you like.
        - Enjoy!

"""

import os
import unittest
import time
from abc import abstractmethod
from functools import partial
import sys
from kivy.clock import Clock
from app import AwareHubApp
from global_config import logger
import queue
import traceback


appdir = os.path.abspath(os.path.dirname(__file__))
if appdir not in sys.path:
    sys.path.append(appdir)


def task(func):
    def wrapper(self, *args, **kwargs):
        """
        After calling original function, set `is_running = False` to execute next task
        If a task returns None, `is_running` flag will be set to `False` and next task will be executed.
        Otherwise, nothing will be executed any more.
        """
        ret_val = func(self, *args, **kwargs)
        if not ret_val:
            self.is_running = False
        return ret_val
    return wrapper


class TestAwareGui(unittest.TestCase):

    app = None
    is_running = False
    q = queue.Queue()

    start_time = time.time()

    exit_after_test = True

    beginning_time = time.time()

    def setUp(self):
        self.verificationErrors = []

    # Copy this function to the destination class and rename it as `test_<module name>`
    # def test_awaregui(self):
    #     logger.debug('Starting AwareGui tester...')
    #     self.app = AwareHubApp()
    #     Clock.schedule_once(self.run_test, 0.000001)
    #     self.app.run()

    def run_test(self, *args):
        """
        Start main testing loop after testing some initial stuff..
        :param args:
        :return:
        """
        Clock.schedule_interval(self.pause, 0.000001)

        # Check if current screen is dashboard
        try:
            self.assertEqual('dashboard_screen', self.app.cur_screen_inst.name)
        except AssertionError:
            self.verificationErrors.append(traceback.format_exc(limit=15))

        self.custom_test()

        if self.exit_after_test:
            self.add_task(target=self.stop_test, delay=3)

    @abstractmethod
    def custom_test(self):
        raise NotImplemented('`custom_test` function should be re-declared.')

    def stop_test(self, *args):
        try:
            self.assertEqual([], self.verificationErrors)
            logger.info('Elapsed: {} sec\n'.format(int(time.time() - self.beginning_time)))
            logger.info('\n\n========================== GREAT! Test passed successfully! ===========================')
        except AssertionError:
            for er in self.verificationErrors:
                logger.error(er)
        self.app.stop()

    # sleep function that catches `dt` from Clock
    def pause(self, *args):
        """
        This will pause the main loop of AwareGui app, and execute tasks from the queue
        :param args:
        :return:
        """
        time.sleep(0.000001)
        if not self.is_running:
            if self.q.qsize() > 0:
                self.is_running = True
                target, delay, loop, timeout = self.q.get()
                if loop:    # Looping task.
                    self.start_time = time.time()   # Initialize start time for timeout
                    Clock.schedule_interval(partial(target, timeout), 2)
                else:
                    Clock.schedule_once(target, delay)

    def add_task(self, target=None, delay=.5, loop=False, timeout=20):
        """
        Add task to the queue.

        NOTE: When adding target function, use `partial` instead of using its function name directly!
            This will work:
                self.add_task(target=partial(self.t_my_task), delay=1)
            This will not work:
                self.add_task(target=self.t_my_task, delay=1)

        :param target:
        :return:
        """
        self.q.put((target, delay, loop, timeout))

    # =========================== Basic tasks that will be used commonly. ========================================

    @task
    def t_go_to_screen(self, screen_name, *args):
        """
        Move to a screen from the dashboard screen
        :param screen_name: destination screen name
        :param args:
        :return:
        """
        cur_screen_name = self.app.cur_screen_inst.name

        try:
            self.assertNotEqual(cur_screen_name, screen_name, 'Already in the screen: {}'.format(screen_name))
        except AssertionError:
            self.verificationErrors.append(traceback.format_exc(limit=15))

        if cur_screen_name == 'dashboard_screen':
            self.app.cur_screen_inst.ids['card_{}'.format(screen_name)].dispatch('on_press')
        else:
            self.app.root.ids.btn_back.dispatch('on_release')

    @task
    def t_go_to_settings_tab(self, tab_name, *args):
        """
        Move to the given tab in settings screen
        :param tab_name:
        :param args:
        :return:
        """
        cur_scr = self.app.cur_screen_inst
        try:
            self.assertEqual('settings_screen', cur_scr.name, 'This can be called in settings tab only')
        except AssertionError:
            self.verificationErrors.append(traceback.format_exc(limit=15))

        cur_scr.ids['btn_{}'.format(tab_name)].dispatch('on_release')


if __name__ == '__main__':
    unittest.main()
