import sys
import os

appdir = os.path.abspath(os.path.join(os.path.abspath(os.path.dirname(__file__)), os.pardir))

if appdir not in sys.path:
    sys.path.append(appdir)

from tests.base import *
from utils.store import set_config_val


class TestJSONData(TestAwareGui):

    def test_json_data(self):
        logger.debug('Starting AwareGui tester (JSON data from Backend)...')
        self.app = AwareHubApp()
        Clock.schedule_once(self.run_test, 0.000001)
        set_config_val('debug', True)
        self.app.run()

    def custom_test(self):
        self.add_task(target=partial(self.t_go_to_screen, 'sensor_state'), delay=1)
        self.add_task(target=partial(self.t_check_sensor_data), delay=1)

    @task
    def t_check_sensor_data(self, *args):
        cur_scr = self.app.cur_screen_inst
        try:
            self.assertIsNotNone(cur_scr.sensor_data, msg='Failed to get sensor data from backend')
        except AssertionError:
            self.verificationErrors.append(traceback.format_exc(limit=15))


if __name__ == '__main__':
    unittest.main()

