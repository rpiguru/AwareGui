# -*- coding: iso8859-15 -*-

from kivy.uix.screenmanager import ScreenManager, NoTransition
from screens.aware_service.screen import AwareServiceScreen
from screens.base.base import BaseScreen
from screens.clock.screen import ClockScreen
from screens.dashboard.screen import DashboardScreen
from screens.error_screen.screen import ErrorScreen
from screens.hub_status.screen import HubStatusScreen
from screens.sensor_state.screen import SensorStateScreen
from screens.settings.screen import SettingsScreen
from screens.solaire_fitness.screen import SolaireFitnessScreen
from screens.weather.screen import WeatherScreen
from screens.welcome.screen import WelcomeScreen

sm = ScreenManager(transition=NoTransition())

screens = {
    'welcome_screen': WelcomeScreen,
    'dashboard_screen': DashboardScreen,
    'solaire_fitness_screen': SolaireFitnessScreen,
    'aware_service_screen': AwareServiceScreen,
    'weather_screen': WeatherScreen,
    'settings_screen': SettingsScreen,
    'clock_screen': ClockScreen,
    'hub_status_screen': HubStatusScreen,
    'sensor_state_screen': SensorStateScreen,
    'blank_screen': BaseScreen,
    'error_screen': ErrorScreen,
}
