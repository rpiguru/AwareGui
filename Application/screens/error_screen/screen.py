# -*- coding: iso8859-15 -*-

import os
from kivy.lang import Builder
from screens.base.base import BaseScreen

Builder.load_file(os.path.join(os.path.dirname(__file__), 'error.kv'))


class ErrorScreen(BaseScreen):

    show_back_button = True

    def on_enter(self, *args):
        error_msg = self.get_app().get_exception()
        self.ids.error_msg.text = str(error_msg)
        super(ErrorScreen, self).on_enter(*args)
