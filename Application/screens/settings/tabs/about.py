# -*- coding: iso8859-15 -*-

import os
from kivy.lang import Builder
from screens.settings.tabs.base import BaseTab
from utils.common import get_app_note

Builder.load_file(os.path.join(os.path.dirname(__file__), 'kv', 'about.kv'))


class AboutTab(BaseTab):

    index = 5

    def on_enter_tab(self, *args):
        self.ids.app_note.text = get_app_note()
