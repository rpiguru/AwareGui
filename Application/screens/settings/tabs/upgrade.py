# -*- coding: iso8859-15 -*-

import os
import utils.common
from kivy.lang import Builder
from kivy.properties import StringProperty

from screens.settings.tabs.base import BaseTab

Builder.load_file(os.path.join(os.path.dirname(__file__), 'kv', 'upgrade.kv'))


class UpgradeTab(BaseTab):

    current_version = StringProperty('')
    new_version = StringProperty('')
    index = 4

    def __init__(self, **kwargs):
        super(UpgradeTab, self).__init__(**kwargs)
        self.current_version = utils.common.get_git_revision()
        self.new_version = utils.common.get_new_version()
        self.ids.updated.text = 'Updated: {}'.format(utils.common.get_git_commit_date())

    def on_upgrade(self):
        # aware_logger.info(
        #     "Upgrading current version({}) to new version({})".format(self.current_version, self.new_version))
        pass

    def on_enter_tab(self, *args):
        pass
