# -*- coding: iso8859-15 -*-

import os
import threading
from functools import partial

from kivy.app import App
from kivy.clock import Clock, mainthread
from kivymd.snackbar import Snackbar
import utils.common
from kivy.lang import Builder
from screens.settings.tabs.base import BaseTab
from utils.system import authenticate_user
from widgets.dialog import PasswordDialog, LoadingDialog, NotificationDialog

Builder.load_file(os.path.join(os.path.dirname(__file__), 'kv', 'general.kv'))


class GeneralTab(BaseTab):

    index = 0
    loading_dlg = LoadingDialog()

    def __init__(self, **kwargs):
        super(GeneralTab, self).__init__(**kwargs)

    def on_enter_tab(self, *args):
        self.ids.aware_version.text = utils.common.get_git_revision()
        self.ids.aware_name.text = utils.common.get_aware_name()
        self.ids.hostname.text = utils.common.get_hostname()

    def on_apply(self):
        new_name = self.ids.aware_name.text
        if len(new_name) == 0:
            Snackbar(text='Invalid Aware Name.', background_color=(.8, 0, .3, .5)).show()
            return
        utils.common.set_aware_name(new_name)
        App.get_running_app().update_dev_name()
        Snackbar(text='Aware Name is updated.').show()

    def on_btn_close(self):
        dlg = PasswordDialog()
        dlg.bind(on_confirm=self.on_confirm_pwd)
        dlg.open()

    def on_confirm_pwd(self, *args):
        pwd = args[1]
        self.loading_dlg.open()
        threading.Thread(target=self.check_pwd, args=(pwd,)).start()

    def check_pwd(self, pwd, *args):
        result = authenticate_user(pwd)
        Clock.schedule_once(partial(self.on_finish_checking, result))

    @mainthread
    def on_finish_checking(self, result, *args):
        self.loading_dlg.dismiss()
        if result:
            App.get_running_app().stop()
        else:
            NotificationDialog(message='Invalid Password', title='').open()
