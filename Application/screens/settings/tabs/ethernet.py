# -*- coding: iso8859-15 -*-

import os
import threading
from kivy.lang import Builder
from kivymd.snackbar import Snackbar
from kivy.clock import Clock
import utils.net
from kivy.properties import StringProperty
from screens.settings.tabs.base import BaseTab
from widgets.dialog import LoadingDialog
from widgets.input import AwareIPMDTextField
from functools import partial

Builder.load_file(os.path.join(os.path.dirname(__file__), 'kv', 'ethernet.kv'))

label_dict = {
    'addr': 'IP Address',
    'netmask': 'Netmask',
    'gateway': 'Gateway',
    'broadcast': 'Broadcast',
    'network': 'Network'
}


class EthernetTab(BaseTab):

    cur_ip = StringProperty('')
    mode = StringProperty('dhcp')
    index = 1

    loading_dlg = LoadingDialog()

    def on_enter_tab(self, *args):
        data = utils.net.get_eth_settings()
        if data:
            for _key in ['addr', 'netmask', 'gateway', 'broadcast', 'network']:
                wid = AwareIPMDTextField(use_dlg=True)
                wid.hint_text = label_dict[_key]
                wid.set_value(data[_key])
                self.ids[_key] = wid
                self.ids.input_container.add_widget(wid)
            self.change_mode()

    def change_mode(self):
        self.mode = self.ids.mode.get_value()['mode']
        for _key in label_dict.keys():
            self.ids[_key].disabled = True if self.mode == 'dhcp' else False

    def on_btn_set(self):
        data = self.collect_data()
        self.loading_dlg.open()
        threading.Thread(target=self.apply_settings, args=(data, )).start()

    def apply_settings(self, data, *args):
        if data['mode'] == 'dhcp':
            result = utils.net.set_eth_settings(mode='dhcp')
        else:
            for _key in ['addr', 'netmask', 'gateway', 'broadcast', 'network']:
                data[_key] = self.ids[_key].text
            result = utils.net.set_eth_settings(mode='static', ip=data['addr'], netmask=data['netmask'],
                                                gateway=data['gateway'], broadcast=data['broadcast'],
                                                network=data['network'])
        Clock.schedule_once(partial(self.on_finish, result))

    def on_finish(self, result, *args):
        self.loading_dlg.dismiss()
        if result is not None:
            Snackbar(text='Ethernet setting is applied, new IP: {}'.format(result)).show()
        else:
            Snackbar(text='Failed to apply, please try again.', background_color=(.8, 0, .3, .5)).show()
