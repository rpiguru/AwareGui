# -*- coding: iso8859-15 -*-
import os
import threading
from functools import partial
import time
from global_config import logger
from kivy.lang import Builder
from kivy.properties import StringProperty
from kivy.uix.widget import Widget
from kivymd.card import MDSeparator
from kivymd.demo import AvatarSampleWidget
from screens.settings.tabs.base import BaseTab
from kivy.clock import Clock, mainthread
import utils.net
from widgets.dialog import WiFiInfoDialog, WiFiConnectDialog, YesNoDialog
from widgets.wifi.wifi_item import WiFiItem

Builder.load_file(os.path.join(os.path.dirname(__file__), 'kv', 'wifi.kv'))


class WiFiTab(BaseTab):

    index = 2
    cur_ssid = StringProperty('')
    ap_list = None
    done = False
    ip = None
    popup = None
    tr_scan = None
    _stop = threading.Event()

    def __init__(self, **kwargs):
        super(WiFiTab, self).__init__(**kwargs)
        self.tr_scan = threading.Thread(target=self.get_ap_list)

    def on_enter_tab(self, *args):
        self.done = False
        threading.Thread(target=self.start_background_threads).start()
        super(WiFiTab, self).on_enter_tab(*args)

    def start_background_threads(self, *args):
        self._stop.clear()
        try:
            self.tr_scan.join()
        except RuntimeError:
            pass
        self.tr_scan.start()

    def get_ap_list(self, *args):
        while not self._stop.isSet():
            ap_list = utils.net.get_ap_list('wlan0')
            cur_ap_name, mac = utils.net.get_current_ap()

            # Move current AP to the beginning of the list
            if cur_ap_name is not None:
                try:
                    cur_ap = [ap for ap in ap_list if ap['ssid'] == cur_ap_name and ap['address'] == mac][0]
                    ap_list.remove(cur_ap)
                except IndexError:
                    cur_ap_name = None
                    cur_ap = None
            else:
                cur_ap = None

            ap_list = sorted(ap_list, key=lambda k: k['quality'])
            ap_list.reverse()

            if cur_ap_name is not None:
                ap_list = [cur_ap, ] + ap_list
                self.cur_ssid = cur_ap_name
            else:
                self.cur_ssid = ''
            self.ap_list = ap_list
            Clock.schedule_once(partial(self.update_ap_list, cur_ap_name))
            time.sleep(10)

    @mainthread
    def update_ap_list(self, cur_ap_name, *args):
        self.ids.ml_left.clear_widgets()
        self.ids.ml_right.clear_widgets()
        saved_ap_data = utils.net.get_saved_aps()
        for i in range(len(self.ap_list)):
            ap = self.ap_list[i]
            txt_ssid = ap['ssid']
            if txt_ssid == cur_ap_name:
                txt_ssid += '   -   [b][i]Connected[/i][/b]'
            try:
                item = WiFiItem(text=txt_ssid, id=txt_ssid, saved=True if txt_ssid in saved_ap_data.keys() else False,
                                secondary_text=ap['address'] + (' (encrypted)' if ap['encrypted'] else ''),
                                signal_percentage=ap['quality'])
                item.bind(on_forget=partial(self.on_open_forget_dlg, ap['ssid']))
                item.bind(on_release=partial(self.on_btn, ap['ssid']))
                if i % 2 == 0:
                    self.ids.ml_left.add_widget(item)
                    self.ids.ml_left.add_widget(MDSeparator(height=1))
                else:
                    self.ids.ml_right.add_widget(item)
                    self.ids.ml_right.add_widget(MDSeparator(height=1))
            except ValueError:
                pass
        self.ids.box_scroll.height = self.ids.ml_left.height
        if len(self.ap_list) % 2 == 1:
            self.ids.ml_right.add_widget(Widget(size_hint_y=None, height=60))
        self.ids.ly_root.remove_widget(self.ids.ly_loading)
        self.ip = utils.net.get_ip_address('wlan0')
        self.done = True

    def on_btn(self, ssid, *args):
        if self.cur_ssid == ssid:
            self.popup = WiFiInfoDialog()
        else:
            self.ip = None
            saved_ap_data = utils.net.get_saved_aps()
            pwd = saved_ap_data[ssid] if ssid in saved_ap_data.keys() else ''
            self.popup = WiFiConnectDialog(ssid=ssid, pwd=pwd)
            self.popup.bind(on_done=self.on_perform_connect)
        self.popup.open()

    def on_perform_connect(self, *args):
        self.ip = args[1]
        logger.info('New IP: {}'.format(self.ip))
        threading.Thread(target=self.get_ap_list).start()

    def on_open_forget_dlg(self, ssid, *args):
        dlg = YesNoDialog(message='Really forget {}?'.format(ssid))
        dlg.bind(on_confirm=partial(self.forget_ssid, ssid))
        dlg.open()

    def forget_ssid(self, ssid, *args):
        args[0].dismiss()
        utils.net.forget_ap_on_file(ssid)
        self.on_enter_tab()

    def on_pre_leave(self, *args):
        self._stop.set()
        super(WiFiTab, self).on_pre_leave(*args)
