# -*- coding: iso8859-15 -*-

import os
import threading
import time

from aware_api.common import check_sensor_status
from kivy.lang import Builder
from screens.base.base import BaseScreen
from kivy.clock import mainthread, Clock
from utils.net import check_internet_connection

Builder.load_file(os.path.join(os.path.dirname(__file__), 'dashboard.kv'))


class DashboardScreen(BaseScreen):

    show_back_button = False
    tr_internet = None
    tr_sensor = None
    _stop = threading.Event()
    has_internet = None
    sensor_state = {'on': None, 'off': None}

    def __init__(self, **kwargs):
        super(DashboardScreen, self).__init__(**kwargs)
        self.tr_internet = threading.Thread(target=self.check_internet)
        self.tr_sensor = threading.Thread(target=self.start_sensing)

    def on_enter(self, *args):
        threading.Thread(target=self.start_background_threads).start()
        super(DashboardScreen, self).on_enter(*args)

    def start_background_threads(self, *args):
        self._stop.clear()
        try:
            self.tr_internet.join()
        except RuntimeError:
            pass
        try:
            self.tr_sensor.join()
        except RuntimeError:
            pass
        self.tr_internet.start()
        self.tr_sensor.start()

    def check_internet(self, *args):
        while not self._stop.isSet():
            internet_connected = check_internet_connection()
            # Update dashboard only when connection state is changed.
            if internet_connected != self.has_internet:
                self.has_internet = internet_connected
                Clock.schedule_once(self.update_internet_status)
            time.sleep(5)

    @mainthread
    def update_internet_status(self, *args):
        state = self.has_internet
        self.ids.img_loading.opacity = 0
        self.ids.img_internet.opacity = 1
        self.ids.img_internet.source = 'assets/images/internet_{}.png'.format('on' if state else 'off')
        self.ids.lb_internet.text = 'Internet: ' + 'ON' if state else 'OFF'
        self.ids.lb_internet.color = (0, 1, 0, 1) if state else (1, 0, 0, 1)

    def start_sensing(self, *args):
        while not self._stop.isSet():
            self.check_sensor_data()
            time.sleep(60)

    def check_sensor_data(self):
        sensor_state = check_sensor_status()
        # print('Sensor status: {}'.format(sensor_state))
        if sensor_state != self.sensor_state and sensor_state is not None:
            self.sensor_state = sensor_state
            Clock.schedule_once(self.update_sensor_state)

    @mainthread
    def update_sensor_state(self, *args):
        self.ids.lb_sensor_on.text = str(self.sensor_state['on'])
        self.ids.lb_sensor_off.text = str(self.sensor_state['off'])

    def on_pre_leave(self, *args):
        self._stop.set()
        super(DashboardScreen, self).on_pre_leave(*args)

    def go_to_hub_status(self):
        self.get_app().has_internet = self.has_internet
        self.switch_screen('hub_status_screen', 'left')
