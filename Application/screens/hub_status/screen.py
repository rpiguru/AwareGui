# -*- coding: iso8859-15 -*-

import os
import threading
import time
import global_config
from utils.common import get_git_commit_date, get_git_revision
from kivy.clock import Clock, mainthread
from kivy.lang import Builder
from screens.base.base import BaseScreen
from utils.net import check_internet_connection, get_external_ip
from utils.net import get_traffic
from utils.system import get_cpu_usage, get_cpu_temperature_c, get_disk_usage, get_mem, get_platform, get_uptime, \
    get_cpu_temperature_f

Builder.load_file(os.path.join(os.path.dirname(__file__), 'hub_status.kv'))


class HubStatusScreen(BaseScreen):
    show_back_button = True
    tr_internet = None
    tr_ip = None
    tr_traffic = None
    _stop = threading.Event()
    has_internet = None
    ip = None
    traffic = None

    def __init__(self, **kwargs):
        super(HubStatusScreen, self).__init__(**kwargs)
        self.tr_internet = threading.Thread(target=self.check_internet)
        self.tr_ip = threading.Thread(target=self.check_ip)
        self.tr_traffic = threading.Thread(target=self.check_traffic)
        self.has_internet = self.get_app().has_internet

    def on_enter(self, *args):
        threading.Thread(target=self.start_background_threads).start()
        Clock.schedule_once(self.update_internet_status)
        super(HubStatusScreen, self).on_enter(*args)
        Clock.schedule_once(self._update_gauge)
        Clock.schedule_interval(self._update_gauge, 2)
        self.update_version_info()

    def start_background_threads(self, *args):
        self._stop.clear()
        try:
            self.tr_internet.join()
        except RuntimeError:
            pass
        try:
            self.tr_ip.join()
        except RuntimeError:
            pass
        try:
            self.tr_traffic.join()
        except RuntimeError:
            pass
        self.tr_internet.start()
        self.tr_traffic.start()
        self.tr_ip.start()

    def check_internet(self, *args):
        while not self._stop.isSet():
            internet_connected = check_internet_connection()
            # Update dashboard only when connection state is changed.
            if internet_connected != self.has_internet:
                self.has_internet = internet_connected
                Clock.schedule_once(self.update_internet_status)
            time.sleep(5)

    def check_ip(self, *args):
        while not self._stop.isSet():
            self.ip = get_external_ip()
            Clock.schedule_once(self.update_ip_address_widget)
            time.sleep(60)

    def check_traffic(self, *args):
        while not self._stop.isSet():
            self.traffic = get_traffic()
            Clock.schedule_once(self.update_network_speed)
            time.sleep(2)

    @mainthread
    def update_internet_status(self, *args):
        if self.has_internet:
            self.ids.internet_status.text = 'Internet: ON'
            self.ids.internet_status.color = [0, .8, 0, .7]
        else:
            self.ids.internet_status.text = 'Internet: OFF'
            self.ids.internet_status.color = [1, 0, 0, .7]

    @mainthread
    def update_ip_address_widget(self, *args):
        if self.ip is not None:
            self.ids.ip_address.text = str(self.ip)
        else:
            self.ids.ip_address.text = str('Not available')

    @mainthread
    def update_network_speed(self, *args):
        if self.traffic is not None:
            self.ids.upload.text = self.traffic['upload']
            self.ids.download.text = self.traffic['download']

    def on_pre_leave(self, *args):
        self._stop.set()
        Clock.unschedule(self.update_ip_address_widget)
        Clock.unschedule(self._update_gauge)
        super(HubStatusScreen, self).on_pre_leave(*args)

    def go_network_settings(self):
        self.go_to_settings('wifi')

    def _update_gauge(self, *args):

        plt = get_platform()
        self.ids.name.text = plt['hostname']
        self.ids.os.text = plt['osname']
        self.ids.kernel.text = plt['kernel']
        self.ids.uptime.text = get_uptime()

        cpu = get_cpu_usage()
        if cpu is not None:
            self.ids.gauge_cpu.value = cpu['used']
            self.ids.cpu_all.text = str(cpu['all'])
            self.ids.cpu_used.text = '{} %'.format(cpu['used'])
            self.ids.cpu_free.text = str(cpu['free'])

        cpu_temp = get_cpu_temperature_c()
        if cpu_temp > 100:
            cpu_temp = 100
        self.ids.gauge_cpu_temp.value = cpu_temp
        self.ids.cpu_temp.text = '{} �C / {} �F'.format(cpu_temp, get_cpu_temperature_f())

        disk_usage = get_disk_usage()
        self.ids.gauge_disk.value = disk_usage['use']
        self.ids.disk_size.text = disk_usage['size']
        self.ids.disk_used.text = disk_usage['used']
        self.ids.disk_avail.text = disk_usage['available']

        mem_usage = get_mem()
        self.ids.gauge_mem.value = mem_usage['percent']
        self.ids.mem_total.text = '{} MB'.format(round(mem_usage['total'], 2))
        self.ids.mem_used.text = '{} MB'.format(round(mem_usage['used'], 2))
        self.ids.mem_free.text = '{} MB'.format(round(mem_usage['free'], 2))

    def update_version_info(self):
        self.ids.version.text = '[i]{}[/i]'.format(' '.join(get_git_revision()))
        self.ids.updated.text = '[i]{}[/i]'.format(get_git_commit_date())
        # self.ids.aware_service.text = '[i]{}[/i]'.format(global_config.version)
        self.ids.aware_service.text = '[i][/i]'
