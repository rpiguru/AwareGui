# -*- coding: iso8859-15 -*-
import os
from kivy.lang import Builder
from screens.base.base import BaseScreen
import aware_api.common

Builder.load_file(os.path.join(os.path.dirname(__file__), 'aware_service.kv'))


class AwareServiceScreen(BaseScreen):

    def on_enter(self, *args):
        super(AwareServiceScreen, self).on_enter(*args)
        self.ids.s_service.active = aware_api.common.get_status()

    def on_switch(self, switch):
        if switch.active:
            aware_api.common.enable_service()
        else:
            aware_api.common.stop_service()
