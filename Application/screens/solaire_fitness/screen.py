# -*- coding: iso8859-15 -*-

import os
from kivy.lang import Builder
from kivy.uix.scrollview import ScrollView
from screens.base.base import BaseScreen

Builder.load_file(os.path.join(os.path.dirname(__file__), 'solaire_fitness.kv'))


class SolaireFitnessScreen(BaseScreen):

    def on_enter(self, *args):
        """
        Clear all previously used data
        :param args:
        :return:
        """
        super(SolaireFitnessScreen, self).on_enter(*args)


class SolaireLayout(ScrollView):

    def __init__(self, **kwargs):
        super(SolaireLayout, self).__init__(**kwargs)
