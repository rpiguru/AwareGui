# -*- coding: iso8859-15 -*-

import os
from kivy.lang import Builder
from screens.base.base import BaseScreen
from kivy.clock import Clock
import utils.time_util
from utils.common import number_to_ordinal

Builder.load_file(os.path.join(os.path.dirname(__file__), 'clock.kv'))


class ClockScreen(BaseScreen):

    def on_enter(self, *args):
        super(ClockScreen, self).on_enter(*args)
        # self.ids.clock.start_clock()
        self.show_clock()
        Clock.schedule_interval(self.show_clock, 1)

    def show_clock(self, *args):
        local_time = utils.time_util.get_local_time()
        self.ids.txt_am_pm.text = local_time.strftime("%p")
        self.ids.txt_time.text = local_time.strftime("%I:%M:%S")
        self.ids.txt_day.text = local_time.strftime("%A")
        self.ids.txt_date.text = local_time.strftime("{} of %B".format(number_to_ordinal(local_time.day)))

    def on_pre_leave(self, *args):
        Clock.unschedule(self.show_clock)
        super(ClockScreen, self).on_pre_leave(*args)
        # self.ids.clock.stop_clock()

    def go_datetime_settings(self):
        self.go_to_settings('datetime')
