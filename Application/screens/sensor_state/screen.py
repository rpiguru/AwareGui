# -*- coding: iso8859-15 -*-

from global_config import configdata
import os
import threading
import time
from aware_api.common import get_last_sensor_data, get_all_sensor_types, add_unit_to_sensor_value, \
    get_dev_by_address, get_device_names, check_connectivity, get_all_group_names, get_devices_by_group
from kivy.clock import Clock, mainthread
from kivy.lang import Builder
from kivy.properties import StringProperty, BooleanProperty
from kivy.uix.boxlayout import BoxLayout
from kivymd.card import MDSeparator
from screens.base.base import BaseScreen
from utils.time_util import format_time
from widgets.dialog import MultiSelectDialog
from widgets.label import H5, H6
from functools import partial

Builder.load_file(os.path.join(os.path.dirname(__file__), 'sensor_state.kv'))


class SensorStateScreen(BaseScreen):

    sensor_data = None
    tr_sensing = None
    _stop = threading.Event()

    selected_devices = {}
    selected_metrics = []

    def __init__(self, **kwargs):
        super(SensorStateScreen, self).__init__(**kwargs)
        self.tr_sensing = threading.Thread(target=self.poll_sensor_data)

    def on_enter(self, *args):
        Clock.schedule_once(self.start_polling_sensor_data)

    def start_polling_sensor_data(self, *args):
        self._stop.clear()
        try:
            self.tr_sensing.join()
        except RuntimeError:
            pass
        self.tr_sensing.start()

    def poll_sensor_data(self, *args):
        while not self._stop.isSet():
            self.sensor_data = get_last_sensor_data()
            Clock.schedule_once(self._update_view)
            time.sleep(configdata['sensor_check_interval'])

    @mainthread
    def _update_view(self, *args):
        """
        Update `Devices` & `Metrics` spinner widgets
        :param args:
        :return:
        """
        if self.sensor_data:
            # Retrieve all device names and update `Devices` spinner
            cur_dev = self.ids.dev_filter.get_value()['dev_filter']
            dev_vals = get_device_names(self.sensor_data, self.ids.conn_filter.get_current_value())
            group_dict = get_all_group_names(self.sensor_data)
            self.ids.dev_filter.order = ['all', 'all_selected', ] + list(group_dict.keys()) + sorted(dev_vals, key=dev_vals.get)
            dev_vals.update(all='All Devices', all_selected='Selected Devices')
            dev_vals.update(group_dict)
            self.ids.dev_filter.set_values(dev_vals)
            if cur_dev in dev_vals.keys():   # Restore the old value only when it is in the new values
                self.ids.dev_filter.update_current_value(dev_vals[cur_dev])
            else:
                self.ids.dev_filter.update_current_value('All Devices')
            # Remove the dev name if it is not in the new values
            new_devs = {k: dev_vals[k] for k in self.selected_devices if k in dev_vals.keys()}
            self.selected_devices = new_devs

            # Retrieve all metric names and update `Metrics` spinner
            cur_met = self.ids.metric_filter.get_current_value()
            met_vals = ['All Metrics', 'Selected Metrics'] + get_all_sensor_types(self.sensor_data)
            self.ids.metric_filter.order = met_vals
            self.ids.metric_filter.set_values(met_vals)
            if cur_met in met_vals:
                self.ids.metric_filter.update_current_value(cur_met)
            # Remove the metric if it is not in the new values
            new_mets = [m for m in self.selected_metrics if m in met_vals]
            self.selected_metrics = new_mets
            self.on_change_dev_filter(False)
            self.on_change_metric_filter(update_metrics=False)
        else:
            self.show_loading(False)

    def _update_container(self):
        self.show_loading()
        loc_wid = self.ids.ly_dev_name
        loc_wid.clear_widgets()
        loc_wid.height = 5

        title_wid = self.ids.ly_metric
        title_wid.clear_widgets()
        title_wid.width = 0
        for met in self.selected_metrics:
            wid = H5(text=met, size_hint=(None, None), size=(len(met) * 10 + 30, 50),
                     halign='center', valign='middle')
            title_wid.add_widget(wid)
            # title_wid.add_widget(MDSeparator(orientation='vertical', size_hint=(None, None), size=(1, 60)))
            title_wid.width += wid.width

        wid = H5(text='Last Seen', size_hint=(None, None), size=(180, 50), halign='center', valign='middle')
        title_wid.add_widget(wid)
        title_wid.width += wid.width

        met_wid = self.ids.ly_detail
        met_wid.clear_widgets()
        met_wid.size = (title_wid.width, 0)
        for addr in sorted(self.selected_devices, key=self.selected_devices.get):
            dev_detail = get_dev_by_address(self.sensor_data, addr)
            if dev_detail:
                connected = check_connectivity(dev_detail['last_seen'])
                loc = DeviceNameWidget(device_name=self.selected_devices[addr],
                                       address=dev_detail['source_address'], connected=connected)
                loc_wid.add_widget(loc)
                loc_wid.add_widget(MDSeparator(height=1))
                loc_wid.height += (loc.height + 1)

                box = BoxLayout(size_hint=(None, None), size=(title_wid.width, 50))
                for met in self.selected_metrics:
                    if met in dev_detail.keys():
                        val = dev_detail[met]
                        if type(val) == list:
                            val = ','.join([str(i) for i in val[:-1]]) if len(val) > 1 else str(val[0])
                        sensor_val = add_unit_to_sensor_value(met, val)
                    else:
                        sensor_val = 'N/A'
                    _wid = H6(text=str(sensor_val), size_hint=(None, None), size=(len(met) * 10 + 30, 50),
                              halign='center', valign='middle')
                    # box.add_widget(MDSeparator(orientation='vertical', size_hint=(None, None), size=(1, 50)))
                    box.add_widget(_wid)
                str_last_seen = format_time(dev_detail['last_seen'])
                box.add_widget(H6(text=str_last_seen, size_hint=(None, None), size=(180, 50),
                                  halign='center', valign='middle'))
                met_wid.add_widget(box)
                met_wid.add_widget(MDSeparator(height=1))
                met_wid.height += (loc.height + 1)

        Clock.schedule_once(partial(self.show_loading, False), .5)

    def on_pre_leave(self, *args):
        self._stop.set()
        super(SensorStateScreen, self).on_pre_leave(*args)

    def on_change_dev_filter(self, update_container=True):
        dev_val = self.ids.dev_filter.get_value()['dev_filter']
        if dev_val == 'all':
            self.selected_devices = get_device_names(self.sensor_data, self.ids.conn_filter.get_current_value())
        elif dev_val == 'all_selected':
            if update_container:
                all_devs = get_device_names(self.sensor_data, self.ids.conn_filter.get_current_value())
                dlg = MultiSelectDialog(choices=all_devs, title='Select devices you want to see')
                dlg.bind(on_finished=self.on_selected_devices)
                dlg.open()
                return          # Exit to update after selecting devices in the dialog
        else:
            if dev_val in get_all_group_names(self.sensor_data).keys():
                self.selected_devices = get_devices_by_group(self.sensor_data, dev_val)
            else:
                self.selected_devices = {dev_val: self.ids.dev_filter.get_current_value()}
        if update_container:
            self._update_container()

    def on_selected_devices(self, *args):
        self.selected_devices = args[1]
        self._update_container()

    def on_change_metric_filter(self, update_container=True, update_metrics=True):
        metric_val = self.ids.metric_filter.get_current_value()
        if metric_val == 'All Metrics':
            self.selected_metrics = get_all_sensor_types(self.sensor_data)
        elif metric_val == 'Selected Metrics':
            if update_metrics:
                all_metrics = {k: k for k in get_all_sensor_types(self.sensor_data)}
                dlg = MultiSelectDialog(choices=all_metrics, title='Select metrics you want to see')
                dlg.bind(on_finished=self.on_selected_metrics)
                dlg.open()
                return              # Exit to update after selecting devices in the dialog
        else:
            self.selected_metrics = [metric_val, ]
        if update_container:
            self._update_container()

    def on_selected_metrics(self, *args):
        self.selected_metrics = args[1].keys()
        self._update_container()

    def on_change_connection_filter(self):
        self._update_view()

    def show_loading(self, val=True, *args):
        self.ids.main_box.opacity = 0 if val else 1
        self.ids.loading_indicator.opacity = 1 if val else 0


class DeviceNameWidget(BoxLayout):

    address = StringProperty('')
    device_name = StringProperty('')
    connected = BooleanProperty(True)
