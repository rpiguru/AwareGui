# -*- coding: iso8859-15 -*-
import sys, os
import random
import pprint
from pymongo import MongoClient

appdir = os.path.abspath(os.path.dirname(__file__))
if appdir not in sys.path:
    sys.path.append(appdir)

try:
    from global_config import configdata
except ImportError:
    sys.path.append(os.path.abspath(os.path.join(appdir, os.pardir)))
    from global_config import configdata


mongo_client = MongoClient(configdata['mongo_host'], configdata['mongo_port'], connect=False)
db_sensor_data = mongo_client['sensor_data']
recent_sensor = db_sensor_data['last_seen']

random_group = ['All Treadmills', 'All Ellipticals', 'All Bikes', 'All Weight Benches', 'All Butterfly Machines']
random_placement = ['Treadmill 1', 'Treadmill 2', 'Treadmill 3', 'Treadmill 4', 'Elliptical 1', 'Elliptical 2',
                    'Elliptical 3', 'Elliptical 4', 'Bike 1', 'Bike 2', 'Bike 3', 'Bike 4', 'Weight Bench 1',
                    'Weight Bench 2', 'Butterfly Machine 1', 'Butterfly Machine 2']

# placement index:group index
group_map = {0: 0, 1: 0, 2: 0, 3: 0, 4: 1, 5: 1, 6: 1, 7: 1, 8: 2, 9: 2, 10: 2, 11: 2, 12: 3, 13: 3, 14: 4, 15: 4}


def prime_data():
    l = len(random_placement)
    for r in recent_sensor.find():
        # if 'tag' not in r:
        rindex = random.randint(0, l - 1)
        gindex = group_map[rindex]

        pprint.pprint(r)
        recent_sensor.update({'_id': r['_id']},
                             {"$set": {'tag': '{0}'.format(random.randint(1, 10000)),
                                       'device_name': random_placement[rindex],
                                       'show_in_details_menu': True,
                                       'key_metrics': ['Presence'], 'group': random_group[gindex]}},
                             upsert=False)


if __name__ == "__main__":
    prime_data()
