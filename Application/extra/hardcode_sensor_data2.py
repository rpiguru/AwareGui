# -*- coding: iso8859-15 -*-
import re
import sys, os
import random
import pprint
from pymongo import MongoClient

appdir = os.path.abspath(os.path.dirname(__file__))
if appdir not in sys.path:
    sys.path.append(appdir)

try:
    from global_config import configdata
except ImportError:
    sys.path.append(os.path.abspath(os.path.join(appdir, os.pardir)))
    from global_config import configdata


mongo_client = MongoClient(configdata['mongo_host'], configdata['mongo_port'], connect=False)
db_sensor_data = mongo_client['sensor_data']
recent_sensor = db_sensor_data['last_seen']

GROUP_D = {
    'Treadmill': 'All Treadmills',
    'Elliptical' : 'All Ellipticals',
    'Bike' : 'All Bikes',
    'Sit Up Station' : 'Sit Up Station Machines',
    'Weight Machine' : 'Weight Machine Machines',
    }

DEVICE_D = {
    '0013A200415003E9'.lower() : 'Bike 1',
    '0013A200415004E4'.lower() : 'Bike 2',
    '0013A20041500290'.lower() : 'Elliptical 1',
    '0013A200415003D5'.lower() : 'Elliptical 2',
    '0013A200415003CC'.lower() : 'Treadmill 1',
    '0013A200415004D9'.lower() : 'Treadmill 2',
    '0013A2004150029F'.lower() : 'Sit Up Station',
    '0013A200415004F2'.lower() : 'Weight Machine',
}
	
def hardcode_data():
    for d in DEVICE_D:
        group_key = re.sub(' \d+','',DEVICE_D[d])
        recent_sensor.update({'source_address': d},
                             {"$set": {'tag': '{0}'.format(random.randint(1, 10000)),
                                       'device_name': DEVICE_D[d],
                                       'show_in_details_menu': True,
                                       'key_metrics': ['Presence'], 'group': GROUP_D[group_key]}},
                             upsert=False)


if __name__ == "__main__":
    hardcode_data()
