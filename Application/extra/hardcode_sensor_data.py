# -*- coding: iso8859-15 -*-
import re
import sys, os
import random
import pprint
from pymongo import MongoClient

appdir = os.path.abspath(os.path.dirname(__file__))
if appdir not in sys.path:
    sys.path.append(appdir)

try:
    from global_config import configdata
except ImportError:
    sys.path.append(os.path.abspath(os.path.join(appdir, os.pardir)))
    from global_config import configdata


mongo_client = MongoClient(configdata['mongo_host'], configdata['mongo_port'], connect=False)
db_sensor_data = mongo_client['sensor_data']
recent_sensor = db_sensor_data['last_seen']

GROUP_D = {
    'Treadmill': 'All Treadmills',
    'Elliptical' : 'All Ellipticals',
    'Bike' : 'All Bikes',
    'Weight Bench' : 'All Weight Benches',
    'Leg Press' : 'All Leg Press Machines',
    'Butterefly' : 'All Butterfly Machines',
    'Kinesis' : 'All Kinesis Machines',
    'Low Row' : 'All Low Row Machines',
    'Pectoral' : 'All Pectoral Machines',
    'Vertical Traction' : 'All Vertical Traction Machines',
    }

DEVICE_D = {
    '0013a200415004d8' : 'Bike 1',
    '0013a20041500564' : 'Bike 2',
    '0013a20041500285' : 'Elliptical 1',
    '0013a20041500292' : 'Elliptical 2',
    '0013a2004150026f' : 'Kinesis 1',
    '0013a200415004f3' : 'Leg Press 1',
    '0013a200415004ea' : 'Low Row 1',
    '0013a20041500284' : 'Pectoral 1',
    '0013a20041500280' : 'Treadmill 1',
    '0013a2004150056f' : 'Treadmill 2',
    '0013a2004150056a' : 'Treadmill 3',
    '0013a200415003d0' : 'Treadmill 4',
    '0013a2004150057b' : 'Vertical Traction 1',
}
	
def hardcode_data():
    for d in DEVICE_D:
        group_key = re.sub(' \d+','',DEVICE_D[d])
        recent_sensor.update({'source_address': d},
                             {"$set": {'tag': '{0}'.format(random.randint(1, 10000)),
                                       'device_name': DEVICE_D[d],
                                       'show_in_details_menu': True,
                                       'key_metrics': ['Presence'], 'group': GROUP_D[group_key]}},
                             upsert=False)


if __name__ == "__main__":
    hardcode_data()
