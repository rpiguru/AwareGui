# -*- coding: iso8859-15 -*-
import os
import sys

import pymongo
from gevent.pywsgi import WSGIServer
from flask import Flask, jsonify, render_template
from flask_restful import Resource, Api
from setproctitle import setproctitle
import multiprocessing as mp

appdir = os.path.abspath(os.path.dirname(__file__))
projdir = os.path.abspath(os.path.join(appdir, '..'))
if projdir not in sys.path:
    sys.path.append(appdir)
    sys.path.append(projdir)

mongo_connection = pymongo.mongo_client.MongoClient("127.0.0.1", 27017, connect=False)
database = pymongo.database.Database(mongo_connection, 'sensor_data')
recent_sensor = database['last_seen']
trend_sensor = database['last_n_records']
reboot_msg = database['reboot_msg']
firmware_sensor = database['sensor_firmware']


# Static assets, not uploaded user content.
TEMPLATE_PATH = os.path.abspath(os.path.join(appdir, "../templates"))
STATIC_PATH = os.path.abspath(os.path.join(appdir, "../static"))
app = Flask(__name__, static_url_path='/static', static_folder=STATIC_PATH,
            template_folder=TEMPLATE_PATH)
app._static_folder = STATIC_PATH

app.url_map.strict_slashes = True
api = Api(app)

'''
Inbound
'''


class UpdateSensorData(Resource):
    def post(self):
        pass

    def get(self):
        results = [x for x in recent_sensor.find({})]
        for x in results:
            x['_id'] = str(x['_id'])
        return render_template('sensor_inbound/sensor_inbound.html')


'''
Outbound
'''


class RecentSensorData(Resource):
    def get(self):
        results = [x for x in recent_sensor.find({})]
        for x in results:
            x['_id'] = str(x['_id'])
        return jsonify(items=results)


class TrendSensorData(Resource):
    def get(self):
        results = [x for x in trend_sensor.find({})]
        for x in results:
            x['_id'] = str(x['_id'])
        return jsonify(items=results)


api.add_resource(TrendSensorData, '/trend_sensor_data/', endpoint='tsensor_data')
api.add_resource(RecentSensorData, '/recent_sensor_data/', endpoint='rsensor_data')
api.add_resource(UpdateSensorData, '/configure_sensor_data/', endpoint='csensor_data')


def internal_api():
    setproctitle('aware_http_internal_api.py')
    # app.run(host=FLASK_HOST, port=FLASK_PORT)
    WSGIServer(("127.0.0.1", 6001), app).serve_forever()


mp.Process(target=internal_api).start()
