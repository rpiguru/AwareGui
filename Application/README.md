# Aware Hub GUI
=========================================

## Install dependencies

### Install Python Kivy on RPi

- Install Kivy

        sudo apt-get update
        sudo apt-get install libsdl2-dev libsdl2-image-dev libsdl2-mixer-dev libsdl2-ttf-dev pkg-config libgl1-mesa-dev libgles2-mesa-dev
        sudo apt-get install python-setuptools libgstreamer1.0-dev git-core gstreamer1.0-plugins-{bad,base,good,ugly} gstreamer1.0-{omx,alsa} python-dev
        
        sudo pip3 install cython==0.25.2
        sudo pip3 install kivy

- Using `Network Manager` for wifi configuration on AwareGui application.

        sudo apt-get install network-manager

    Open `/etc/NetworkManager/NetworkManager.conf` and modify something:

        sudo nano /etc/NetworkManager/NetworkManager.conf

    Change `managed=false` to `managed=true`

        sudo service network-manager restart

    Edit `/etc/network/interfaces` file:

        sudo nano /etc/network/interfaces

    Comment out any **wlan** interfaces.

    Reboot system.

    **NOTE:** Wifi interface on PIXEL would not work if you comment out the line that contains `/etc/wpa_supplicant`


### Backlight adjustment

    sudo nano /etc/udev/rules.d/backlight-permissions.rules

Insert the line:
    
    SUBSYSTEM=="backlight",RUN+="/bin/chmod 666 /sys/class/backlight/%k/brightness /sys/class/backlight/%k/bl_power"    
    

### Display *BuildingLink* logo at boot time.
    
- Remove log messages on boot.

        sudo nano /boot/cmdline.txt
    
    + Replace `console=tty1` by `console=tty3` to redirect boot messages to the third console.
    + Add `loglevel=3` to disable non-critical kernel log messages.
    + Add `logo.nologo quiet` to the end of the line to remove the Raspberry PI logos from displaying 
    
    Now, open `/etc/rc.local` and add another command:
    
        sudo nano /etc/rc.local

    Add `clear` just before the line exit 0.
    
    And reboot.
    
- Add custom logo at boot time.
    
        sudo apt-get install fbi
        sudo cp kivy_app/assets/images/aware_logo_fullsize.png /etc/splash.png

    Create startup script.
        
        sudo nano /etc/init.d/asplashscreen
    
    And add below.
    
        #! /bin/sh
        ### BEGIN INIT INFO
        # Provides:          asplashscreen
        # Required-Start:
        # Required-Stop:
        # Should-Start:      
        # Default-Start:     S
        # Default-Stop:
        # Short-Description: Show custom splashscreen
        # Description:       Show custom splashscreen
        ### END INIT INFO
        
        do_start () {
        
            /usr/bin/fbi -T 1 -noverbose -a /etc/splash.png    
            exit 0
        }
        
        case "$1" in
          start|"")
            do_start
            ;;
          restart|reload|force-reload)
            echo "Error: argument '$1' not supported" >&2
            exit 3
            ;;
          stop)
            # No-op
            ;;
          status)
            exit 0
            ;;
          *)
            echo "Usage: asplashscreen [start|stop]" >&2
            exit 3
            ;;
        esac
        
        :
    
    Make script executable and install it for init mode.
        
        sudo chmod a+x /etc/init.d/asplashscreen
        sudo insserv /etc/init.d/asplashscreen
    
    Now reboot.
        
        sudo reboot

### Execute GUI app
    
    sudo pip3 install -r Application/extra/requirements.txt
    
    sudo python3 app.py


### Make `.deb` package (not tested)
    
    
    cd deb
    pyinstaller --onefile make_deb.py

