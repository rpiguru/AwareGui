# -*- coding: iso8859-15 -*-

"""
    Common utilities
"""
import os

import datetime
import global_config
from kivy.config import _is_rpi
from utils import config_util
import socket
import subprocess


def get_new_version():
    # TODO: get the latest version
    return '0.2.0'


def turn_bright(n):
    """
    Adjust brightness of touch screen
    :param n: brightness value, range 0 ~ 255
    :return:
    """
    if n < 0:
        n = 0
    if n > 255:
        n = 255
    if _is_rpi:
        p = subprocess.Popen(['echo {} > /sys/class/backlight/rpi_backlight/brightness'.format(n)], shell=True)
        p.communicate()
        p.wait()


def get_aware_name():
    return config_util.get_config('device_name', 'Aware')


def set_aware_name(val):
    config_util.set_config('device_name', val)


def get_aware_title():
    return config_util.get_config('title', 'Aware Hub')


def get_hostname():
    return socket.gethostname()


def get_serial_number():
    """
    Get Serial Number of RPi
    """
    sn = "0000000000000000"
    try:
        f = open('/proc/cpuinfo', 'r')
        for line in f:
            if line[0:6] == 'Serial':
                sn = line[10:26]
        f.close()
    except IndexError:
        sn = "ERROR000000000"

    return sn


def number_to_ordinal(n):
    """
    Convert number to ordinal number string
    """
    return "%d%s" % (n, "tsnrhtdd"[(n / 10 % 10 != 1) * (n % 10 < 4) * n % 10::4])


def get_git_commit_date():
    m_date = os.path.getmtime(os.path.join(global_config.appdir, 'VERSION'))
    return datetime.datetime.fromtimestamp(m_date).strftime("%d/%m/%Y %H:%M:%S")


def get_app_note():
    rel_file = os.path.join(os.path.dirname(global_config.appdir), 'Documentation', 'ApplicationNotes.txt')
    try:
        f = open(rel_file, 'r+')
        content = f.read()
        f.close()
        return content
    except IOError:
        return 'Not Available'


def get_git_revision():
    try:
        f = open(os.path.join(global_config.appdir, 'VERSION'), 'r+')
        content = f.read()
        f.close()
        for line in content.splitlines():
            if line.strip().startswith('git revision'):
                return line.split()[2]
    except (IOError, IndexError):
        pass
    return 'Not Available'
