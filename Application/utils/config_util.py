# -*- coding: iso8859-15 -*-

"""
    Utility for configuration
    All configurations are saved into the local mongodb (sensor_data/awaregui)
    NOTE: There MUST be only a document in this collection.

"""
from pymongo import MongoClient
from global_config import configdata, logger

try:
    if not configdata['debug']:
        mongo_client = MongoClient(configdata['mongo_host'], configdata['mongo_port'], connect=False)
        db_sensor_data = mongo_client['sensor_data']
        gui_col = db_sensor_data[configdata['mongo_collection']]
except Exception as e:
    logger.error('Failed to local mongodb: {}'.format(e))


def get_config(option, default_value=None):
    """
    Get configuration value from the local mongo DB
    :param option:
    :param default_value:
    :return:
    """
    if configdata['debug']:
        return default_value
    try:
        doc = gui_col.find_one()
    except Exception as er:
        logger.error('Failed to get config from the local mongodb: {}'.format(er))
        return default_value

    if doc is None:
        return default_value
    if option not in doc.keys():
        return default_value
    else:
        return doc[option]


def set_config(option, value):
    """
    :param option:
    :param value:
    :return:
    """
    if configdata['debug']:
        return True
    try:
        doc = gui_col.find_one()
        if doc is None:
            gui_col.insert_one({option: value})
        else:
            doc.update({option: value})
            gui_col.update({'_id': doc['_id']}, {"$set": doc}, upsert=False)
        return True
    except Exception as er:
        logger.error('Failed to update config from the local mongodb: {}'.format(er))
        return False
