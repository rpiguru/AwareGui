# -*- coding: iso8859-15 -*-
import os
import smtplib
import socket
import sys
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.utils import COMMASPACE

appdir = os.path.abspath(os.path.dirname(__file__))
projdir = os.path.abspath(os.path.join(appdir, '..'))
if projdir not in sys.path:
    sys.path.append(appdir)
    sys.path.append(projdir)

from global_config import configdata


def send_email(to_list, subject, body):
    message = MIMEMultipart()
    message['From'] = configdata['email_user']
    message['Subject'] = subject
    message.preamble = ''
    body = MIMEText(body, _subtype="plain", _charset="utf-8")
    message.attach(body)
    send_to = [configdata['email_user']] + to_list
    message['To'] = COMMASPACE.join(send_to)

    smtp = smtplib.SMTP(configdata['email_server'], configdata['email_port'])
    smtp.starttls()
    smtp.ehlo()
    smtp.login(configdata['email_user'], configdata['email_password'])
    smtp.sendmail(configdata['email_user'], send_to, message.as_string())


def aware_email_alert(body):
    return send_email([], "Alert from {0}".format(socket.gethostname()), body)


if __name__ == "__main__":
    send_email(['rpiguru@techie.com'], "Test", """This is a test of the BL alert system.
        This is only a test. Beeeeeeeeeeeeeep""")
