# -*- coding: iso8859-15 -*-

"""
System utility to change GPU memory size
"""
import getpass
import os
import multiprocessing
import platform
import random
from datetime import timedelta

import signal

import pam
from kivy.config import _is_rpi
from global_config import logger


def get_platform():
    """
    Get the OS name, hostname and kernel
    """
    try:
        osname = " ".join(platform.linux_distribution())
        uname = platform.uname()

        if len(osname.strip()) == 0:
            osname = uname[0]

        return {'osname': osname, 'hostname': uname[1], 'kernel': uname[2]}

    except Exception as err:
        logger.exception('Failed to get detailed platform information: {}'.format(err))
        return {'osname': 'N/A', 'hostname': 'N/A', 'kernel': 'N/A'}


def get_uptime():
    """
    Get uptime
    """
    try:
        with open('/proc/uptime', 'r') as f:
            uptime_seconds = float(f.readline().split()[0])
            uptime_time = str(timedelta(seconds=uptime_seconds))
            data = uptime_time.split('.', 1)[0]
        return data
    except Exception as err:
        logger.exception('Failed to get up time: {}'.format(err))
        return 'N/A'


def change_gpu_size(size):

    f = open('/boot/config.txt', 'r+')
    content = f.readlines()

    is_changed = False

    for i in range(len(content)):
        if 'gpu_mem' in content[i]:
            content[i] = 'gpu_mem=256'
            is_changed = True
            break
    if not is_changed:
        content.append('gpu_mem={}'.format(size))

    f.seek(0)
    f.truncate()
    f.write(''.join(content))
    f.close()
    return True


def get_cpus():
    """
    Get the number of CPUs and model/type
    """
    try:
        pipe = os.popen("cat /proc/cpuinfo |" + "grep 'model name'")
        data = pipe.read().strip().split(':')[-1]
        pipe.close()

        if not data:
            pipe = os.popen("cat /proc/cpuinfo |" + "grep 'Processor'")
            data = pipe.read().strip().split(':')[-1]
            pipe.close()

        cpus = multiprocessing.cpu_count()
        return {'cpus': cpus, 'type': data}

    except Exception as err:
        logger.exception('Failed to get number of CPUs: {}'.format(err))
        return {'cpus': 1, 'type': 'N/A'}


def get_cpu_usage():
    """
    Get the CPU usage and running processes
    :rtype: dict
    """
    try:
        pipe = os.popen("ps aux --sort -%cpu,-rss")
        data = pipe.read().strip().split('\n')
        pipe.close()

        usage = [i.split(None, 10) for i in data]
        del usage[0]

        total_usage = []

        for element in usage:
            usage_cpu = element[2]
            total_usage.append(usage_cpu)

        total_usage = round(sum(float(i) for i in total_usage) / get_cpus()['cpus'], 1)

        total_free = round((100 * int(get_cpus()['cpus'])) - float(total_usage), 1)

        cpu_used = {'free': total_free, 'used': total_usage, 'all': total_free + total_usage}
        return cpu_used

    except Exception as err:
        logger.exception('Failed to get CPU usage: {}'.format(err))
        return {'free': 0, 'used': 0, 'all': 0}


def get_mem():
    """
    Get memory usage
    """
    try:
        pipe = os.popen("free -tm | grep 'Mem' | awk '{print $2,$4,$6,$7}'")
        data = pipe.read().strip().split()
        pipe.close()

        all_mem = int(data[0])
        free_mem = int(data[1])
        buffers = int(data[2])
        cached_mem = int(data[3])

        # Memory in buffers + cached is actually available, so we count it
        # as free. See http://www.linuxatemyram.com/ for details
        free_mem += buffers + cached_mem

        percent = (100 - ((free_mem * 100) / all_mem))
        usage = (all_mem - free_mem)

        mem_usage = {'used': usage, 'buffers': buffers, 'cached': cached_mem, 'total': all_mem,
                     'free': free_mem, 'percent': percent}
        return mem_usage

    except Exception as err:
        logger.exception('Failed to get memory usage: {}'.format(err))
        return {'used': 0, 'buffers': 0, 'cached': 0, 'total': 0, 'free': 0, 'percent': 0}


def get_cpu_temperature_c():
    """
    Get Celsius Temperature of CPU
    :return:
    """
    if _is_rpi:
        pipe = os.popen("cat /sys/class/thermal/thermal_zone0/temp")
        data = pipe.read().strip()
        pipe.close()
        return round(float(data) / 1000, 1)
    else:
        return random.randint(50, 100)


def get_cpu_temperature_f():
    """
    Get fahrenheit temperature of CPU
    :return:
    """
    return round(get_cpu_temperature_c() * 1.8 + 32, 1)


def get_disk_usage():
    """
    Get disk usage
    :return:
    """
    try:
        pipe = os.popen("df -h")
        data = pipe.read().strip().splitlines()[1].split()
        pipe.close()

        return {'size': data[1], 'used': data[2], 'available': data[3], 'use': float(data[4][:-1])}
    except Exception as er:
        logger.exception('Failed to get disk usage: {}'.format(er))
        return {'size': 'N/A', 'used': 'N/A', 'available': 'N/A', 'use': 0}


def kill_process(proc_name):
    """
    Search the process and kill it.
    """
    pid_list = []
    pipe = os.popen("ps ax | grep " + proc_name + " | grep -v grep")
    cmd_result = pipe.read().strip()
    pipe.close()
    for line in cmd_result:
        fields = line.split()
        pid_list.append(fields[0])
    # if app is already running, the list's size is greater than 2
    if len(pid_list) > 0:
        for pid in pid_list:
            os.kill(int(pid), signal.SIGKILL)  # kill all process


def authenticate_user(pwd):
    """
    Check system password of current user
    NOTE: This function is blocking the flow(1~2 sec) when password is incorrect
    :param pwd:
    :return:
    """
    user = getpass.getuser()
    p = pam.pam()
    return p.authenticate(user, pwd, service='system-auth')
