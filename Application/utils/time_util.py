# -*- coding: iso8859-15 -*-

"""
    Utility for date & time operations
"""
import glob
from subprocess import PIPE
import utils.config_util as config_util
import datetime
import subprocess
from global_config import debug, logger
import pytz
import time
from kivy.config import _is_rpi


def get_current_timezone():
    """
    Get Olson timezone name from local mongodb
    :return:
    """
    return config_util.get_config('timezone', 'US/Eastern')


def set_current_timezone(tz):
    """
    Save new timezone value to local mongodb
    :param tz:
    :return:
    """
    return config_util.set_config('timezone', tz)


def get_local_time():
    """
    Get local time based on the timezone setting from the local mongodb
    :return:
    """
    str_tz = config_util.get_config('timezone', 'US/Eastern')
    utc_time = datetime.datetime.utcnow()
    if str_tz is None:
        local_time = utc_time
    else:
        tz = pytz.timezone(str_tz)
        local_time = pytz.utc.localize(utc_time, is_dst=None).astimezone(tz)
    return local_time


def to_utc_datetime(local_dt):
    """
    Convert local datetime to utc datetime
    """
    tz = pytz.timezone(config_util.get_config('timezone', 'US/Eastern'))
    return tz.normalize(tz.localize(local_dt)).astimezone(pytz.utc)


def epoch_to_datetime(epoch):
    str_tz = config_util.get_config('timezone', 'US/Eastern')
    if str_tz is None:
        return datetime.datetime.fromtimestamp(epoch)
    else:
        return datetime.datetime.fromtimestamp(epoch, pytz.timezone(str_tz))


def update_system_datetime(utc_dt):
    str_new_time = utc_dt.strftime('%Y-%m-%d %H:%M:%S')
    if _is_rpi:
        p = subprocess.Popen(["date --set='{}'".format(str_new_time)], shell=True)
        p.communicate()
        p.wait()
        logger.debug('Updating system date & time: {}'.format(str_new_time))
    else:
        # aware_logger.info('Setting new date & time: {}'.format(str_new_time))
        logger.debug('Setting new UTC date & time: {}'.format(str_new_time))


def format_time(_time):
    elapsed = int(time.time()) - _time
    if elapsed < 10:
        return 'Just Now'
    elif elapsed < 60:
        return '{} seconds ago'.format(elapsed)
    elif elapsed < 120:
        return 'A minute ago'.format(elapsed)
    elif elapsed < 3600:
        return '{} minutes ago'.format(int(elapsed / 60))
    elif elapsed < 7200:
        return 'An hour ago'
    elif elapsed < 3600 * 24:
        return '{} hours ago'.format(int(elapsed / 3600))
    elif elapsed < 3600 * 24 * 2:
        return 'A day ago'
    elif elapsed < 3600 * 24 * 30:
        return '{} days ago'.format(int(elapsed / 3600 / 24))
    elif elapsed < 3600 * 24 * 30 * 2:
        return 'A month ago'
    elif elapsed < 3600 * 24 * 30 * 12:
        return '{} months ago'.format(int(elapsed / 3600 / 24 / 30))
    elif elapsed < 3600 * 24 * 30 * 12 * 2:
        return 'A year ago'
    else:
        return '{} years ago'.format(int(elapsed / 3600 / 24 / 30 / 12))


def get_all_timezone_names():
    """
    Get Olson timezone names
    :return: List of Olson timezone names
    """
    # FIXME: If we need additional timezones out of US/Europe, just add!
    continental_list = ['US', 'Europe']

    zone_list = []
    if not debug:
        for cont in continental_list:
            for zone in glob.glob('/usr/share/zoneinfo/{}/*'.format(cont)):
                zone_list.append('{}/{}'.format(*(zone.split('/')[-2:])))
    else:
        zone_list = ['US/Aleutian', 'US/Arizona', 'US/Indiana-Starke', 'US/Mountain', 'US/Pacific', 'US/Samoa',
                     'US/Hawaii', 'US/Central', 'US/Eastern', 'US/Alaska', 'US/East-Indiana', 'US/Michigan',
                     'US/Pacific-New', 'Europe/Podgorica', 'Europe/Oslo', 'Europe/Stockholm', 'Europe/Tiraspol',
                     'Europe/Bucharest', 'Europe/Mariehamn', 'Europe/Nicosia', 'Europe/Paris', 'Europe/Amsterdam',
                     'Europe/Belgrade', 'Europe/Malta', 'Europe/Lisbon', 'Europe/Budapest', 'Europe/Kiev',
                     'Europe/London', 'Europe/Belfast', 'Europe/Monaco', 'Europe/Vatican', 'Europe/Warsaw',
                     'Europe/Gibraltar', 'Europe/Istanbul', 'Europe/Astrakhan', 'Europe/Jersey', 'Europe/Andorra',
                     'Europe/Uzhgorod', 'Europe/Madrid', 'Europe/Moscow', 'Europe/Isle_of_Man', 'Europe/Chisinau',
                     'Europe/Vaduz', 'Europe/San_Marino', 'Europe/Zaporozhye', 'Europe/Vilnius', 'Europe/Simferopol',
                     'Europe/Vienna', 'Europe/Tallinn', 'Europe/Guernsey', 'Europe/Athens', 'Europe/Zurich',
                     'Europe/Helsinki', 'Europe/Ulyanovsk', 'Europe/Zagreb', 'Europe/Bratislava', 'Europe/Saratov',
                     'Europe/Dublin', 'Europe/Sarajevo', 'Europe/Berlin', 'Europe/Skopje', 'Europe/Luxembourg',
                     'Europe/Prague', 'Europe/Riga', 'Europe/Sofia', 'Europe/Ljubljana', 'Europe/Copenhagen',
                     'Europe/Tirane', 'Europe/Rome', 'Europe/Minsk', 'Europe/Kirov', 'Europe/Kaliningrad',
                     'Europe/Samara', 'Europe/Brussels', 'Europe/Volgograd', 'Europe/Busingen']

    # Ignore timezone that is not supported by `pytz` package.
    tz_list = []
    for tz in zone_list:
        try:
            pytz.timezone(tz)
            tz_list.append(tz)
        except pytz.UnknownTimeZoneError:
            pass
    return tz_list


timezone_list = get_all_timezone_names()


if __name__ == '__main__':

    print(get_all_timezone_names())
