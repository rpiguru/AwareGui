from global_config import configdata


def set_config_val(_key, val):
    configdata[_key] = val


def get_config_val(_key):
    return configdata[_key]
