#!/usr/bin/env python
# -*- coding: iso8859-15 -*-
"""

    GUI app for Aware Hub

__author__ = "Wang Yang"
__copyright__ = "Copyright 2017, The Aware Project"
__credits__ = ["Wang Yang", ]
__version__ = "0.0.1"
__maintainer__ = "Wang Yang"
__email__ = "rpiguru@techie.com"
__status__ = "Development"

"""

import os
import traceback
import sys
from kivy.app import App

# Import configure after App import
import kivy_config

import gc
import utils.common
import utils.net
import utils.time_util
import subprocess
from kivy.base import ExceptionHandler, ExceptionManager
from kivy.config import _is_rpi
from kivy.lang import Builder
from kivy.properties import BooleanProperty, ObjectProperty
from kivy.uix.floatlayout import FloatLayout
from kivy.uix.screenmanager import NoTransition
from kivy.clock import Clock

# Import widget manager to perform factory registration
import widgets.widgetmanager

from screens.screenmanager import screens
from kivymd.theming import ThemeManager
from widgets.popup import FullLogoPopup
from global_config import configdata, logger, version, debug

appdir = os.path.abspath(os.path.dirname(__file__))
if appdir not in sys.path:
    sys.path.append(appdir)


class AwareExceptionHandler(ExceptionHandler):

    def handle_exception(self, exception):
        logger.exception('-- Exception: {}'.format(repr(exception)))
        _app = App.get_running_app()
        _app.save_exception(traceback.format_exc(limit=20))
        _app.switch_screen('error_screen')
        return ExceptionManager.PASS


ExceptionManager.add_handler(AwareExceptionHandler())


Builder.load_file(os.path.join(os.path.dirname(__file__), 'app.kv'))


class MainWidget(FloatLayout):
    counter = 0
    logo_displayed = False

    def __init__(self, **kwargs):
        super(MainWidget, self).__init__(**kwargs)
        Clock.schedule_interval(self.count_down, 1)
        self.initialize_counter()

    def initialize_counter(self):
        utils.common.turn_bright(255)
        self.logo_displayed = False
        self.counter = configdata['screen_saver_time'] * 60

    def on_touch_down(self, touch):
        if self.logo_displayed:
            self.initialize_counter()
            return
        self.initialize_counter()
        super(MainWidget, self).on_touch_down(touch)
        if self.counter != 0 and self.ids.lb_time.collide_point(*touch.pos):
            _app = App.get_running_app()
            _app.cur_screen_inst.switch_screen('clock_screen', 'right')

    def count_down(self, *args):
        if self.counter > 0:
            self.counter -= 1
        elif not self.logo_displayed:
            FullLogoPopup().open()
            self.logo_displayed = True
            utils.common.turn_bright(configdata["sleep_brightness"])


class AwareHubApp(App):

    show_back_button = BooleanProperty(False)
    cur_screen_inst = ObjectProperty(None)
    theme_cls = ThemeManager()

    previous_screen = None
    exception = None

    has_internet = False

    clk_time = None
    clk_wifi = None
    clk_ap = None

    running_popup = None

    def build(self):
        """
        base function of kivy app
        :return:
        """
        self.title = utils.common.get_aware_title()
        self.root = MainWidget()
        self.update_dev_name()
        self.update_ap()
        self.start_schedules()
        self.switch_screen(configdata['INITIAL_SCREEN'])

    def start_schedules(self):
        self.clk_time = Clock.schedule_interval(self.show_time, 1)
        self.clk_wifi = Clock.schedule_interval(self.update_wifi_signal_strength, 1)
        self.clk_ap = Clock.schedule_interval(self.update_ap, 5)

    def switch_screen(self, screen_name, direction=None, **kwargs):
        """
        Go to given screen
        :param screen_name:     destination screen name
        :param direction:       "up", "down", "right", "left"
        :return:
        """
        try:
            sm = self.root.ids.sm
            # sm.transition = NoTransition() if direction is None else SlideTransition()
            sm.transition = NoTransition()
            if sm.has_screen(screen_name):
                sm.current = screen_name
                return
            else:
                if screen_name in screens.keys():
                    if self.previous_screen:
                        self.previous_screen.opacity = 0
                    if screen_name == 'settings_screen' and 'start_tab' in kwargs.keys():
                        screen = screens[screen_name](name=screen_name, start_tab=kwargs['start_tab'])
                    else:
                        screen = screens[screen_name](name=screen_name)
                    self.cur_screen_inst = screen
                    # sm.switch_to(screen, direction=direction)
                    sm.switch_to(screen)
                    if screen_name != 'blank_screen':
                        logger.debug('Moving to {} '.format(screen_name))
                    if self.previous_screen:
                        sm.remove_widget(self.previous_screen)
                        del self.previous_screen
                        gc.collect()
                    self.previous_screen = screen
                    self.show_back_button = screen.show_back_button
                    if self.show_back_button:
                        self.root.ids.btn_back.bind(on_release=screen.on_back)
                        Clock.schedule_once(self.display_back_button)
                    else:
                        self.root.ids.btn_back.opacity = 0
                    return True
        except Exception as e:
            logger.error('Failed to switch screen: {}'.format(e))

    def display_back_button(self, *args):
        self.root.ids.btn_back.opacity = 1

    def update_wifi_signal_strength(self, *args):
        signal_strength = utils.net.get_wifi_strength()
        self.root.ids.img_wifi.set_strength(int(signal_strength / 20))

    def go_to_settings(self, tab_name):
        """
        Go to a tab of settings screen
        """
        self.switch_screen('settings_screen', start_tab=tab_name)

    def save_exception(self, ex):
        self.exception = ex

    def get_exception(self):
        return self.exception

    def update_dev_name(self):
        self.root.ids.lb_dev_name.text = utils.common.get_aware_name()

    def stop(self, *largs):
        # Move to blank screen to kill threads in each screens
        self.switch_screen('blank_screen')
        # Cancel all scheduled tasks
        self.clk_time.cancel()
        self.clk_wifi.cancel()
        self.clk_ap.cancel()

        if not debug:
            p = subprocess.Popen(['sudo', 'systemctl', 'stop', 'awaregui'])
            p.communicate()
            p.wait()
        super(AwareHubApp, self).stop(*largs)

        os.kill(os.getpid(), 9)

    def open_dashboard(self):
        """
        Close this app.
        The master process will open the web browser, and will open me as soon as the browser gets closed.
        :return:
        """
        self.stop()

    # ================================= Scheduled functions ======================================

    def show_time(self, *args):
        local_time = utils.time_util.get_local_time()
        self.root.ids.lb_time.text = local_time.strftime("%I:%M %p")

    def update_ap(self, *args):
        cur_ap, mac = utils.net.get_current_ap()
        if cur_ap is None:
            # logger.info('WiFi is not connected to any router')
            self.root.ids.lb_ap_name.text = 'Not connected'
            self.root.ids.img_wifi_status.source = 'assets/images/wifi/wifi_disconnected.png'
        else:
            # logger.info('WiFi is connected to `{}`'.format(cur_ap))
            self.root.ids.lb_ap_name.text = 'Connected to: [b]{}[/b]'.format(cur_ap)
            self.root.ids.img_wifi_status.source = 'assets/images/wifi/wifi.png'


if __name__ == '__main__':

    if _is_rpi:
        if os.geteuid() != 0:
            msg = "You need to have root privileges to run this script.\nPlease try again by using 'sudo'.\nExiting..."
            logger.error(msg)
            exit(msg)
    logger.info('=== Starting Aware GUI APP, version: {} ==='.format(version))

    app = AwareHubApp()

    try:
        app.run()
    finally:
        app.stop()
