# -*- coding: iso8859-15 -*-
"""
    Global configuration file to be used everywhere

    Important Note: The variables that starts with `_` are not intended to be used outside of this module.
                    If you need to create a variable in this module that would be used outside,
                    remove `_` at the beginning of its name.

"""
import json
import logging.handlers
import os
import pprint
import re
import socket
import sys
from raven import Client
from raven.handlers.logging import SentryHandler

# used to turn off default kivy loggers
os.environ["KIVY_NO_FILELOG"] = '1'
# os.environ['KIVY_USE_DEFAULTCONFIG'] = '1'

appdir = os.path.abspath(os.path.dirname(__file__))
if appdir not in sys.path:
    sys.path.append(appdir)

# ============================ Read config file =====================================
_override_name = re.sub('\.', '_', socket.gethostname())
_override_name = re.sub('-', '_', _override_name)
_override_name = re.sub(' ', '_', _override_name)
_host_config_override = 'awaregui_{0}.config'.format(_override_name)
# Read configuration file. Allow host name override for testing.
try:
    _config_filename = os.path.join(appdir, _host_config_override)
    _config_data = open(_config_filename).read()
except Exception as e:
    print("Failed to load local config file({}), now loading default config file...".format(e))
    _config_filename = os.path.join(appdir, "awaregui.config")
    _config_data = open(_config_filename).read()

configdata = json.loads(_config_data)


# ================ Set up a specific logger with our desired output level ==================
_log_level = logging.getLevelName(configdata['loglevel'])
logging.basicConfig(level=_log_level)
logger = logging.getLogger("AwareGui")
logger.setLevel(_log_level)
# Current Log Formatter for both the console output and the log file
_formatter = logging.Formatter(
    '%(asctime)s - %(name)s - %(levelname)s - %(module)s - %(funcName)s - %(threadName)s - %(thread)d - %(message)s')

# Create log dir if not exists
if not os.path.exists(os.path.dirname(configdata["logfile"])):
    try:
        os.makedirs(os.path.dirname(configdata["logfile"]))
    except OSError as exc:  # Guard against race condition
        pass

_filehandler = logging.handlers.RotatingFileHandler(configdata["logfile"], maxBytes=configdata["logsize"],
                                                    backupCount=configdata["logbkcnt"])
_filehandler.setFormatter(_formatter)
_filehandler.setLevel(_log_level)

_console_handler = logging.StreamHandler()
_console_handler.setFormatter(_formatter)

# This variable will be used widely in other modules
logger.addHandler(_filehandler)
logger.addHandler(_console_handler)

# ============================ Add Sentry logger ===========================================
version = open(os.path.join(appdir, 'VERSION')).read()

_sentry_client = Client(dsn=configdata["sentry_dsn"], release=version)
_sentry_handler = SentryHandler(_sentry_client)
_sentry_handler.setLevel(logging.ERROR)
logger.addHandler(_sentry_handler)

# ============================ Constant values to be used elsewhere =========================

debug = configdata['debug']
animation = configdata['animation']

_msg = "current config:" + pprint.pformat(configdata)
logger.info(_msg)
