# -*- coding: iso8859-15 -*-
"""
    Common utilities of `Aware Service`
"""
import json
import os
import urllib.request
import time
from global_config import configdata, logger

# Field names to be excluded when getting all sensor types
from utils.mail import aware_email_alert
from utils.store import get_config_val

exclusive_sensor_fields = ['last_seen', 'msg_type', 'source_address', 'p1']

unit_map = {
    'BatteryLevel': '%',
    'BatteryVoltage': 'mV',
    'Humidity': '%',
    'Temperature': '�F',
    'Distance': '"',
    'Volume': '%',
}

boolean_sensors = ['IsOpen', 'Presence']

cnt_sensor_data_fail = 0


def get_status():
    """
    Get status of `Aware Service`
    :return:
    """
    return True


def enable_service():
    pass


def stop_service():
    pass


def get_last_sensor_data(is_detail=True):
    """
    Retrieve last sensor data from aware_service
    :return:
    """
    global cnt_sensor_data_fail
    if get_config_val('debug'):
        resp = json.loads(open(os.path.join(os.path.dirname(__file__), 'sample_response.json')).read())
        try:
            for item in resp['items']:
                item['last_seen'] = int(time.time())
                item['data']['last_seen'] = item['last_seen']
                if is_detail:
                    if 'show_in_details_menu' in item.keys() and \
                                    item['show_in_details_menu'] in [False, 'false', 'False']:
                        resp['items'].remove(item)
            return resp
        except KeyError as e:
            logger.warning('Error when parsing sample JSON data: {}'.format(e))
    else:
        try:
            response = urllib.request.urlopen(configdata['url_last'])
            data = json.loads(response.read().decode('utf-8'))
            return data
        except Exception as e:
            if cnt_sensor_data_fail < 10:
                logger.error('Failed to get last sensor data from aware service: {}'.format(e))
                cnt_sensor_data_fail += 1
            else:
                logger.error(
                    '** Failed to get last sensor data from aware service, sending email alert, reason: {}'.format(e))
                aware_email_alert('Failed to get last sensor data from aware service, reason: {}'.format(e))
                cnt_sensor_data_fail = 0


def check_sensor_status():
    _data = get_last_sensor_data(is_detail=False)
    if _data is not None and 'items' in _data.keys():
        item_on = item_off = 0
        for _item in _data['items']:
            try:
                if 'msg_type' in _item.keys():
                    msg_type = _item['msg_type']
                elif 'msg_type' in _item['data'].keys():
                    msg_type = _item['data']['msg_type']
                else:
                    msg_type = None
            except Exception as e:
                msg_type = None
                logger.warning('Error({}) when getting `msg_type` from JSON data: {}'.format(e, _item))

            try:
                if msg_type == 1 and 'p1' not in _item['data'].keys():
                    last_seen = _item.get('last_seen') if _item.get('last_seen') else _item['data']['last_seen']
                    if check_connectivity(last_seen):
                        item_on += 1
                    else:
                        item_off += 1
            except Exception as e:
                logger.warning('Error when checking connectivity from JSON data({}): {}'.format(_item, e))

        return {'on': item_on, 'off': item_off}
    else:
        return {'on': 0, 'off': 0}


def get_sensor_data():
    """
    Retrieve sensor data from the past 24 hours
    :return:
    """
    try:
        response = urllib.request.urlopen(configdata['url_24h'])
        data = json.loads(response.read().decode('utf-8'))
        return data
    except Exception as e:
        logger.error('Failed to get sensor data from aware service: {}'.format(e))


def get_all_sensor_types(data):
    """
    Get all sensor types from the sensor data
    :param data:
    :return:
    """
    if data is None:
        return []
    sensor_list = []
    for item in [d['data'] for d in data['items'] if d.get('data')]:
        for _key in item.keys():
            if _key not in exclusive_sensor_fields and _key not in sensor_list:
                sensor_list.append(_key)
    return sorted(sensor_list)


def get_all_group_names(data):
    """getting `msg_type`
    Retrieve all group names
    :param data:
    :return:
    :rtype: dict
    """
    if data is None:
        return {}
    group_list = []
    for item in data['items']:
        if 'group' in item.keys():
            if not item['group'] in group_list:
                group_list.append(item['group'])
        else:
            logger.warning('Item does not have `group` key: {}'.format(item))

    return {g: g for g in sorted(group_list)}


def get_devices_by_group(data, group_name):
    """
    Retrieve devices by their group name
    :param data:
    :param group_name:
    :return:
    :rtype: dict
    """
    devs = {}
    for item in data['items']:
        try:
            if item.get('group') == group_name:
                if 'p1' not in item['data'].keys() and item.get('device_name'):     # Ignore hubs
                    devs.update({item['source_address']: item['device_name']})
        except Exception as e:
            logger.warning('Error({}) when getting device by group: {}'.format(e, item))
    return devs


def get_all_device_info(data):
    """
    Get all device info from the sensor data
    :param data:
    :return:
    """
    if data is None:
        return []
    dev_list = []
    for item in data['items']:
        if 'p1' in item['data'].keys():
            continue
        try:
            msg_type = item['msg_type']
        except KeyError:
            try:
                msg_type = item['data']['msg_type']
            except KeyError:
                msg_type = None
        if item.get('device_name'):
            dev_data = {
                'device_name': item['device_name'],
                'msg_type': msg_type,
                'last_seen': item['last_seen'],
                'group': item['group'],
                'source_address': item['source_address']
            }
            dev_list.append(dev_data)

    return dev_list


def get_device_names(_data, _filter='All'):
    """
    Get list of device names with given filter
    :param _data: sensor data
    :param _filter: `All`, `Only Connected`, `Not Connected`
    :return:
    """
    loc_list = {}
    for d in get_all_device_info(_data):
        if d.get('device_name') and d.get('source_address'):
            is_connected = check_connectivity(d['last_seen'])
            if _filter == 'All':
                loc_list.update({d['source_address']: d['device_name']})
            elif _filter == 'Only Connected' and is_connected:
                loc_list.update({d['source_address']: d['device_name']})
            elif _filter == 'Not Connected' and not is_connected:
                loc_list.update({d['source_address']: d['device_name']})
        else:
            logger.warning('Item does not have `device_name` or `source_address`: {}'.format(d))
    return loc_list


def get_dev_by_address(_data, address):
    """
    Get detailed sensor data of a device with its address
    :param _data:
    :param address:
    :return:
    """
    try:
        resp_dev = [dev for dev in _data['items'] if dev.get('source_address') == address][0]
        dev = resp_dev.get('data')
        return dev
    except Exception as e:
        logger.warning('Error({}) when getting device by address: {}'.format(e, address))


def get_sensors_by_sensor_name(data, sensor_name):
    """
    Get all measurement data of a sensor
    :param data:
    :param sensor_name:
    :return:
    """
    vals = []
    for item in data['items']:
        try:
            _val = {
                'address': item['source_address'],
                'device_name': item['device_name'],
                'group': item['group'],
                'value': item['data'].get(sensor_name)
            }
            if _val.get('value'):
                vals.append(_val)
        except Exception as e:
            logger.warning('Failed to get sensor({}) by sensor name({}), item: {}'.format(e, sensor_name, item))
    return vals


def add_unit_to_sensor_value(sensor_name, value):
    """
    Add unit string at the end of the sensor value
    :param sensor_name:
    :param value:
    :return:
    """
    if sensor_name in boolean_sensors:
        return 'Yes' if value == '1' else 'No'
    elif 'temperature' in sensor_name.lower():
        try:
            temp_c = float(value)
            return '{} {}'.format(round(temp_c * 1.8 + 32, 1), unit_map['Temperature'])
        except ValueError:
            return 'Invalid'
    elif sensor_name in unit_map.keys():
        return '{} {}'.format(value, unit_map[sensor_name])
    else:
        return value


def check_connectivity(epoch_time):
    return True if time.time() - epoch_time < 120 else False
