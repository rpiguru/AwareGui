# -*- coding: iso8859-15 -*-

import os
from kivy.factory import Factory
from kivy.lang import Builder
from kivy.properties import NumericProperty, ListProperty, StringProperty, AliasProperty
from kivy.uix.boxlayout import BoxLayout
import assets.styles.defaultstyle as style
from kivymd.icon_definitions import md_icons

from .base import AnimatedWidget, ClickableWidget
from kivy.clock import Clock

Builder.load_file(os.path.join(os.path.dirname(__file__), 'kv', 'card.kv'))


class BorderLessCard(BoxLayout, AnimatedWidget, ClickableWidget):
    border_radius = NumericProperty(0)
    background_color = ListProperty(style.BACKGROUND_COLOR_LIGHT)
    orientation = StringProperty('vertical')

    def _get_diameter(self):
        return self.border_radius * 2

    _diameter = AliasProperty(_get_diameter, None, bind=('border_radius',))


class Card(BorderLessCard):
    border_color = ListProperty([0.48, 0.55, 0.59, 0.5])
    border_width = NumericProperty(1)


class TitledCard(BorderLessCard):
    border_color = ListProperty([0.48, 0.55, 0.59, 0.5])
    border_width = NumericProperty(1)
    title = StringProperty('')
    title_icon = StringProperty('')

    def __init__(self, **kwargs):
        super(TitledCard, self).__init__(**kwargs)
        self.size_hint_y = None
        Clock.schedule_once(self._process_icon)

    def _process_icon(self, *args):
        if self.title_icon != '':
            self.ids.icon.text = u"{}".format(md_icons[self.title_icon])
        else:
            self.ids.icon.width = 0
            self.ids.icon.parent.padding = [10, 0, 0, 0]

Factory.register('Card', cls=Card)
Factory.register('TitledCard', cls=TitledCard)
