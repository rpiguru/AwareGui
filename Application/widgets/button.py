# -*- coding: iso8859-15 -*-

import os
from kivy.animation import Animation
from assets.styles import defaultstyle
from kivy.factory import Factory
from kivy.lang import Builder
from kivy.properties import StringProperty, ListProperty, NumericProperty
from kivy.uix.behaviors import ButtonBehavior
from kivy.uix.button import Button
from widgets.base import AnimatedWidget, ClickableWidget
from kivy.uix.widget import Widget
from kivy.vector import Vector

Builder.load_file(os.path.join(os.path.dirname(__file__), 'kv', 'button.kv'))


class AwareButton(Button, AnimatedWidget):
    pass


class WhiteButton(AwareButton, AnimatedWidget):
    pass


class QuietButton(Button, AnimatedWidget, ClickableWidget):
    def __init__(self, **kwargs):
        super(QuietButton, self).__init__(**kwargs)
        self.register_event_type('on_release')

    def on_touch_up(self, touch):
        if self.collide_point(touch.x, touch.y):
            self.dispatch('on_release')

    def on_touch_move(self, touch):
        pass

    def on_release(self):
        pass


class CircularButton(ButtonBehavior, Widget):

    text = StringProperty('')
    text_color = ListProperty([.1, .1, .1, 1])
    background_color = ListProperty(defaultstyle.COLOR_2)

    def collide_point(self, x, y):
        return Vector(x, y).distance(self.center) <= self.width / 2


class AwareTabButton(Button, AnimatedWidget):

    _line_width = NumericProperty(0)
    tabbed_color = ListProperty(defaultstyle.BACKGROUND_COLOR_LIGHT)
    untabbed_color = ListProperty(defaultstyle.BACKGROUND_COLOR_LIGHT_LIGHT)

    def __init__(self, **kwargs):
        super(AwareTabButton, self).__init__(**kwargs)
        self.on_tabbed(False)

    def on_tabbed(self, val):
        if val:
            self.background_color = self.tabbed_color
            Animation(_line_width=self.width, duration=.2, t='out_quad').start(self)
        else:
            self.background_color = self.untabbed_color
            Animation(_line_width=0, duration=.2, t='out_quad').start(self)


Factory.register('AwareButton', cls=AwareButton)
Factory.register('WhiteButton', cls=WhiteButton)
Factory.register('QuietButton', cls=WhiteButton)
Factory.register('CircularButton', cls=CircularButton)
