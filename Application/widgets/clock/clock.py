# -*- coding: iso8859-15 -*-

import os

from kivy.app import App
from kivy.properties import BooleanProperty
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.widget import Widget
from kivy.graphics import Color, Line
from kivy.uix.floatlayout import FloatLayout
from math import cos, sin, pi
from kivy.clock import Clock
from kivy.lang import Builder

from utils.time_util import get_local_time

Builder.load_file(os.path.join(os.path.dirname(__file__), 'watch.kv'))


class MyClockWidget(FloatLayout):

    enable_second_hand = BooleanProperty(True)
    enable_string = BooleanProperty(True)

    def start_clock(self):
        Clock.schedule_interval(self.show_date_time, 1)

    def stop_clock(self):
        Clock.unschedule(self.ticks.update_clock)

    def show_date_time(self, *args):
        cur_time = get_local_time()
        self.ticks.update_clock(cur_time)
        if self.enable_string:
            self.ids.lb_date.text = cur_time.strftime('%m/%d/%Y %a')
            self.ids.lb_time.text = cur_time.strftime('%H:%M:%S')


class Ticks(Widget):

    enable_second_hand = BooleanProperty(True)

    def update_clock(self, time):
        self.canvas.clear()
        with self.canvas:
            if self.enable_second_hand:
                Color(0.2, 0.5, 0.2)
                Line(points=[self.center_x, self.center_y, self.center_x + .9 * self.r * sin(pi / 30 * time.second),
                             self.center_y + .9 * self.r * cos(pi / 30 * time.second)], width=2)
            Color(0.3, 0.6, 0.3)
            Line(points=[self.center_x, self.center_y, self.center_x + .7 * self.r * sin(pi / 30 * time.minute),
                         self.center_y + .7 * self.r * cos(pi / 30 * time.minute)], width=4)
            Color(0.4, 0.7, 0.4)
            th = time.hour * 60 + time.minute
            Line(points=[self.center_x, self.center_y, self.center_x + .5 * self.r * sin(pi / 360 * th),
                         self.center_y + .5 * self.r * cos(pi / 360 * th)], width=6)


class MyClockApp(App):
    def build(self):
        box = BoxLayout()
        box.add_widget(BoxLayout())
        clock = MyClockWidget()
        clock.start_clock()
        box.add_widget(clock)
        return box


if __name__ == '__main__':
    MyClockApp().run()
