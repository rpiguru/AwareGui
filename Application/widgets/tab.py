# -*- coding: iso8859-15 -*-

import os
from functools import partial
from kivy.lang import Builder
from kivy.properties import ListProperty
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.scrollview import ScrollView
from kivy.clock import Clock
from widgets.button import AwareTabButton

Builder.load_file(os.path.join(os.path.dirname(__file__), 'kv', 'tab.kv'))


class ScrolledTabPanel(ScrollView):

    labels = ListProperty()

    cur_tab = None

    def __init__(self, **kwargs):
        super(ScrolledTabPanel, self).__init__(**kwargs)
        Clock.schedule_once(self._add_tabs)
        self.register_event_type('on_tabbed')

    def _add_tabs(self, *args):
        self.clear_widgets()
        ly = BoxLayout(size_hint=(None, None), size=(0, self.height), spacing=2, padding=2)
        for lb in self.labels:
            btn = CustomTabButton(text=lb)
            btn.bind(on_release=partial(self._on_tab_pressed, btn))
            ly.add_widget(btn)
            ly.width += (btn.width + 25)
        self.add_widget(ly)
        ly.children[-1].dispatch('on_release')

    def _on_tab_pressed(self, pressed_btn, *args):
        pressed_btn.on_tabbed(True)
        if self.cur_tab is not None:
            self.cur_tab.on_tabbed(False)
        self.cur_tab = pressed_btn
        self.dispatch('on_tabbed')

    def get_tabbed_name(self):
        return self.cur_tab.text

    def on_tabbed(self, *args):
        pass


class CustomTabButton(AwareTabButton):

    def __init__(self, **kwargs):
        super(CustomTabButton, self).__init__(**kwargs)
        Clock.schedule_once(self._init)

    def _init(self, *args):
        self.width = len(self.text) * 8.5
