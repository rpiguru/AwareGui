# -*- coding: iso8859-15 -*-

import os
import re
from kivy.core.window import Window
from kivy.factory import Factory
from kivy.lang import Builder
from kivy.properties import StringProperty, BooleanProperty, NumericProperty
from kivy.uix.textinput import TextInput
from kivymd.textfields import MDTextField
from widgets.dialog import InputDialog, IPInputDialog

from .base import DefaultInput, LabeledInputBase, AnimatedWidget
from kivy.clock import Clock


Builder.load_file(os.path.join(os.path.dirname(__file__), 'kv', 'input.kv'))


class AwareTextInput(TextInput, DefaultInput, AnimatedWidget):

    def mark_as_normal(self):
        pass

    def custom_error_check(self):
        pass

    def mark_as_error(self):
        pass

    max_length = NumericProperty(100)

    def insert_text(self, substring, from_undo=False):
        super(AwareTextInput, self).insert_text(substring, from_undo)
        self.text = self.text.lstrip()[:self.max_length]

    def on_text_validate(self):
        next_widget = self._get_focus_next('focus_next')
        if next_widget is not None:
            next_widget.focus = True

    def _get_value(self):
        return self.text

    def set_value(self, value):
        self.text = value


class LabeledTextInputBase(LabeledInputBase):
    hint_text = StringProperty('')
    password = BooleanProperty(False)


class LabeledTextInput(LabeledTextInputBase):
    pass


class AwareDateInput(AwareTextInput):
    def mark_as_normal(self):
        pass

    def custom_error_check(self):
        pass

    def mark_as_error(self):
        pass

    hint_text = StringProperty('MM/DD/YYYY')

    def insert_text(self, substring, from_undo=False):
        """
        Format date input
        :param substring:
        :param from_undo:
        :return:
        """
        super(AwareDateInput, self).insert_text(substring, from_undo)
        digits = re.findall(r'\d+', self.text)
        digits = ''.join(digits)
        formated_string = ''

        i = 0
        for c in self.hint_text:
            if str.isalnum(c):
                if i == len(digits):
                    break
                formated_string += digits[i]
                i += 1
            else:
                formated_string += c
        self.text = formated_string
        self.cursor = (len(formated_string), 0)

    def _get_value(self):
        data = re.findall(r'\d+', self.text)
        # convert date from mm-dd-yyyy to yyyy-mm-dd format
        date = []
        if len(data) > 0:
            date.append(data[0])

        if len(data) > 1:
            date.append(data[1])
            date[0], date[1] = date[1], date[0]

        if len(data) > 2:
            date.append(data[2])
        return '-'.join(date[::-1]) if date else ''


class LabeledDateInput(LabeledTextInputBase):
    hint_text = StringProperty('MM/DD/YYYY')

    def _get_value(self):  # TODO looks like unreachable code
        data = super(LabeledDateInput, self)._get_value()
        data = re.findall(r'\d+', data)
        # convert date from mm-dd-yyyy to yyyy-mm-dd format
        date = []
        if len(data) > 0:
            date.append(data[0])

        if len(data) > 1:
            date.append(data[1])
            date[0], date[1] = date[1], date[0]

        if len(data) > 2:
            date.append(data[2])

        return '-'.join(date[::-1]) if date else ''


class AwareMDTextField(MDTextField):
    ignore_clear = BooleanProperty(False)
    ignore_validate = BooleanProperty(False)
    use_dlg = BooleanProperty(False)

    def __init__(self, **kwargs):
        super(AwareMDTextField, self).__init__(**kwargs)
        Clock.schedule_once(self.bind_dlg)

    def bind_dlg(self, *args):
        if self.use_dlg:
            self.bind(focus=self.show_dlg)

    def show_dlg(self, inst, val):
        if val and not self.disabled:
            Window.release_all_keyboards()
            dlg = InputDialog(text=self.text, hint_text=self.hint_text)
            dlg.bind(on_confirm=self.on_confirm)
            dlg.open()

    def on_confirm(self, *args):
        self.text = args[1]
        self.dispatch('on_text_validate')


class AwareIPMDTextField(AwareMDTextField):

    def show_dlg(self, inst, val):
        if val and not self.disabled:
            Window.release_all_keyboards()
            dlg = IPInputDialog(text=self.text, hint_text=self.hint_text)
            dlg.bind(on_confirm=self.on_confirm)
            dlg.open()


class AwareIPInput(MDTextField):

    key = StringProperty('')

    def mark_as_normal(self):
        pass

    def custom_error_check(self):
        errors = []
        if len(self.text) == 0:
            return errors
        for val in self.text.split('.'):
            try:
                if int(val) > 255:
                    errors.append('Invalid IP address value')
                    break
            except ValueError:
                errors.append('Invalid IP address value')
                break

        if len(self.text.split('.')) != 4:
            errors.append('Invalid structure')

        return errors

    def mark_as_error(self):
        pass

    hint_text = StringProperty('255.255.255.255')
    max_length = NumericProperty(15)

    def insert_text(self, substring, from_undo=False):
        """
        Format date input
        :param substring:
        :param from_undo:
        :return:
        """
        if substring == '.':
            if self.text.count('.') >= 3:
                return
            elif str(self.text[-1]) != '.':
                super(AwareIPInput, self).insert_text(substring, from_undo)
                return
            else:
                return
        elif not str.isdigit(substring):
            return
        elif len(self.text.split('.')[-1]) == 3:
            return
        super(AwareIPInput, self).insert_text(substring, from_undo)
        self.cursor = (len(self.text), 0)

    def get_value(self):
        return {self.key: self.text}


class LabeledIPInput(LabeledTextInputBase):

    def custom_error_check(self):
        return self.ids.input.custom_error_check()


Factory.register('AwareTextInput', cls=AwareTextInput)
Factory.register('LabeledTextInput', cls=LabeledTextInput)
Factory.register('AwareDateInput', cls=AwareDateInput)
Factory.register('LabeledDateInput', cls=LabeledDateInput)
Factory.register('AwareIPInput', cls=AwareIPInput)
Factory.register('LabeledIPInput', cls=LabeledIPInput)
Factory.register('AwareMDTextField', cls=AwareMDTextField)
Factory.register('AwareIPMDTextField', cls=AwareIPMDTextField)
