# -*- coding: iso8859-15 -*-

import os
from kivy.lang import Builder
from kivy.properties import StringProperty, NumericProperty, Clock
from kivy.uix.popup import Popup
import utils.net


Builder.load_file(os.path.join(os.path.dirname(__file__), 'kv', 'popup.kv'))


class ConfirmPopup(Popup):

    label_text = StringProperty('')

    def __init__(self, **kwargs):
        self.register_event_type('on_confirm')
        super(ConfirmPopup, self).__init__(**kwargs)

    def on_confirm(self, *args):
        pass

    def on_yes(self, *args):
        self.dismiss()
        self.dispatch('on_confirm', *args)


class PasswordPopup(Popup):

    label_text = StringProperty('')

    def __init__(self, **kwargs):
        self.register_event_type('on_confirm')
        super(PasswordPopup, self).__init__(**kwargs)

    def on_confirm(self, *args):
        pass

    def on_yes(self, *args):
        self.ids.btn_yes.disabled = True
        self.ids.btn_no.disabled = True
        self.dismiss()
        pwd = self.ids.txt_pwd.text
        self.dispatch('on_confirm', pwd, *args)


class CautionPopup(Popup):

    def __init(self, **kwargs):
        super(CautionPopup, self).__init__(**kwargs)


class WiFiDetailPopup(Popup):
    auto_dismiss = True
    ssid = StringProperty('')
    quality = NumericProperty(0)
    ip = StringProperty('')
    mac_addr = StringProperty('')

    def on_open(self):
        Clock.schedule_once(self.get_wifi_detail)

    def get_wifi_detail(self, *args):
        wifi_detail = utils.net.get_detail_of_connected_net()
        self.ssid = wifi_detail['ssid']
        self.quality = int(wifi_detail['quality'])
        self.ip = wifi_detail['ip']
        self.mac_addr = wifi_detail['mac']


class FullLogoPopup(Popup):

    title_size = 0

    def on_touch_down(self, touch):
        if self.collide_point(*touch.pos):
            self.dismiss()
