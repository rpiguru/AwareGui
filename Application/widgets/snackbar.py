# -*- coding: iso8859-15 -*-

from kivymd.snackbar import Snackbar


class ErrorSnackbar(Snackbar):
    background_color = (.8, 0, .3, .5)
