# -*- coding: iso8859-15 -*-

import os
from aware_api.common import add_unit_to_sensor_value
from kivy.clock import Clock
from kivy.lang import Builder
from kivy.properties import StringProperty, ListProperty
from kivy.uix.boxlayout import BoxLayout
from kivymd.card import MDSeparator

Builder.load_file(os.path.join(os.path.dirname(__file__), 'kv', 'sensor_detail.kv'))


class SensorDetailWidget(BoxLayout):

    data = ListProperty()
    sensor_name = StringProperty('')

    def __init__(self, **kwargs):
        super(SensorDetailWidget, self).__init__(**kwargs)
        Clock.schedule_once(self._update_me)

    def _update_me(self, *args):
        self.ids.ml.add_widget(MDSeparator(height=1))
        for item in self.data:
            val = item['value']
            if type(val) == list:
                val = ','.join([str(i) for i in val[:-1]]) if len(val) > 1 else str(val[0])
            self.ids.ml.add_widget(DeviceWidget(sensor_name=self.sensor_name, address=item['address'], value=val,
                                                timestamp=item['last_seen'].isoformat(),
                                                device_name=item['device_name']))
            self.ids.ml.add_widget(MDSeparator(height=1))


class DeviceWidget(BoxLayout):

    sensor_name = StringProperty('')
    address = StringProperty('')
    timestamp = StringProperty('')
    value = StringProperty('')
    device_name = StringProperty('')

    def __init__(self, **kwargs):
        super(DeviceWidget, self).__init__(**kwargs)
        self.value = add_unit_to_sensor_value(self.sensor_name, self.value)
