# -*- coding: iso8859-15 -*-

import os
from aware_api.common import exclusive_sensor_fields, add_unit_to_sensor_value
from kivy.clock import Clock
from kivy.lang import Builder
from kivy.properties import DictProperty, StringProperty, NumericProperty
from kivy.uix.boxlayout import BoxLayout
from kivymd.card import MDSeparator

Builder.load_file(os.path.join(os.path.dirname(__file__), 'kv', 'device_detail.kv'))


class DeviceDetailWidget(BoxLayout):

    data = DictProperty()

    def __init__(self, **kwargs):
        super(DeviceDetailWidget, self).__init__(**kwargs)
        Clock.schedule_once(self._update_me)

    def _update_me(self, *args):
        self.ids.ml.add_widget(MDSeparator(height=1))
        for _key in self.data.keys():
            if _key not in exclusive_sensor_fields:
                vals = self.data[_key]
                desc = vals[-1] if len(vals) > 1 else ''
                val = ','.join([str(i) for i in vals[:-1]]) if len(vals) > 1 else str(vals[0])
                if len(desc) > 0:
                    desc = '({})'.format(desc)
                self.ids.ml.add_widget(SensorWidget(title=_key, desc=desc, value=val))
            elif _key == 'last_seen':
                self.ids.ml.add_widget(SensorWidget(title='Last Seen', value=self.data[_key].isoformat(),
                                                    title_width=.3))
            self.ids.ml.add_widget(MDSeparator(height=1))


class SensorWidget(BoxLayout):

    title = StringProperty('')
    desc = StringProperty('')
    value = StringProperty('')
    title_width = NumericProperty(2.5)

    def __init__(self, **kwargs):
        super(SensorWidget, self).__init__(**kwargs)
        self.value = add_unit_to_sensor_value(self.title, self.value)
