# -*- coding: iso8859-15 -*-

import os
from kivy.uix.gridlayout import GridLayout

from kivy.factory import Factory
from functools import partial
from kivy.clock import Clock
from kivy.lang import Builder
from kivy.properties import StringProperty, DictProperty, BooleanProperty, ListProperty
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.checkbox import CheckBox

from widgets.base import DefaultInput, AnimatedWidget


Builder.load_file(os.path.join(os.path.dirname(__file__), 'kv', 'checkbox.kv'))


class AwareCheckBox(CheckBox, DefaultInput, AnimatedWidget):
    def custom_error_check(self):
        pass

    def set_value(self, value):
        pass

    background_checkbox_normal = StringProperty('assets/images/checkbox/checkbox_off.png')

    background_checkbox_down = StringProperty('assets/images/checkbox/checkbox_on.png')

    background_radio_normal = StringProperty('assets/images/checkbox/checkbox_radio_off.png')

    background_radio_down = StringProperty('assets/images/checkbox/checkbox_radio_on.png')

    background_radio_disabled_normal = StringProperty('assets/images/checkbox/checkbox_radio_disabled_off.png')

    background_radio_disabled_down = StringProperty('assets/images/checkbox/checkbox_radio_disabled_on.png')

    def __init__(self, **kwargs):
        self.register_event_type('on_change')
        super(AwareCheckBox, self).__init__(**kwargs)
        self.bind(active=self._on_active_changed)

    def _on_active_changed(self, *args):
        self.dispatch('on_change')

    def mark_as_error(self):
        self.background_radio_normal = 'assets/images/checkbox/checkbox_radio_off_error.png'
        self.background_checkbox_normal = 'assets/images/checkbox/checkbox_off_error.png'

    def mark_as_normal(self):
        self.background_radio_normal = 'assets/images/checkbox/checkbox_radio_off.png'
        self.background_checkbox_normal = 'assets/images/checkbox/checkbox_off.png'

    def _get_value(self):
        return self.active

    def on_change(self):
        pass


class CheckBoxGroup(BoxLayout, DefaultInput, AnimatedWidget):
    values = DictProperty({})

    radio = BooleanProperty(False)
    """Use as Radio Group widget
    Do not forget to set _key property
    """

    order = ListProperty([])
    """Set custom choices order
    """

    _key = StringProperty(allownone=True)
    """if we use this widget as part of the other widget we need to set
    this to unique value, because it used as radio group property
    Required only if radio == True
    """

    def __init__(self, **kwargs):
        self.register_event_type('on_change')
        super(CheckBoxGroup, self).__init__(**kwargs)
        self.bind(values=self._update_values)
        self.bind(key=self._update_values)
        self.bind(_key=self._update_values)
        self.bind(order=self._update_values)
        self.bind(radio=self._update_values)

    def _update_values(self, obj, values):
        self.clear_widgets()
        self.ids = {}
        keys = self.order if self.order else self.values.keys()
        for key in keys:
            node = BoxLayout(size_hint_y=None, height=45, padding=(10, 0, 10, 0))

            checkbox = AwareCheckBox(size_hint_x=None, width=50)
            checkbox.id = key
            if self.radio:
                checkbox.group = str(self.key if self.key else self._key)

            checkbox.size_hint_x = None
            checkbox.width = 50
            self.ids.update({key: checkbox})

            p = Factory.get('P')()
            p.text = self.values[key]

            node.add_widget(checkbox)
            node.add_widget(p)
            self.add_widget(node)

    def on_change(self, *args, **kwargs):
        pass

    @property
    def checkboxes(self):
        for key in self.values.keys():
            widget = getattr(self.ids, key)
            if widget:
                yield widget

    def mark_as_error(self):
        for cb in self.checkboxes:
            cb.mark_as_error()

    def mark_as_normal(self):
        for cb in self.checkboxes:
            cb.mark_as_normal()

    def _get_value(self):
        if self.radio:
            for cb in self.checkboxes:
                if cb._get_value():
                    return cb.id

            return None

        else:
            result = {}
            for cb in self.checkboxes:
                result.update({cb.id: cb._get_value()})

            return result

    def on_touch_down(self, touch):
        """
        We use schedule_once because on_change handler called before
        value has been changed
        :return:
        """
        if self.collide_point(touch.x, touch.y):
            Clock.schedule_once(partial(self.dispatch, 'on_change'))
            self.clear_errors()

        super(CheckBoxGroup, self).on_touch_down(touch)


class LabeledCheckbox(GridLayout):

    group = StringProperty('')
    allow_no_selection = BooleanProperty(True)
    text = StringProperty('')

    def __init__(self, **kwargs):
        self.register_event_type('on_checked')
        super(LabeledCheckbox, self).__init__(**kwargs)
        Clock.schedule_once(self.bind_chk)

    def bind_chk(self, *args):
        self.ids.chk.bind(active=self.on_released)

    def on_released(self, *args):
        self.dispatch('on_checked', self.ids.chk.active)

    def on_checked(self, *args):
        pass

    def set_value(self, val):
        self.ids.chk.active = val

    def get_value(self):
        return self.ids.chk.active

    def enable(self, value):
        self.ids.chk.disabled = not value


Factory.register('AwareCheckBox', cls=AwareCheckBox)
Factory.register('CheckBoxGroup', cls=CheckBoxGroup)
Factory.register('LabeledCheckbox', cls=LabeledCheckbox)
