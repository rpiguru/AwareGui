(function() {
    'use strict';

    // Declare core application module which pulls all the components together
    angular.module('aware', [
        'ngAnimate',
        'ngRoute',
        'ngSanitize',
        'ngTouch',
        'ui.router',
        'ui.bootstrap',
        'ui.toggle',
        'ui-notification',
        'angular-electron',
        'ui.select',
        'ae-datetimepicker',
        'ui.bootstrap.datetimepicker',
        'ngAria',
        'ngMaterial',
        'material.components.keyboard',
        'ngDialog',
        'checklist-model'
    ]);
})();