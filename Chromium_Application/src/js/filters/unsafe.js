(function() {
    'use strict';
    var activeAnimation = 0;
    angular.module('aware')
        .filter('unsafe', function($sce) {
            return function(val) {
                return $sce.trustAsHtml(val);
            };
        });
})();