(function() {
    'use strict';

    angular.module('aware')
        .config(function($stateProvider, $urlRouterProvider) {
            $stateProvider.
            state('dashboard', {
                url: '/dashboard',
                templateUrl: './templates/dashboard.html',
                controller: 'DashboardController',
                controllerAs: 'vm',
                abstract: true,
                redirectTo: 'dashboard.menu'
            }).state('dashboard.menu', {
                url: '/menu',
                templateUrl: './templates/menu.html',
                controller: 'MenuController',
                controllerAs: 'vm'
            }).state('dashboard.awareservice', {
                url: '/awareservice',
                templateUrl: './templates/aware-service.html',
                controller: 'AwareServiceController',
                controllerAs: 'vm'
            }).state('dashboard.hubstatus', {
                url: '/hubstatus',
                templateUrl: './templates/hub-status.html',
                controller: 'HubStatusController',
                controllerAs: 'vm'
            }).state('dashboard.clock', {
                url: '/clock',
                templateUrl: './templates/clock.html',
                controller: 'ClockController',
                controllerAs: 'vm'
            }).state('dashboard.weather', {
                url: '/weather',
                templateUrl: './templates/weather.html',
                controller: 'WeatherController',
                controllerAs: 'vm'
            }).state('dashboard.sensors', {
                url: '/sensors',
                templateUrl: './templates/sensors.html',
                controller: 'SensorsController',
                controllerAs: 'vm',
            }).state('dashboard.settings', {
                url: '/settings',
                templateUrl: './templates/settings.html',
                controller: 'SettingsController',
                controllerAs: 'vm',
                abstract: false,
                redirectTo: 'dashboard.settings.general'
            }).state('dashboard.settings.general', {
                url: '/general',
                templateUrl: './templates/settings-general.html',
                controller: 'SettingsGeneralController',
                controllerAs: 'vm'
            }).state('dashboard.settings.ethernet', {
                url: '/ethernet',
                templateUrl: './templates/settings-ethernet.html',
                controller: 'SettingsEthernetController',
                controllerAs: 'vm'
            }).state('dashboard.settings.wifi', {
                url: '/wifi',
                templateUrl: './templates/settings-wifi.html',
                controller: 'SettingsWifiController',
                controllerAs: 'vm'
            }).state('dashboard.settings.time', {
                url: '/time',
                templateUrl: './templates/settings-time.html',
                controller: 'SettingsTimeController',
                controllerAs: 'vm'
            });

            $urlRouterProvider.when('', '/dashboard/menu');
        });
})();