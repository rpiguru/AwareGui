(function() {
    'use strict';
    var activeAnimation = 0;
    angular.module('aware')
        .directive('gauge', ['$timeout','$log', function($timeout,$log) {
            return {
                restrict: 'AEC',
                templateUrl: './templates/gauge-directive.html',
                scope: {
                    gauge: '=gauge',
                    gaugeText: '=gaugeText'
                },
                controller: function($scope) {
                   
                },
                link: function(scope, element, attr) {

                    updateNeedle(attr.gauge);

                    scope.$watch('gauge', function(value) {
                        updateNeedle(value);
                    });

                    element.addClass('gauge-container');

                    var ele = element.find('img');

                    function updateNeedle(value) {
                        //Animation	
                        //$(ele).css({'transform': 'rotate(-'+ attr.gauge + 'deg)'});
                        $(ele).animate({
                            textIndent: 0
                        }, {
                            step: function() {

                                var needleAngle = ((value * 180) / 100).toFixed() - 90;

                                $(this).css('transform', 'rotate(' + needleAngle + 'deg)');
                            },
                            duration: 500,
                            complete: function() {}
                        });
                    }
                }
            };
        }])
})();