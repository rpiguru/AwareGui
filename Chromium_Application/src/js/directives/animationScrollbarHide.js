(function() {
    'use strict';
    var activeAnimation = 0;
    angular.module('aware')
        .directive('animationScrollbarHide', ['$timeout', function($timeout) {
            return {
                restrict: 'AEC',
                link: function(scope, element, attr) {
                    //Animation Started
                    var ele = element;
                    element.on('animationstart', function(argument) {
                        if (argument.target.className.split(' ')[0] === ele.context.className.split(' ')[0]) {
                            activeAnimation++;
                            $('body').addClass("noScroll");
                        }
                    });

                    //Animation Ended
                    element.on('animationend', function(argument) {
                        activeAnimation--;
                        if (activeAnimation == 0) {
                            $('body').removeClass("noScroll");
                        }
                    });
                }
            };
        }]);

    angular.module('aware')
        .directive('animationEvent', ['$timeout', '$parse', function($timeout, $parse) {
            return {
                restrict: 'AEC',
                link: function(scope, element, attr) {
                    //Animation Started
                    var ele = element;
                    element.on('animationstart', function(argument) {
                        scope.$apply(function() {
                            var func = $parse(attr.onAnimationStart);
                            func(scope);
                        })
                    });

                    //Animation Ended
                    element.on('animationend', function(argument) {
                        $timeout(function() {
                            var func = $parse(attr.onAnimationEnd);
                            func(scope);
                        })


                    });
                }
            };
        }]);
})();