(function() {
    'use strict';

    var isOnline = require('is-online');

    angular.module('aware')
        .controller('HubStatusController', ['$scope', '$log', '$state', '$location', '$timeout', 'helpers', 'systemInfohelper', '$interval', '$rootScope', 'configHelper',

            function($scope, $log, $state, $location, $timeout, helpers, systemInfohelper, $interval, $rootScope, configHelper) {
                $log.log('HubStatusController called .................');
                var vm = this;
                vm.pageClass = 'hub-status-page';
                vm.back = back;
                vm.info = {};
                vm.internetText = "Off";
                vm.tileId = 0;
                vm.showLoading = showLoading;
                vm.hideLoading = hideLoading;

                var tileAnimationTimeout, fetchInterval;

                $scope.$on('$destroy', function() {
                    $interval.cancel(fetchInterval);
                    if (tileAnimationTimeout) {
                        $interval.cancel(tileAnimationTimeout);
                    }
                });


                function showLoading() {
                    if ($state.current.name == 'dashboard.hubstatus') {
                        $rootScope.loading = true;

                        isOnline().then(function(online) {
                            if (online) {
                                vm.internetText = 'On';
                            } else {
                                vm.internetText = "Off";
                            }
                        });

                        systemInfohelper.getIp(function(err, ip) {
                            $timeout(function() {
                                if (err) {
                                    throw err;
                                }
                                vm.info.ipAddress = ip;
                            })
                        });

                        configHelper.getGitCommitDate(function(value) {
                            vm.updated = helpers.getDateTime(value, 2);
                        });

                        configHelper.getGitRevision(function(value) {
                            vm.currentVersion = value;
                        });

                        ini(true);
                    }

                };

                function ini(enableInterval) {

                    systemInfohelper.getAll(function(info) {

                        $timeout(function() {

                            vm.info = info;
                            vm.info.time.uptime = helpers.getPrettyHrs(vm.info.time.uptime);
                            vm.info.cpuTemperature.main = info.cpuTemperature.main.toFixed(1);
                            vm.info.cpuTemperature.mainF = (info.cpuTemperature.main * 9 / 5 + 32).toFixed(1);
                            vm.info.mem.usedPer = ((100 * info.mem.used) / info.mem.total).toFixed();
                            vm.info.mem.used = helpers.formatBytes(info.mem.used);
                            vm.info.mem.total = helpers.formatBytes(info.mem.total);
                            vm.info.mem.free = helpers.formatBytes(info.mem.free);
                            vm.info.currentLoad.all = info.currentLoad.cpus.length * 100;
                            var used = 0;
                            for (var i = 0; i < info.currentLoad.cpus.length; i++) {
                                used = used + info.currentLoad.cpus[i].load;
                            }
                            vm.info.currentLoad.free = (vm.info.currentLoad.all - used).toFixed(1);
                            if (!angular.isDefined(vm.info.ipAddress)) {
                                for (var i = 0; i < vm.info.networkInterfaces.length; i++) {
                                    if (vm.info.networkInterfaces[i].internal == false) {
                                        vm.info.ipAddress = vm.info.networkInterfaces[i].ip4;
                                    }
                                }
                            }
                            var totalUsed = 0;
                            var totalSize = 0;
                            for (var i = 0; i < vm.info.fsSize.length; i++) {
                                totalUsed = totalUsed + vm.info.fsSize[i].used;
                                totalSize = totalSize + vm.info.fsSize[i].size;
                            }

                            vm.info.fsSize.totalUsedPer = ((100 * totalUsed) / totalSize).toFixed();

                            vm.info.fsSize.totalUsed = helpers.formatBytes(totalUsed, 1);
                            vm.info.fsSize.totalSize = helpers.formatBytes(totalSize, 1);
                            vm.info.fsSize.totalFree = helpers.formatBytes(totalSize - totalUsed, 1);
                            vm.info.networkStats.rx_sec = vm.info.networkStats.rx_sec != "-1" ? helpers.formatBytes(vm.info.networkStats.rx_sec, false) + '/s' : '0 Bytes/s';
                            vm.info.networkStats.tx_sec = vm.info.networkStats.tx_sec != "-1" ? helpers.formatBytes(vm.info.networkStats.tx_sec, false) + '/s' : '0 Bytes/s';

                            handleAnimation();
                            if (enableInterval === true) {
                                fetchInterval = $interval(function() {

                                    ini();
                                    if ($rootScope.loading == true) {
                                        handleAnimation();
                                    }

                                }, 2000);
                            }
                        });
                    });
                }

                function hideLoading() {
                    $rootScope.loading = false;
                }

                function handleAnimation() {
                    //$rootScope.loading = false;

                    tileAnimationTimeout = $interval(function() {

                        vm.tileId = vm.tileId + 1;
                        if (vm.tileId === 7) {
                            $interval.cancel(tileAnimationTimeout);
                        }
                    }, 300);
                }

                function init() {


                    systemInfohelper.getTime(function(info) {

                        $timeout(function() {
                            vm.info.time = info.time;
                            vm.info.time.uptime = helpers.getPrettyHrs(vm.info.time.uptime);
                        })
                    });


                    systemInfohelper.getCpuTemperature(function(info) {

                        $timeout(function() {
                            vm.info.cpuTemperature = info.cpuTemperature;
                            vm.info.cpuTemperature.main = info.cpuTemperature.main.toFixed(1);
                            vm.info.cpuTemperature.mainF = (info.cpuTemperature.main * 9 / 5 + 32).toFixed(1);
                        })
                    });

                    systemInfohelper.getMem(function(info) {

                        $timeout(function() {
                            vm.info.mem = info.mem;
                            vm.info.mem.usedPer = ((100 * info.mem.used) / info.mem.total).toFixed();
                            vm.info.mem.used = helpers.formatBytes(info.mem.used);
                            vm.info.mem.total = helpers.formatBytes(info.mem.total);
                            vm.info.mem.free = helpers.formatBytes(info.mem.free);
                        })

                    });

                    systemInfohelper.getCurrentLoad(function(info) {

                        $timeout(function() {
                            vm.info.currentLoad = info.currentLoad;
                            vm.info.currentLoad.all = info.currentLoad.cpus.length * 100;
                            var used = 0;
                            for (var i = 0; i < info.currentLoad.cpus.length; i++) {
                                used = used + info.currentLoad.cpus[i].load;
                            }
                            vm.info.currentLoad.free = (vm.info.currentLoad.all - used).toFixed(1);

                        })
                    });

                    /*
                    systemInfohelper.getNetworkInterfaces(function(info) {

                        $timeout(function() {
                            vm.info.networkInterfaces = info.networkInterfaces;
                            if (!angular.isDefined(vm.info.ipAddress)) {
                                for (var i = 0; i < vm.info.networkInterfaces.length; i++) {
                                    if (vm.info.networkInterfaces[i].internal == false) {
                                        vm.info.ipAddress = vm.info.networkInterfaces[i].ip4;
                                    }
                                }
                            }
                        })
                    });
                    */

                    systemInfohelper.getFsSize(function(info) {

                        $timeout(function() {
                            vm.info.fsSize = info.fsSize;
                            var totalUsed = 0;
                            var totalSize = 0;
                            for (var i = 0; i < vm.info.fsSize.length; i++) {
                                totalUsed = totalUsed + vm.info.fsSize[i].used;
                                totalSize = totalSize + vm.info.fsSize[i].size;
                            }

                            vm.info.fsSize.totalUsedPer = ((100 * totalUsed) / totalSize).toFixed();

                            vm.info.fsSize.totalUsed = helpers.formatBytes(totalUsed, 1);
                            vm.info.fsSize.totalSize = helpers.formatBytes(totalSize, 1);
                            vm.info.fsSize.totalFree = helpers.formatBytes(totalSize - totalUsed, 1);

                        })
                    });


                    systemInfohelper.getNetworkStats(function(info) {
                        $timeout(function() {
                            vm.info.networkStats = info.networkStats;
                            vm.info.networkStats.rx_sec = helpers.formatBytes(vm.info.networkStats.rx_sec, false) + '/s';
                            vm.info.networkStats.tx_sec = helpers.formatBytes(vm.info.networkStats.tx_sec, false) + '/s';
                        });

                    });


                }

                function back() {
                    $location.path('/dashboard/menu');
                }

            }
        ]);
})();