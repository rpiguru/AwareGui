(function() {
    'use strict';

    angular.module('aware')

    .controller('SensorsController', ['$scope', '$log', 'sensorsHelper', 'Notification', '$location', 'ngDialog', '$timeout',
        function($scope, $log, sensorsHelper, Notification, $location, ngDialog, $timeout) {
            $log.log('SensorsController called .................');
            var vm = this;
            vm.pageClass = "sensors-page";
            vm.statusChoises = ['All', 'Only Connected', 'Not Connected'];
            vm.devicesChoises = ['All Devices', 'Selected Devices'];
            vm.metricsChoises = ['All Metrics', 'Selected Metrics', 'BatteryLevel', 'BatteryVoltage', 'Distance', 'Humidity', 'IsOpen', 'Presence', 'Room Temperature', 'Volume'];

            vm.selectedStatusChoise = vm.statusChoises[0]
            vm.selectedDevicesChoise = vm.devicesChoises[0]
            vm.selectedMetricsChoise = vm.metricsChoises[0]

            vm.selectedMetrics = ['BatteryLevel', 'BatteryVoltage', 'Distance', 'Humidity', 'IsOpen', 'Presence', 'Room Temperature', 'Volume'];
            vm.sensors = [];

            vm.back = back;
            vm.onStatusSelected = onStatusSelected;
            vm.onDevicesSelected = onDevicesSelected;
            vm.onMetricsSelected = onMetricsSelected;
            vm.checkStatus = checkStatus;

            init();

            function init() {
                sensorsHelper.getSensorData().then(function(data) {
                    vm.sensors = data.items;
                    vm.devices = data.devices;
                    vm.connectedSensors = data.connectedSensors;
                });
            }

            function checkStatus(value, index) {
                var show = false;
                if (vm.selectedStatusChoise == 'All') {
                    return true;
                } else if (vm.selectedStatusChoise == 'Only Connected') {
                    return value.connected == true;
                } else {
                    return value.connected == false;
                }
            }

            function onStatusSelected(item) {

            }

            function onDevicesSelected(item) {
                if (item === 'All Devices') {
                    $timeout(function() {
                        for (var i = 0; i < vm.sensors.length; i++) {
                            vm.sensors[i].show = vm.sensors[i].data && angular.isArray(vm.sensors[i].data.BatteryLevel) ? true : false;
                        }
                    });

                } else if (item === 'Selected Devices') {
                    var dialog = ngDialog.openConfirm({
                        template: 'templates/device-selection.html',
                        controller: function metricsSelectionController($scope, devices) {
                            var ovm = this;
                            ovm.selected = [];
                            ovm.devices = devices;
                        },
                        controllerAs: 'ovm',
                        overlay: true,
                        closeByDocument: false,
                        closeByEscape: false,
                        resolve: {
                            devices: function depFactory() {
                                return vm.devices;
                            }
                        }
                    }).then(function(result) {
                        var items = angular.copy(result);
                        if (items) {
                            $timeout(function() {
                                for (var i = 0; i < vm.sensors.length; i++) {
                                    vm.sensors[i].show = items.indexOf(vm.sensors[i].source_address) >= 0 ? true : false;
                                }
                            });
                        }
                    });
                }
            }

            function onMetricsSelected(item) {

                if (item === 'All Metrics') {
                    $timeout(function() {
                        vm.selectedMetrics.length = 0;
                        vm.selectedMetrics = vm.selectedMetrics.concat(['BatteryLevel', 'BatteryVoltage', 'Distance', 'Humidity', 'IsOpen', 'Presence', 'Room Temperature', 'Volume']);
                    });

                } else if (item === 'Selected Metrics') {
                    var dialog = ngDialog.openConfirm({
                        template: 'templates/metrics-selection.html',
                        controller: function metricsSelectionController($scope) {
                            var ovm = this;
                            ovm.selected = [];
                            ovm.metrics = ['BatteryLevel', 'BatteryVoltage', 'Distance', 'Humidity', 'IsOpen'];
                        },
                        controllerAs: 'ovm',
                        overlay: true,
                        closeByDocument: false,
                        closeByEscape: false
                    }).then(function(result) {
                        var items = angular.copy(result);
                        if (items) {
                            $timeout(function() {
                                vm.selectedMetrics.length = 0;
                                vm.selectedMetrics = vm.selectedMetrics.concat(items);
                            });
                        }
                    });
                } else {
                    $timeout(function() {
                        vm.selectedMetrics.length = 0;
                        vm.selectedMetrics.push(item);
                    });
                }
            }

            function back() {
                $location.path('/dashboard/menu');
            }
        }
    ]);
})();