(function() {
    'use strict';

    angular.module('aware')

    .controller('SettingsTimeController', ['$scope', '$log', '$timeout', '$interval', 'systemInfohelper', 'helpers',
        function($scope, $log, $timeout, $interval, systemInfohelper, helpers) {
            $log.log('SettingsTimeController called .................');
            var vm = this;
            vm.pageClass = "settings-time-page";
            vm.date = new Date();
            vm.time = new Date();

            vm.dateOptions = {
                format: "yyyy-MM-dd"
            };
            vm.timeOptions = {
                format: "LT"
            };

            vm.isDateOpen = false;
            vm.isTimeOpen = false;
            vm.zoneList = ['US/Aleutian', 'US/Arizona', 'US/Indiana-Starke', 'US/Mountain', 'US/Pacific', 'US/Samoa',
                'US/Hawaii', 'US/Central', 'US/Eastern', 'US/Alaska', 'US/East-Indiana', 'US/Michigan',
                'US/Pacific-New', 'Europe/Podgorica', 'Europe/Oslo', 'Europe/Stockholm', 'Europe/Tiraspol',
                'Europe/Bucharest', 'Europe/Mariehamn', 'Europe/Nicosia', 'Europe/Paris', 'Europe/Amsterdam',
                'Europe/Belgrade', 'Europe/Malta', 'Europe/Lisbon', 'Europe/Budapest', 'Europe/Kiev',
                'Europe/London', 'Europe/Belfast', 'Europe/Monaco', 'Europe/Vatican', 'Europe/Warsaw',
                'Europe/Gibraltar', 'Europe/Istanbul', 'Europe/Astrakhan', 'Europe/Jersey', 'Europe/Andorra',
                'Europe/Uzhgorod', 'Europe/Madrid', 'Europe/Moscow', 'Europe/Isle_of_Man', 'Europe/Chisinau',
                'Europe/Vaduz', 'Europe/San_Marino', 'Europe/Zaporozhye', 'Europe/Vilnius', 'Europe/Simferopol',
                'Europe/Vienna', 'Europe/Tallinn', 'Europe/Guernsey', 'Europe/Athens', 'Europe/Zurich',
                'Europe/Helsinki', 'Europe/Ulyanovsk', 'Europe/Zagreb', 'Europe/Bratislava', 'Europe/Saratov',
                'Europe/Dublin', 'Europe/Sarajevo', 'Europe/Berlin', 'Europe/Skopje', 'Europe/Luxembourg',
                'Europe/Prague', 'Europe/Riga', 'Europe/Sofia', 'Europe/Ljubljana', 'Europe/Copenhagen',
                'Europe/Tirane', 'Europe/Rome', 'Europe/Minsk', 'Europe/Kirov', 'Europe/Kaliningrad',
                'Europe/Samara', 'Europe/Brussels', 'Europe/Volgograd', 'Europe/Busingen'
            ];
            vm.timeZones = systemInfohelper.getTimeZones();
            vm.openCalendar = openCalendar;
            vm.openTime = openTime;
            vm.tileId = 0;

            vm.showLoading = showLoading;
            vm.setDate = setDate;
            vm.setTimeZone = setTimeZone;
            var tileAnimationTimeout;



            $scope.$on('$destroy', function() {
                $interval.cancel(tileAnimationTimeout);
            });

            $timeout(function() {
                showLoading();
            }, 2000)

            function showLoading() {
                init();
            };

            function init() {


                vm.selectedZone = systemInfohelper.getTimeZone();

                tileAnimationTimeout = $interval(function() {
                    vm.tileId = vm.tileId + 1;
                    if (vm.tileId === 2) {
                        $interval.cancel(tileAnimationTimeout);
                    }
                }, 300);

            }

            function openTime(e) {
                e.preventDefault();
                e.stopPropagation();

                vm.isTimeOpen = true;
            }

            function openCalendar() {
                vm.isDateOpen = true;
            }

            function setDate() {
                if (!angular.isDate(vm.time)) {
                    vm.time = new Date(vm.time.toLocaleString());
                }
                if (!angular.isDate(vm.date)) {
                    vm.date = new Date(vm.date.toLocaleString());
                }
                vm.date.setHours(vm.time.getHours());
                vm.date.setMinutes(vm.time.getMinutes());
                vm.date.setSeconds(vm.time.getSeconds());
                var datetime = helpers.getDateTime(vm.date);
                systemInfohelper.setClock(datetime, function(result) {
                    $log.log(result);
                });

            };

            function setTimeZone() {
                systemInfohelper.setTimeZone(vm.selectedZone, function(result) {
                    $log.log(result);
                });

            };
        }
    ]);
})();