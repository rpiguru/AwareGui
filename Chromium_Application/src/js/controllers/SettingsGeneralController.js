(function() {
    'use strict';
    var pam = require('authenticate-pam');

    angular.module('aware')

    .controller('SettingsGeneralController', ['$scope', '$log', 'systemInfohelper', 'remote', 'processHelper', 'ngDialog', 'configHelper', 'Notification', '$rootScope',
        function($scope, $log, systemInfohelper, remote, processHelper, ngDialog, configHelper, Notification, $rootScope) {
            $log.log('SettingsGeneralController called .................');
            var vm = this;
            vm.pageClass = "settings-general-page";
            vm.name = "";
            vm.hostname = "";
            vm.version = "";
            vm.closeApp = closeApp;
            vm.apply = apply;

            init();

            function init() {
                systemInfohelper.getOS(function(info) {
                    vm.hostname = info.os.hostname;
                    $scope.$apply();
                });
                configHelper.getAwareName(function(value) {
                    vm.name = value;
                });

                configHelper.getGitRevision(function(value) {
                    vm.version = value;
                });
            }

            function apply() {
                if (vm.name.length > 0) {
                    configHelper.setAwareName(vm.name, function(result) {
                        $log.log(result);

                        Notification.primary('Aware Name is updated');
                        $rootScope.$broadcast('name-changed');
                    });
                } else {
                    Notification.warning('Invalid Aware Name.');
                }
            }

            function closeApp() {
                var dialog = ngDialog.openConfirm({
                    template: 'templates/login.html',
                    controller: function metricsSelectionController($scope) {
                        var ovm = this;
                        ovm.showpassword = 'password';
                        ovm.password = '';
                    },
                    controllerAs: 'ovm',
                    overlay: true,
                    closeByDocument: false,
                    closeByEscape: false
                }).then(function(password) {
                    if (password) {
                        pam.authenticate('pi', password, function(err) {
                            if (err) {
                                $log.log(err);
                                var dialog1 = ngDialog.open({
                                    template: 'templates/message.html',
                                    controller: function metricsSelectionController($scope, title, message) {
                                        var ovm = this;
                                        ovm.title = title;
                                        ovm.message = message;
                                    },
                                    controllerAs: 'ovm',
                                    overlay: true,
                                    closeByDocument: true,
                                    closeByEscape: true,
                                    resolve: {
                                        title: function depFactory() {
                                            return 'Error';
                                        },
                                        message: function depFactory() {
                                            return 'Invalid Password';
                                        }
                                    }
                                });
                            } else {
                                $log.log("Authenticated!");
                                processHelper.stopService('awaregui_chromium');
                                var window = remote.getCurrentWindow();
                                window.close();
                            }
                        });
                    }
                });

            }
        }



    ]);
})();