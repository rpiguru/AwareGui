(function() {
    'use strict';

    angular.module('aware')

    .controller('SettingsWifiController', ['$scope', '$log', 'wifiHelper', 'dialogsHelper', 'Notification', '$rootScope', '$timeout', '$state', 'ngDialog',
        function($scope, $log, wifiHelper, dialogsHelper, Notification, $rootScope, $timeout, $state, ngDialog) {
            $log.log('SettingsWifiController called .................');
            var vm = this;
            vm.pageClass = "settings-wifi-page";
            vm.networks = [];
            vm.connect = connect;
            vm.password = "";
            vm.showLoading = showLoading;

            $rootScope.$on('wifi-changed', function() {
                init();
            });

            init();

            function showLoading() {
                if ($state.current.name == 'dashboard.settings.wifi') {
                    $rootScope.loading = true;
                }
            };

            function init() {
                $timeout(function() {
                    var connection;
                    wifiHelper.getCurrentConnections(function(connections) {
                        if (angular.isArray(connections) && connections.length > 0) {
                            connection = connections[0];
                        }

                        wifiHelper.getWifiNetworks(function(networks) {
                            if (angular.isArray(networks)) {
                                if (connection != undefined) {
                                    var index;
                                    for (var i = 0; i < networks.length; i++) {
                                        if (networks[i].ssid === connection.ssid) {
                                            index = i;
                                        }
                                    }
                                    if (index != undefined) {
                                        networks.splice(index, 1);
                                    }

                                    networks.splice(0, 0, connection);
                                }
                                vm.networks = networks;
                                $log.log(vm.networks);
                            } else {
                                vm.networks = [];
                            }
                            $rootScope.loading = false;
                            $timeout(function() {
                                $scope.$apply();
                            });

                        });

                    });
                }, 2000);

            }

            function connect(network) {
                if (network.encrypted == 'Open') {
                    wifiHelper.disconnect(function() {
                        wifiHelper.connect(network.ssid, '', function(message) {
                            $log.log(message);
                            if (message === 'Connected') {
                                Notification.primary('Wifi Connected');
                            } else {
                                Notification.error('Wifi connection error : ' + message);
                            }

                        });
                    });
                } else {
                    var dialog = ngDialog.openConfirm({
                        template: 'templates/settings-wifi-password.html',
                        controller: function metricsSelectionController($scope, ssid) {
                            var ovm = this;
                            ovm.showpassword = 'password';
                            ovm.password = '';
                            ovm.ssid = ssid;
                        },
                        controllerAs: 'ovm',
                        overlay: true,
                        closeByDocument: false,
                        closeByEscape: false,
                        resolve: {
                            ssid: function depFactory() {
                                return network.ssid;
                            }
                        }
                    }).then(function(password) {
                        if (password) {
                            vm.password = password;
                            wifiHelper.disconnect(function() {
                                wifiHelper.connect(network.ssid, vm.password, function(message) {
                                    $log.log(message);
                                    if (message === 'Connected') {
                                        wifiHelper.getCurrentConnections(function(connections) {
                                            if (angular.isArray(connections) && connections.length > 0 && connections[0].ssid == network.ssid) {
                                                Notification.primary('Wifi Connected');
                                            } else {
                                                showConnectionError(network);
                                            }

                                        })

                                    } else {
                                        //Notification.error('Wifi connection error : ' + message);
                                        showConnectionError(network);
                                    }
                                });
                            });

                        }

                    });
                }

            }

            function showConnectionError(network) {
                var dialog1 = ngDialog.open({
                    template: 'templates/message.html',
                    controller: function metricsSelectionController($scope, title, message) {
                        var ovm = this;
                        ovm.title = title;
                        ovm.message = message;
                    },
                    controllerAs: 'ovm',
                    overlay: true,
                    closeByDocument: true,
                    closeByEscape: true,
                    resolve: {
                        title: function depFactory() {
                            return 'Info';
                        },
                        message: function depFactory() {
                            return 'Failed to connect to ' + network.ssid + '<br/>Please try again later';
                        }
                    }
                });
            }

        }
    ]);
})();