(function() {
    'use strict';
    var isOnline = require('is-online');

    angular.module('aware')

    .controller('MenuController', ['$scope', '$log', '$state', '$location', '$interval', 'sensorsHelper',
        function($scope, $log, $state, $location, $interval, sensorsHelper) {
            $log.log('MenuController called .................');
            var vm = this;
            vm.pageClass = 'menu-page';
            vm.internetLogoImage = 'images/internet_off.png';
            vm.internetText = 'Off';
            vm.sensorsOnCount = "0";
            vm.sensorsOffCount = "0";
            vm.gotoPage = gotoPage;
            vm.tileId = 0;
            var tileAnimationTimeout;

            $scope.$on('$destroy', function() {
                $interval.cancel(tileAnimationTimeout);
            });

            init();

            function init() {
                tileAnimationTimeout = $interval(function() {
                    vm.tileId = vm.tileId + 1;
                    if (vm.tileId === 8) {
                        $interval.cancel(tileAnimationTimeout);
                    }
                }, 300);

                sensorsHelper.getSensorData().then(function(data) {
                    try {
                        vm.sensorsOnCount = data.connectedSensors.length;
                        vm.sensorsOffCount = data.devices.length - data.connectedSensors.length;
                    } catch (e) {

                    }

                });

                isOnline().then(function(online) {
                    if (online) {
                        vm.internetLogoImage = 'images/internet_on.png';
                        vm.internetText = 'On';
                    }
                });
            }

            function gotoPage(path) {
                //$state.go(state,{},{reload:true});
                $location.path(path);
            }
        }
    ]);
})();