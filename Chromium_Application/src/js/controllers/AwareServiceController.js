(function() {
    'use strict';

    angular.module('aware')

    .controller('AwareServiceController', ['$scope', '$log', '$location', 'processHelper', 'AWARE_SERVICE',
        function($scope, $log, $location, processHelper, AWARE_SERVICE) {
            $log.log('AwareServiceController called .................');
            var vm = this;
            vm.pageClass = 'aware-service-page';
            vm.awareSeriveEnabled = processHelper.isRunning(AWARE_SERVICE);
            vm.back = back;
            vm.awareServiceProcess = '';
            vm.toggleService = toggleService;


            function back() {
                $location.path('/dashboard/menu');
            }

            function init() {
                if (vm.awareSeriveEnabled) {
                    var processes = processHelper.getProcess(AWARE_SERVICE);
                    if (processes.length > 0) {
                        vm.awareServiceProcess = processes[0];
                    }
                }
            }

            function toggleService() {
                if (vm.awareSeriveEnabled === true) {
                    processHelper.startService(AWARE_SERVICE);
                } else {
                    processHelper.stopService(AWARE_SERVICE);
                }
            }
        }
    ]);
})();