(function() {
    'use strict';

    angular.module('aware')

    .controller('SettingsController', ['$scope', '$log', '$location', '$rootScope', '$state', '$stateParams',
        function($scope, $log, $location, $rootScope, $state, $stateParams) {
            $log.log('SettingsController called .................');
            var vm = this;
            vm.pageClass = 'settings-page';
            var len = $state.current.name.split('.').length;
            var tab;
            if (len > 0) {
                tab = $state.current.name.split('.')[len - 1]
            }
            vm.selectedTab = tab ? tab : 'general';
            vm.back = back;
            vm.gotoPage = gotoPage;

            $log.log($state.current.name);
            $log.log($state.current);
            $log.log($stateParams);

            function back() {
                $location.path('/dashboard/menu');
            }

            function gotoPage(path) {
                //$state.go(state,{},{reload:true});
                $location.path(path);
            }
        }
    ]);
})();