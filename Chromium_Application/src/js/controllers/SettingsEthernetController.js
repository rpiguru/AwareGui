(function() {
    'use strict';

    angular.module('aware')

    .controller('SettingsEthernetController', ['$scope', '$log', 'networkHelper', 'Notification', 'ETHERNET_INTERFACE',
        function($scope, $log, networkHelper, Notification, ETHERNET_INTERFACE) {
            $log.log('SettingsEthernetController called .................');
            var vm = this;
            vm.pageClass = "settings-ethernet-page";
            vm.connectionMethods = ['DHCP', 'MANUAL', 'STATIC'];

            vm.ip = "0.0.0.0";
            vm.netmask = "0.0.0.0";
            vm.gateway = "0.0.0.0";
            vm.broadcast = "0.0.0.0";
            vm.network = "0.0.0.0";
            vm.selectedMethod = vm.connectionMethods[0];
            vm.set = set;
            vm.onKeybordShow = onKeybordShow;
            init();

            function init() {
                networkHelper.getInfo(function(config) {

                    $log.log(config);
                    vm.mac = config.mac;
                    vm.name = config.name;
                    vm.ip = config.ip;
                    vm.netmask = config.netmask;
                    vm.gateway = config.gateway;
                    vm.broadcast = config.broadcast;
                    vm.network = config.network;
                    vm.selectedMethod = config.method;
                }, ETHERNET_INTERFACE);
            }

            function onKeybordShow() {
                $log.log(onKeybordShow);
            }

            function set() {
                var config = {};

                config[ETHERNET_INTERFACE] = {
                    auto: true,
                    dhcp: vm.selectedMethod == 'DHCP',
                    ipv4: {
                        address: vm.ip,
                        netmask: vm.netmask,
                        gateway: vm.gateway,
                        network: vm.network,
                        broadcast: vm.broadcast
                            //,dns: '8.8.8.8',
                    }
                };

                networkHelper.save(config);
                Notification.primary('Config Saved');
            }

        }
    ]);
})();