(function() {
    'use strict';

    angular.module('aware')
        .controller('WeatherController', ['$scope', '$log', '$state', '$location', '$interval', 'helpers', 'systemInfohelper', 'weather',
            function($scope, $log, $state, $location, $interval, helpers, systemInfohelper, weather) {
                $log.log('WeatherController called .................');
                var vm = this;
                vm.pageClass = 'weather-page';
                vm.back = back;
                vm.tileId = 0;
                vm.weather = {};
                vm.showLoading = showLoading;
                var tileAnimationTimeout;

                $scope.$on('$destroy', function() {
                    $interval.cancel(tileAnimationTimeout);
                });

                init();

                function showLoading() {
                    tileAnimationTimeout = $interval(function() {
                        vm.tileId = vm.tileId + 1;
                        if (vm.tileId === 2) {
                            $interval.cancel(tileAnimationTimeout);
                        }
                    }, 300);
                };

                function init() {
                    systemInfohelper.getLocation().then(function(loc) {
                        console.log(loc);
                        weather.getWeather(loc).then(function(wdata) {
                            console.log(wdata);
                            vm.weather = wdata;
                            vm.weatherImage = 'images/weather/' + vm.weather.channel.item.condition.text_alt.toLowerCase() + '.png';
                        });
                    });
                }

                function back() {
                    $location.path('/dashboard/menu');
                }

            }
        ]);
})();