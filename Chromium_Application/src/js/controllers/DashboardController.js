(function() {
    'use strict';

    angular.module('aware')

    .controller('DashboardController', ['$scope', '$log', '$filter', 'wifiHelper', '$rootScope', '$timeout', 'configHelper',
        function($scope, $log, $filter, wifiHelper, $rootScope, $timeout, configHelper) {
            $log.log('DashboardController called .................');
            var vm = this;
            var wifiTimeout;
            vm.pageClass = "dashboard-page";
            vm.HHmm = $filter('date')(new Date(), 'HH:mm a');
            vm.wifiStrengthImage = 'images/wifi/0.png';
            vm.wifiLogoImage = 'images/wifi/wifi_disconnected.png';
            vm.wifiStatusText = 'Not connected';
            vm.awareLogoImage = 'images/aware_logo_small.png';
            vm.tileId = 0;
            vm.name = 'Aware';

            $rootScope.$on('wifi-changed', function() {
                if (angular.isDefined(wifiTimeout)) {
                    $timeout.cancel(wifiTimeout);
                }
                wifiTimeout = $timeout(function() {
                    init();
                }, 5000);
            });
            $rootScope.$on('name-changed', function() {
                configHelper.getAwareName(function(value) {
                    vm.name = value;
                });
            });

            init();

            function init() {

                wifiHelper.getCurrentConnections(function(connections) {

                    if (angular.isArray(connections) && connections.length > 0) {
                        $timeout(function() {
                            var connection = connections[0];
                            vm.wifiStrengthImage = 'images/wifi/' + connection.signal_strength + '.png';
                            vm.wifiLogoImage = 'images/wifi/wifi.png';
                            vm.wifiStatusText = 'Connected - ' + connection.ssid;
                        })

                    }

                });

                configHelper.getAwareName(function(value) {
                    vm.name = value;
                });

            }
        }
    ]);
})();