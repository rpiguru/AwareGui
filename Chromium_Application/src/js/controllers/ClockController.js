(function() {
    'use strict';

    angular.module('aware')
        .controller('ClockController', ['$scope', '$log', '$state', '$location', '$interval', 'helpers',
            function($scope, $log, $state, $location, $interval, helpers) {
                $log.log('ClockController called .................');
                var vm = this;
                vm.pageClass = 'clock-page';
                vm.back = back;
                vm.clockSettings = clockSettings;
                var tileAnimationTimeout;
                $scope.$on('$destroy', function() {
                    $interval.cancel(tileAnimationTimeout);
                });

                init();

                function init() {
                    var date = new Date();
                    tileAnimationTimeout = $interval(function() {
                        vm.time = helpers.getTime(new Date()).replace('AM', '').replace('PM', '');
                        var hours = new Date().getHours();
                        vm.time_am_pm = (hours >= 12) ? "PM" : "AM";
                    }, 1000);
                    vm.day = helpers.getDayName(date.getDay());
                    vm.dayOfMonth = helpers.getGetOrdinal(date.getDate()) + " Of " + helpers.getMonthText(date.getMonth());
                }

                function back() {
                    $location.path('/dashboard/menu');
                }

                function clockSettings(path) {
                    $state.transitionTo('dashboard.settings.time');
                }


            }
        ]);
})();