const si = require('systeminformation');
var Q = require("q");
//const promisify = require("promisify-node"); //<-- Require promisify
//const getIP = promisify(require('external-ip')); // <-- And then wrap the library
//const getIP = require('external-ip');
// this usually takes a few seconds
function sysinfo(limit = 100000) {
    var information = {};

    information.time = si.time();

    var promises = [];
    //var deferred = Q.defer();
    var promise1 = si.osInfo().then(function(o) {
        information.os = o;
        return Q(true);
    });
    promises.push(promise1);

    var promise2 = si.cpu().then(function(o) {
        information.cpu = o;
        return Q(true);
    });
    promises.push(promise2);

    var promise3 = si.cpuCurrentspeed().then(function(o) {
        information.cpuCurrentspeed = o;
        return Q(true);
    });
    promises.push(promise3);
    var promise4 = si.cpuTemperature().then(function(o) {
        information.cpuTemperature = o;
        return Q(true);
    });
    promises.push(promise4);
    var promise5 = si.mem().then(function(o) {
        information.mem = o;
        return Q(true);
    });
    promises.push(promise5);
    var promise6 = si.currentLoad().then(function(o) {
        information.currentLoad = o;
        return Q(true);
    });
    promises.push(promise6);
    var promise7 = si.networkInterfaceDefault().then(function(o) {
        information.networkInterfaceDefault = o;
        return Q(true);
    });
    promises.push(promise7);
    var promise8 = si.networkInterfaces().then(function(o) {
        information.networkInterfaces = o;
        return Q(true);
    });
    promises.push(promise8);
    var promise9 = si.networkStats().then(function(o) {
        information.networkStats = o;
        return Q(true);
    });
    promises.push(promise9);
    var promise10 = si.fsSize().then(function(o) {
        information.fsSize = o;
        return Q(true);
    });
    promises.push(promise10);

    console.log(promises);
    return Q.all(promises).then(function() {
        return information;
    });
}

module.exports = sysinfo;