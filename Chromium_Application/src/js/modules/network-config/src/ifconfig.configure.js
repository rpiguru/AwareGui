'use strict';
var _ = require('lodash');
var assert = require('assert');
var fs = require('fs');

module.exports = function(cp) {
    /**
     * Configure a network interface
     * @param  {string} name        interface name
     * @param  {object} description interface definition
     * @param  {function} f(err)
     */
    function configure(config, f) {
        //assert(_.isString(name));
        //assert(_.isPlainObject(description));

        for (var device in config) {
            var description = prepConfig(device, config[device]);

            fs.readFile(configure.FILE, {
                encoding: 'utf8'
            }, function(err, content) {
                if (err) {
                    return f(err);
                }

                fs.writeFile(configure.FILE, replaceInterface(device, content, description), function(err) {
                    if (err) {
                        return f(err);
                    }

                    cp.exec('service networking reload', function(err, __, stderr) {
                        f(err || stderr || null);
                    });
                });

            });
            break;
        }
    }

    configure.FILE = '/etc/network/interfaces';

    return configure;
};


function replaceInterface(name, content, interfaceDescription) {
    return excludeInterface(name, content).trim() + '\n\n' + interfaceDescription + '\n';
}


function excludeInterface(name, content) {
    var without = _.curry(function(name, content) {
        return !_.includes(content, name);
    });

    return _.chain(content)
        .split('\n\n')
        .filter(without(name))
        .join('\n\n').trim();
}

function prepConfig(device, config) {

    var output = [];

    //output.push('auto lo')
    //output.push('iface lo inet loopback')

    output.push('')

    if (config.auto)
        output.push('auto ' + device)

    if (config.dhcp == true)
        output.push('iface ' + device + ' inet dhcp')
    else
        output.push('iface ' + device + ' inet static')

    if (config.wireless) {
        if (config.wireless.ssid)
            output.push('  wpa-ssid ' + config.wireless.ssid)

        if (config.wireless.psk)
            output.push('  wpa-psk ' + config.wireless.psk)
    }

    if (config.ipv4) {
        if (config.ipv4.address)
            output.push('  address ' + config.ipv4.address)

        if (config.ipv4.netmask)
            output.push('  netmask ' + config.ipv4.netmask)

        if (config.ipv4.gateway)
            output.push('  gateway ' + config.ipv4.gateway)

        if (config.ipv4.broadcast)
            output.push('  broadcast ' + config.ipv4.broadcast)

        if (config.ipv4.network)
            output.push('  network ' + config.ipv4.network)

        if (config.ipv4.dns)
            output.push('  dns-nameservers ' + config.ipv4.dns)
    }

    return output.join("\n");
}