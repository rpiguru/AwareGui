(function() {
    'use strict';

    const child_process = require('child_process');
    const fs = require('fs');

    var network = require('./js/modules/network-config');

    angular.module('aware')

    .service('networkHelper', ['$log',
        function($log) {
            $log.log('networkHelper called .................');

            var information = {};

            function getConnectionMethod(callback, intf = 'eth0') {


                fs.readFile('/etc/network/interfaces', {
                    encoding: 'utf8'
                }, function(err, content) {
                    var connectionMethod = '';
                    if (err) {
                        $log.log(err);

                    } else {
                        var data = content.split('/n');
                        for (var i = 0; i < data.length; i++) {
                            data[i] = data[i].replace(/ +(?= )/g, '');
                            data[i] = data[i].trim();
                            var sp = data[i].split(' ');
                            if (sp.indexOf('iface') > -1 && sp.indexOf(intf) > -1 && sp.indexOf('inet') > -1 && sp.indexOf('#') != 0) {
                                connectionMethod = sp[sp.length - 1].toUpperCase();
                                break;
                            }
                        }
                    }

                    if (connectionMethod === '') {
                        getDynamic(callback, intf);
                    } else {
                        callback(connectionMethod);
                    }

                });

                function getDynamic(callback, intf = 'eth0') {
                    getDevice(function(device) {
                        child_process.exec('nmcli con show id "' + device.connection + '"', function(error, stdout, stderr) {
                            var connectionMethod = 'DHCP';
                            if (error == null) {
                                var data = stdout.split('\n');
                                for (var i = 0; i < data.length; i++) {
                                    $log.log(data);
                                    data[i] = data[i].replace(/ +(?= )/g, '');
                                    data[i] = data[i].trim();
                                    var sp = data[i].split(' ');
                                    $log.log(sp);
                                    if (sp[0] == 'ipv4.method:') {
                                        if (sp[1].toLowerCase() == 'auto')
                                            connectionMethod = 'DHCP';
                                        else
                                            connectionMethod = 'STATIC';

                                        break;
                                    }
                                }
                            }
                            callback(connectionMethod);
                        });

                    }, intf);
                }

            }

            function getDevices(callback) {
                child_process.exec('nmcli device status', function(error, stdout, stderr) {
                    var devices = [];
                    if (error == null) {
                        var data = stdout.split('\n');
                        for (var i = 0; i < data.length; i++) {
                            $log.log(data);
                            data[i] = data[i].replace(/ +(?= )/g, '');
                            data[i] = data[i].trim();
                            var sp = data[i].split(' ');
                            $log.log(sp);
                            if (sp.length > 1 && sp[0] != 'DEVICE') {
                                var device = {};
                                device.name = sp[0];
                                device.type = sp[1];
                                device.state = sp[2];
                                device.connection = sp[3];
                                devices.push(device);
                            }
                        }
                    }
                    callback(devices);
                });
            }

            function getDevice(callback, intf = 'eth0') {
                getDevices(function(devices) {
                    for (var i = 0; i < devices.length; i++) {
                        if (devices[i].name == intf) {
                            callback(devices[i]);
                            break;
                        }
                    }
                });
            }

            function getNetworkAddress(callback, intf = 'eth0') {
                child_process.exec('netstat -nr', function(error, stdout, stderr) {
                    var router_addr = '0.0.0.0';
                    if (error == null) {
                        var data = stdout.split('\n');
                        for (var i = 0; i < data.length; i++) {
                            $log.log(data);
                            data[i] = data[i].replace(/ +(?= )/g, '');
                            data[i] = data[i].trim();
                            var sp = data[i].split(' ');
                            $log.log(sp);
                            if (sp[sp.length - 1] == intf && sp[3] == 'U') {
                                router_addr = sp[0];
                                break;
                            }
                        }
                    }
                    callback(router_addr);
                });
            }

            this.getNetworkAddress = getNetworkAddress;

            this.getInfo = function(callback, intf = 'eth0') {
                network.interfaces(function(error, interfaces) {
                    $log.log(interfaces);

                    var info = {};
                    if (error == null) {
                        for (var i = 0; i < interfaces.length; i++) {
                            if (interfaces[i].name == intf) {
                                info = interfaces[i];
                            }
                        }
                    }
                    getNetworkAddress(function(network) {
                        info.network = network;

                        getConnectionMethod(function(method) {
                            info.method = method;
                            callback(info);
                        }, intf)

                    }, intf);

                });
            }

            this.getDevices = getDevices;

            this.restart = function(cb) {
                child_process.exec('/etc/init.d/networking restart', cb);
            }

            this.restore = function(outFile) {
                var src = outFile || '/etc/network/interfaces';
                var dest = src + '.awareBakup';
                checkIfFile(dest, function(err, isFile) {
                    if (isFile) {
                        fs.unlink(src, function(error) {
                            if (error) {
                                $log.log(error);
                            }

                            copyFile(dest, src);

                        });
                    }
                });
            }

            this.save = function(config, outFile) {
                var src = outFile || '/etc/network/interfaces';
                var dest = src + '.awareBackup';

                fs.unlink(dest, function(error) {
                    if (error) {
                        $log.log(error);
                    }

                    copyFile(src, dest);
                    network.configure(config, function(err) {
                        $log.log(err);
                    });
                });

            }

            function copyFile(src, dest) {

                let readStream = fs.createReadStream(src);

                readStream.once('error', (err) => {
                    $log.log(err);
                });

                readStream.once('end', () => {
                    $log.log('done copying');
                });

                readStream.pipe(fs.createWriteStream(dest));
            }

            function checkIfFile(file, cb) {
                fs.stat(file, function fsStat(err, stats) {
                    if (err) {
                        if (err.code === 'ENOENT') {
                            return cb(null, false);
                        } else {
                            return cb(err);
                        }
                    }
                    return cb(null, stats.isFile());
                });
            }
        }
    ]);
})();