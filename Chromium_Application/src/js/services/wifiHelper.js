(function() {
    'use strict';

    var wifi = require('node-wifi');

    angular.module('aware')
        .service('wifiHelper', ['$log', 'helpers', '$rootScope', 'WIFI_INTERFACE',
            function($log, helpers, $rootScope, WIFI_INTERFACE) {
                $log.log('wifiHelper called .................');

                // Initialize wifi module
                // Absolutely necessary even to set interface to null
                wifi.init({
                    iface: WIFI_INTERFACE // network interface, choose a random wifi interface if set to null
                });

                var wifiNetworks = [];

                this.getWifiNetworks = function(callback) {
                    // Scan networks
                    wifi.scan(function(err, networks) {
                        if (err) {
                            $log.log(err);
                            callback(err);

                        } else {


                            networks = setStrength(networks);

                            $log.log(networks);
                            wifiNetworks = networks;
                            callback(networks);
                        }
                    });
                }

                this.connect = function(ssid, password, callback) {

                    // Connect to a network
                    wifi.connect({
                        ssid: ssid,
                        password: password
                    }, function(err) {
                        if (err) {
                            $log.log(err);
                            callback(err);

                        } else {

                            $rootScope.$emit('wifi-changed');

                            $log.log('Connected');
                            callback('Connected');
                        }
                    });
                }

                this.disconnect = function(callback) {

                    // Disconnect from a network
                    // not available on all os for now
                    wifi.disconnect(function(err) {
                        if (err) {
                            $log.log(err);
                            callback(err);
                        } else {

                            $rootScope.$emit('wifi-changed');

                            $log.log('Disconnected');
                            callback('Disconnected');
                        }
                    });

                }

                this.getCurrentConnections = function(callback) {

                    // Disconnect from a network
                    // not available on all os for now
                    wifi.getCurrentConnections(function(err, currentConnections) {
                        if (err) {
                            $log.log(err);
                            callback(err);
                        } else {

                            currentConnections = setStrength(currentConnections);
                            $log.log(currentConnections);
                            callback(currentConnections);
                            /*
                            // you may have several connections
                            [
                                {
                                    iface: '...', // network interface used for the connection, not available on macOS
                                    ssid: '...',
                                    mac: '...',
                                    frequency: <number>, // in MHz
                                    signal_level: <number>, // in dB
                                    security: '...' // not available on linux
                                }
                            ]
                            */
                        }
                    });

                }

                function setStrength(networks) {
                    for (var i = 0; i < networks.length; i++) {
                        var signalStrengthPer = Math.min(Math.max(2 * (networks[i].signal_level + 100), 0), 100);
                        networks[i].encrypted = networks[i].security == "" ? "Open" : "Encrypted";
                        networks[i].signal_strength_per = signalStrengthPer;

                        if (signalStrengthPer <= 0) {
                            networks[i].signal_strength = 0
                        } else if (signalStrengthPer <= 25) {
                            networks[i].signal_strength = 1
                        } else if (signalStrengthPer <= 50) {
                            networks[i].signal_strength = 2
                        } else if (signalStrengthPer <= 75) {
                            networks[i].signal_strength = 3
                        } else {
                            networks[i].signal_strength = 4
                        }
                    }

                    return networks;
                }
            }
        ]);
})();