(function() {
    'use strict';

    var child_process = require('child_process');


    angular.module('aware')

    .service('helpers', ['$log',
        function($log) {
            $log.log('helpers called .................');

            this.prettyMemSize = function(size) {
                return (size / (Math.pow(1024, 3))).toFixed(2);
            }

            this.prettyDiskSize = function(size) {
                return (size / Math.pow(1000, 3)).toFixed(1);
            }

            this.command = function(command) {
                return 'bash -c "${command}"';
            }

            this.formatBytes = function(bytes, decimals = 0) {
                if (bytes == 0) return '0 Bytes';
                var k = 1024,
                    dm = decimals + 1 || 3,
                    sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'],
                    i = Math.floor(Math.log(bytes) / Math.log(k));
                return parseFloat((bytes / Math.pow(k, i)).toFixed(dm)) + ' ' + sizes[i];
            }

            this.getPrettyHrs = function(seconds) {
                var days = Math.floor(seconds / (3600 * 24));
                var hrs = Math.floor(seconds / 3600);
                var mnts = Math.floor((seconds - (hrs * 3600)) / 60);
                var secs = seconds - (hrs * 3600) - (mnts * 60);
                if (days > 0) {
                    return days + ":" + hrs + ':' + mnts + ':' + secs;
                } else {
                    return hrs + ':' + mnts + ':' + secs;
                }
            }

            this.getStringValue = function(o) {
                if (typeof o == 'string') return o;
                return 'Undefined';
            }

            this.getGetOrdinal = function(n) {
                var s = ["th", "st", "nd", "rd"],
                    v = n % 100;
                return n + (s[(v - 20) % 10] || s[v] || s[0]);
            }

            this.getMonthText = function(month) {
                var months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
                return months[month];
            }

            this.getDayName = function(day) {
                var days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];

                return days[day];
            }

            this.getTime = function(d) {
                return d.toLocaleTimeString('en-US', {
                    hour: '2-digit',
                    minute: '2-digit',
                    second: '2-digit',
                    hour12: true
                });
            }

            this.getDateTime = function(d, format) {
                // Parse our locale string to [date, time]
                var date = d.toLocaleString('en-US', {
                    hour12: false,
                }).split(" ");

                // Now we can access our time at date[1], and monthdayyear @ date[0]
                var time = date[1]; //d.toLocaleString('en-US', {hour: '2-digit', minute: '2-digit', second: '2-digit', hour12: true});
                var mdy = date[0];

                // We then parse  the mdy into parts
                mdy = mdy.split('/');
                var month = ("0" + parseInt(mdy[0])).slice(-2);
                var day = ("0" + parseInt(mdy[1])).slice(-2);
                var year = ("0" + parseInt(mdy[2])).slice(-2);
                if(format){
                    return  month + '/' + day + '/' + year + ' ' + time;
                } 
                return year + '-' + month + '-' + day + ' ' + time;
            }


        }
    ]);
})();