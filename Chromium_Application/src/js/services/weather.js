(function() {
    'use strict';

    angular.module('aware')

    .service('weather', function($http, $q,$log) {

        var weather_conditions = [{
            "code": 0,
            "condition": "tornado",
            "alt": "thundershowers"
        }, {
            "code": 1,
            "condition": "tropical storm",
            "alt": "thundershowers"
        }, {
            "code": 2,
            "condition": "hurricane",
            "alt": "thundershowers"
        }, {
            "code": 3,
            "condition": "severe thunderstorms",
            "alt": "thundershowers"
        }, {
            "code": 4,
            "condition": "thunderstorms",
            "alt": "thundershowers"
        }, {
            "code": 5,
            "condition": "mixed rain and snow",
            "alt": "rain"
        }, {
            "code": 6,
            "condition": "mixed rain and sleet",
            "alt": "rain"
        }, {
            "code": 7,
            "condition": "mixed snow and sleet",
            "alt": "snow"
        }, {
            "code": 8,
            "condition": "freezing drizzle",
            "alt": "rain"
        }, {
            "code": 9,
            "condition": "drizzle",
            "alt": "rain"
        }, {
            "code": 10,
            "condition": "freezing rain",
            "alt": "rain"
        }, {
            "code": 11,
            "condition": "rain",
            "alt": "rain"
        }, {
            "code": 12,
            "condition": "showers",
            "alt": "rain"
        }, {
            "code": 13,
            "condition": "snow flurries",
            "alt": "snow"
        }, {
            "code": 14,
            "condition": "light snow showers",
            "alt": "rain"
        }, {
            "code": 15,
            "condition": "blowing snow",
            "alt": "snow"
        }, {
            "code": 16,
            "condition": "snow",
            "alt": "snow"
        }, {
            "code": 17,
            "condition": "hail",
            "alt": "scattered showers"
        }, {
            "code": 18,
            "condition": "sleet",
            "alt": "scattered showers"
        }, {
            "code": 19,
            "condition": "breezy",
            "alt": "breezy"
        }, {
            "code": 19,
            "condition": "dust",
            "alt": "breezy"
        }, {
            "code": 20,
            "condition": "foggy",
            "alt": "breezy"
        }, {
            "code": 21,
            "condition": "haze",
            "alt": "breezy"
        }, {
            "code": 22,
            "condition": "smoky",
            "alt": "breezy"
        }, {
            "code": 23,
            "condition": "blustery",
            "alt": "blustery"
        }, {
            "code": 24,
            "condition": "windy",
            "alt": "windy"
        }, {
            "code": 25,
            "condition": "cold",
            "alt": "breezy"
        }, {
            "code": 26,
            "condition": "cloudy",
            "alt": "cloudy"
        }, {
            "code": 27,
            "condition": "mostly cloudy",
            "alt": "mostly cloudy"
        }, {
            "code": 27,
            "condition": "mostly cloudy (night)",
            "alt": "mostly cloudy"
        }, {
            "code": 28,
            "condition": "mostly cloudy (day)",
            "alt": "mostly cloudy"
        }, {
            "code": 29,
            "condition": "partly cloudy",
            "alt": "partly cloudy"
        }, {
            "code": 29,
            "condition": "partly cloudy (night)",
            "alt": "partly cloudy"
        }, {
            "code": 30,
            "condition": "partly cloudy (day)",
            "alt": "partly cloudy"
        }, {
            "code": 31,
            "condition": "clear",
            "alt": "clear"
        }, {
            "code": 31,
            "condition": "mostly clear",
            "alt": "clear"
        }, {
            "code": 31,
            "condition": "clear (day)",
            "alt": "clear"
        }, {
            "code": 31,
            "condition": "clear (night)",
            "alt": "clear"
        }, {
            "code": 32,
            "condition": "sunny",
            "alt": "sunny"
        }, {
            "code": 32,
            "condition": "mostly sunny",
            "alt": "sunny"
        }, {
            "code": 33,
            "condition": "fair (night)",
            "alt": "fair"
        }, {
            "code": 34,
            "condition": "fair (day)",
            "alt": "fair"
        }, {
            "code": 34,
            "condition": "fair",
            "alt": "fair"
        }, {
            "code": 35,
            "condition": "mixed rain and hail",
            "alt": "rain"
        }, {
            "code": 36,
            "condition": "hot",
            "alt": "sunny"
        }, {
            "code": 37,
            "condition": "isolated thunderstorms",
            "alt": "thunderstorms"
        }, {
            "code": 38,
            "condition": "scattered thunderstorms",
            "alt": "thunderstorms"
        }, {
            "code": 39,
            "condition": "scattered thunderstorms",
            "alt": "thunderstorms"
        }, {
            "code": 40,
            "condition": "scattered showers",
            "alt": "scattered showers"
        }, {
            "code": 41,
            "condition": "heavy snow",
            "alt": "snow"
        }, {
            "code": 42,
            "condition": "scattered snow showers",
            "alt": "snow"
        }, {
            "code": 43,
            "condition": "heavy snow",
            "alt": "snow"
        }, {
            "code": 44,
            "condition": "partly cloudy",
            "alt": "partly cloudy"
        }, {
            "code": 45,
            "condition": "thundershowers",
            "alt": "thundershowers"
        }, {
            "code": 46,
            "condition": "snow showers",
            "alt": "snow"
        }, {
            "code": 47,
            "condition": "isolated thundershowers",
            "alt": "thundershowers"
        }, {
            "code": 3200,
            "condition": "not available",
            "alt": "clear"
        }];

        var getQuery = function(location) {
            var query = 'select * from weather.forecast where woeid in ' +
                '(select woeid from geo.places(1) where text="(lat,lan)")';

            query = query.replace('lat', location.lat)
                .replace('lan', location.lan);
            $log.log(query);
            return query;
        }

        var getUrl = function(location) {
            var baseUrl = 'https://query.yahooapis.com/v1/public/yql?q=';
            var query = encodeURIComponent(getQuery(location));
            var finalUrl = baseUrl + query;
            return finalUrl;
        }

        this.getWeather = function(location) {
            var url = getUrl(location);
            var params = {
                format: 'json',
                env: 'store%3A%2F%2Fdatatables.org%2Falltableswithkeys',
                //callback: 'JSON_CALLBACK'
            }
            var deferred = $q.defer();
            $http({
                method: 'GET',
                url: url,
                params: params
            }).then(function(json) {
                $log.log(json.data);
                //console.log(JSON.parse((json.data));

                if (json.data.query) {
                    json.data.query.results.channel.today = json.data.query.results.channel.item.forecast[0];
                    for (var i = 0; i < weather_conditions.length; i++) {
                        if (weather_conditions[i].condition === json.data.query.results.channel.item.condition.text.toString().toLowerCase()) {
                            json.data.query.results.channel.item.condition.text_alt = weather_conditions[i].alt;
                            break;
                        }
                    }
                }

                deferred.resolve(json.data.query.results);
            }, function(error) {
                $log.log(error);
                deferred.reject(error)
            });
            return deferred.promise;
        }

        this.getWeather1 = function(location) {
            var url = getUrl(location);
            var params = {
                format: 'json',
                env: 'store%3A%2F%2Fdatatables.org%2Falltableswithkeys',
                callback: 'JSON_CALLBACK'
            }
            var deferred = $q.defer();
            $http({
                method: 'JSONP',
                url: url,
                params: params
            }).then(function(json) {
                $log.log(JSON.stringify(json));
                var result = json.query.results;
                deferred.resolve(result);
            }, function(error) {
                $log.log(JSON.stringify(error));
                deferred.reject(JSON.stringify(error))
            });
            return deferred.promise;
        }

    });

})();