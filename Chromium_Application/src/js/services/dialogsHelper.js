(function() {
    'use strict';
    var dialogs = require('./js/modules/dialogs')

    angular.module('aware')

    .factory('dialogsHelper', ['$log', 'ngDialog',
        function($log, ngDialog) {
            $log.log('dialogsHelper called .................');

            var dh = {}; //dialogs();
            dh.prompt = function(text, title, callback) {

                var dialog = ngDialog.openConfirm({
                    template: 'templates/prompt.html',
                    controller: ['$scope', 'text', 'title', function($scope, text, title) {
                        // controller logic
                        $scope.value = "";
                        $scope.text = text;
                        $scope.title = title;
                    }],
                    resolve: {
                        text: function depFactory() {
                            return text;
                        },
                        title: function depFactory() {
                            return title;
                        }
                    },
                    overlay: false,
                    closeByDocument: false,
                    closeByEscape: false
                }).then(function(confirm) {
                    callback(confirm);
                }, function(reject) {
                    callback();
                });

            };

            return dh;
        }
    ]);
})();