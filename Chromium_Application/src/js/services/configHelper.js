(function() {
    'use strict';

    var MongoClient = require('mongodb').MongoClient;
    var fs = require('fs');
    var path = require('path');


    angular.module('aware')

    .service('configHelper', ['$log', '$http','MONGO_COLLECTION','MONGO_HOST','MONGO_PORT',
        function($log, $http,MONGO_COLLECTION,MONGO_HOST,MONGO_PORT) {
            $log.log('configHelper called .................');
            var configdata = {};
            var doc = {};

            this.get_config = get_config;

            this.set_config = set_config;

            this.getAwareName = function(callback) {
                get_config('device_name', 'Aware', function(value) {
                    callback(value);
                })
            }

            this.getAwareTitle = function(callback) {
                get_config('title', 'Aware Hub', function(value) {
                    callback(value);
                })
            }

            this.setAwareName = function(value, callback) {
                set_config('device_name', value, function(value) {
                    callback(value);
                });
            }


            this.getGitCommitDate = function(callback) {
                try {
                    var fpath = path.join(__dirname.replace('/src', ''), '/VERSION');
                    fs.stat(fpath, function(err, stats) {
                        if (err) {
                            return callback('');
                        }
                        $log.log(stats);
                        var mtime = new Date(stats.mtime);
                        $log.log(mtime);
                        callback(mtime);
                    });
                } catch (err) {
                    callback('');
                }
            };

            this.getAppNote = function(callback) {

                $http.get('../ApplicationNotes.txt').then(function(result) {
                    if (result.data) {
                        return callback(result);
                    }
                    return callback('Not Available');
                }, function() {
                    return callback('Not Available');
                });

            }

            this.getGitRevision = function(callback) {

                $http.get('../VERSION').then(function(result) {
                    if (result.data) {
                        var content = result.data.split('\n');
                        for (var i = 0; i < content.length; i++) {
                            if (content[i].trim().startsWith('git revision')) {
                                return callback(content[i].trim().split(' ')[2]);
                            }
                        }
                    } else {
                        return callback('Not Available');
                    }
                }, function() {
                    return callback('Not Available');
                });
            }

            function get_config(option, default_value = undefined, callback) {

                if (configdata['debug']) {
                    return callback(default_value);
                }

                try {
                    find(function(result, db) {
                        doc = result;
                        db.close();
                        if (!doc) {
                            callback(default_value);
                        } else if (!doc[option]) {
                            callback(default_value);
                        } else {
                            callback(doc[option]);
                        }
                    });
                } catch (er) {
                    callback(default_value);
                }


            }

            function set_config(option, value, callback) {
                if (configdata['debug']) {
                    return callback(true);
                }
                try {
                    find(function(result, db) {
                        doc = result;
                        if (!doc) {
                            insert(db, {
                                option: value
                            }, function(result) {
                                db.close();
                                callback(result);
                            });
                        } else {
                            doc[option] = value;
                            var copy = angular.copy(doc);
                            delete copy._id
                            update(db, {
                                '_id': doc['_id']
                            }, {
                                "$set": copy
                            }, function(result) {
                                db.close();
                                callback(result);
                            });
                        }
                    });
                } catch (er) {
                    callback(false);
                }
            }

            init();

            function init() {
                $http.get('../awaregui.config').then(function(result) {
                    configdata = result.data;
                    $log.log(configdata);

                    find(function(result, db) {
                        doc = result;
                        db.close();
                    });
                });
            }

            function find(callback) {

                var url = "mongodb://" + MONGO_HOST + ":" + MONGO_PORT + "/sensor_data";

                MongoClient.connect(url, function(err, db) {
                    if (err) throw err;
                    db.collection(MONGO_COLLECTION).findOne({}, function(err, result) {
                        if (err) {
                            $log.error('Failed to get config from the local mongodb: ' + er);
                        }
                        $log.log(result);
                        callback(result, db);
                    });
                });

            }

            function insert(db, data, callback) {
                db.collection(configdata['mongo_collection']).insertOne(data, function(err, result) {
                    if (err) {
                        $log.error('Failed to insert config to the local mongodb: ' + err);
                        return callback(false);
                    }
                    $log.log(result);
                    callback(true);
                });
            }

            function update(db, query, data, callback) {
                db.collection(configdata['mongo_collection']).updateOne(query, data, function(err, result) {
                    if (err) {
                        $log.error('Failed to update config from the local mongodb: ' + err);
                        return callback(false);
                    }
                    $log.log(result);
                    callback(true);
                });
            }
        }
    ]);
})();