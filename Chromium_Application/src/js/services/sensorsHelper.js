(function() {
    'use strict';

    angular.module('aware')
        .service('sensorsHelper', ['$log', 'helpers', '$http','URL_LAST',
            function($log, helpers, $http, URL_LAST) {
                $log.log('helpers called .................');

                var samplesensordata = '{"items":[{"_id":"596e74dbd4f05674e9f773e0","data":{"BatteryLevel":[94],"BatteryVoltage":[161],"Distance":[41,"Sonar"],"Humidity":[71],"IsOpen":[0],"Presence":[0,"Sonar"],"Room Temperature":[0],"Volume":[42],"last_seen":1502739079,"msg_type":1,"source_address":"0123456789abcdef"},"last_seen":1502739079,"source_address":"0123456789abcdef"},{"_id":"596e74e6d4f05674e9f773e1","data":{"p1":{"d":{"17":{"ov":68},"Load":1.04,"Pending":0,"cdt":"2017-08-14T19:37:00.045598Z","did":"0000000086A880C0","gid":"0000000086A880C0"},"v":2}},"last_seen":1502739430,"msg_type":1,"source_address":"0000000086a880c0"},{"_id":"596e75faf01f4b1c4b44773e","data":{"BatteryLevel":[54],"BatteryVoltage":[2630],"Distance":[74,"Sonar"],"Humidity":[97],"IsOpen":[0],"Presence":[0,"Lidar"],"Room Temperature":[22],"Volume":[35],"last_seen":1502739199,"msg_type":1,"source_address":"23456789abcdef01"},"last_seen":1502739199,"source_address":"23456789abcdef01"},{"_id":"596e76eaf01f4b1c4b447742","data":{"BatteryLevel":[15],"BatteryVoltage":[2397],"Distance":[44,"Sonar"],"Humidity":[20],"IsOpen":[0],"Presence":[0,"Lidar"],"Room Temperature":[17],"Volume":[13],"last_seen":1502401245,"msg_type":1,"source_address":"6789abcdef012345"},"last_seen":1502401245,"source_address":"6789abcdef012345"},{"_id":"596e779ef01f4b1c4b447745","data":{"BatteryLevel":[44],"BatteryVoltage":[2725],"Distance":[4,"Sonar"],"Humidity":[58],"IsOpen":[1],"Presence":[0,"Sonar"],"Room Temperature":[29],"Volume":[52],"last_seen":1502400465,"msg_type":1,"source_address":"9abcdef012345678"},"last_seen":1502400465,"source_address":"9abcdef012345678"},{"_id":"596e7816f01f4b1c4b447747","data":{"BatteryLevel":[70],"BatteryVoltage":[1597],"Distance":[42,"Sonar"],"Humidity":[52],"IsOpen":[0],"Presence":[1,"Accelerometer"],"Room Temperature":[3],"Volume":[78],"last_seen":1502400585,"msg_type":1,"source_address":"bcdef0123456789a"},"last_seen":1502400585,"source_address":"bcdef0123456789a"},{"_id":"596e788ef01f4b1c4b447749","data":{"BatteryLevel":[65],"BatteryVoltage":[1273],"Distance":[5,"Accelerometer"],"Humidity":[33],"IsOpen":[0],"Presence":[0,"Lidar"],"Room Temperature":[6],"Volume":[13],"last_seen":1502400705,"msg_type":1,"source_address":"def0123456789abc"},"last_seen":1502400705,"source_address":"def0123456789abc"},{"_id":"596e78caf01f4b1c4b44774a","data":{"BatteryLevel":[7],"BatteryVoltage":[1554],"Distance":[35,"Lidar"],"Humidity":[2],"IsOpen":[0],"Presence":[1,"Sonar"],"Room Temperature":[16],"Volume":[35],"last_seen":1502400765,"msg_type":1,"source_address":"ef0123456789abcd"},"last_seen":1502400765,"source_address":"ef0123456789abcd"},{"_id":"596e7906f01f4b1c4b44774b","data":{"BatteryLevel":[84],"BatteryVoltage":[422],"Distance":[15,"Lidar"],"Humidity":[58],"IsOpen":[0],"Presence":[0,"Lidar"],"Room Temperature":[13],"Volume":[76],"last_seen":1502400825,"msg_type":1,"source_address":"f0123456789abcde"},"last_seen":1502400825,"source_address":"f0123456789abcde"},{"_id":"596e7517d4f05674e9f773e2","data":{"BatteryLevel":[51],"BatteryVoltage":[2946],"Distance":[18,"Sonar"],"Humidity":[15],"IsOpen":[1],"Presence":[0,"Accelerometer"],"Room Temperature":[22],"Volume":[12],"last_seen":1502739139,"msg_type":1,"source_address":"123456789abcdef0"},"last_seen":1502739139,"source_address":"123456789abcdef0"},{"_id":"596e7672f01f4b1c4b447740","data":{"BatteryLevel":[85],"BatteryVoltage":[62],"Distance":[67,"Accelerometer"],"Humidity":[8],"IsOpen":[0],"Presence":[0,"Accelerometer"],"Room Temperature":[6],"Volume":[34],"last_seen":1502739319,"msg_type":1,"source_address":"456789abcdef0123"},"last_seen":1502739319,"source_address":"456789abcdef0123"},{"_id":"596e76aef01f4b1c4b447741","data":{"BatteryLevel":[59],"BatteryVoltage":[822],"Distance":[67,"Sonar"],"Humidity":[22],"IsOpen":[0],"Presence":[1,"Lidar"],"Room Temperature":[2],"Volume":[22],"last_seen":1502739378,"msg_type":1,"source_address":"56789abcdef01234"},"last_seen":1502739378,"source_address":"56789abcdef01234"},{"_id":"596e7726f01f4b1c4b447743","data":{"BatteryLevel":[40],"BatteryVoltage":[2468],"Distance":[95,"Accelerometer"],"Humidity":[69],"IsOpen":[1],"Presence":[0,"Accelerometer"],"Room Temperature":[13],"Volume":[72],"last_seen":1502401305,"msg_type":1,"source_address":"789abcdef0123456"},"last_seen":1502401305,"source_address":"789abcdef0123456"},{"_id":"596e77daf01f4b1c4b447746","data":{"BatteryLevel":[81],"BatteryVoltage":[1904],"Distance":[63,"Lidar"],"Humidity":[20],"IsOpen":[1],"Presence":[0,"Accelerometer"],"Room Temperature":[22],"Volume":[40],"last_seen":1502400525,"msg_type":1,"source_address":"abcdef0123456789"},"last_seen":1502400525,"source_address":"abcdef0123456789"},{"_id":"596e7762f01f4b1c4b447744","data":{"BatteryLevel":[81],"BatteryVoltage":[1189],"Distance":[48,"Sonar"],"Humidity":[68],"IsOpen":[1],"Presence":[0,"Lidar"],"Room Temperature":[22],"Volume":[72],"last_seen":1502401365,"msg_type":1,"source_address":"89abcdef01234567"},"last_seen":1502401365,"source_address":"89abcdef01234567"},{"_id":"596e7852f01f4b1c4b447748","data":{"BatteryLevel":[67],"BatteryVoltage":[2697],"Distance":[88,"Lidar"],"Humidity":[65],"IsOpen":[0],"Presence":[1,"Lidar"],"Room Temperature":[23],"Volume":[76],"last_seen":1502400645,"msg_type":1,"source_address":"cdef0123456789ab"},"last_seen":1502400645,"source_address":"cdef0123456789ab"},{"_id":"596e7636f01f4b1c4b44773f","data":{"BatteryLevel":[25],"BatteryVoltage":[561],"Distance":[82,"Sonar"],"Humidity":[37],"IsOpen":[0],"Presence":[1,"Sonar"],"Room Temperature":[2],"Volume":[67],"last_seen":1502739259,"msg_type":1,"source_address":"3456789abcdef012"},"last_seen":1502739259,"source_address":"3456789abcdef012"}]}';

                this.getSensorData = function() {

                    return $http({
                        method: "GET",
                        url: URL_LAST
                    }).then(function success(response) {

                        var data = response.data; //angular.fromJson(samplesensordata);
                        var result = data.items;
                        var devices = [];
                        var connectedSensors = [];
                        for (var i = 0; i < result.length; i++) {

                            if (result[i].data && angular.isArray(result[i].data.BatteryLevel)) {
                                result[i].BatteryLevel = result[i].data.BatteryLevel[0];
                                result[i].BatteryVoltage = result[i].data.BatteryVoltage[0];
                                result[i].Distance = result[i].data.Distance[0];
                                result[i].DistanceType = result[i].data.Distance[1];
                                result[i].Humidity = result[i].data.Humidity[0];
                                result[i].IsOpen = result[i].data.IsOpen[0];
                                result[i].Presence = result[i].data.Presence[0];
                                result[i].PresenceType = result[i].data.Presence[1];
                                result[i].RoomTemperature = result[i].data['Room Temperature'][0];
                                result[i].Volume = result[i].data.Volume[0];
                                result[i].last_seen_time = new Date(result[i].data.last_seen)
                                result[i].msg_type = result[i].data.msg_type;
                                var twominearlier = new Date();
                                twominearlier.setMinutes(twominearlier.getMinutes() - 2);
                                result[i].connected = result[i].last_seen_time > twominearlier;
                                result[i].show = true;
                                if (devices.indexOf(result[i].source_address) < 0) {
                                    devices.push(result[i]);
                                }
                                if (connectedSensors.indexOf(result[i].source_address) < 0 && result[i].connected === true) {
                                    connectedSensors.push(result[i]);
                                }
                            }
                        }

                        data.devices = devices;
                        data.connectedSensors = connectedSensors;
                        return data;
                    }, function error(response) {
                        return response;
                    });
                };

            }
        ]);
})();