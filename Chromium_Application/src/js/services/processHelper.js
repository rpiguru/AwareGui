(function() {
    'use strict';

    var ps = require('ps-node');
    var child_process = require('child_process');


    angular.module('aware')
        .service('processHelper', ['$log', 'helpers',
            function($log, helpers) {
                $log.log('helpers called .................');

                this.isRunning = function(search = '', sort = []) {
                    search = search.toLowerCase();

                    let ps = child_process.spawnSync('ps', ['ax', '-o', 'pid,rss,pmem,vsize,uname,pcpu,cmd', ...sort, '--no-headings']);

                    if (ps.stderr.toString())
                        $log.log('List Processes', ps.stderr.toString());
                    var processlist;
                    if (search)
                        processlist = ps.stdout.toString().split('\n')
                        .filter(p => p.toLowerCase().indexOf(search) !== -1)
                        .map(p => procParse(p.trim())).slice(0, -1);
                    else
                        processlist = ps.stdout.toString().split('\n')
                        .map(p => procParse(p.trim())).slice(0, -1);

                    return processlist.length > 0;
                };

                this.getProcess = function(search = '', sort = []) {
                    search = search.toLowerCase();

                    let ps = child_process.spawnSync('ps', ['ax', '-o', 'pid,rss,pmem,vsize,uname,pcpu,cmd', ...sort, '--no-headings']);

                    if (ps.stderr.toString())
                        $log.log('List Processes', ps.stderr.toString());

                    if (search)
                        return ps.stdout.toString().split('\n')
                            .filter(p => p.toLowerCase().indexOf(search) !== -1)
                            .map(p => procParse(p.trim())).slice(0, -1);
                    else
                        return ps.stdout.toString().split('\n')
                            .map(p => procParse(p.trim())).slice(0, -1);
                };

                this.endProcess = function(pid) {
                    if (selectedPid != 0) {
                        var kill = spawnSync('kill', [pid]);

                        if (kill.stderr.toString().indexOf('permitted') !== -1)
                            $log.log('permitted', 'error');
                        else
                            $log.error('Process Kill', kill.stderr.toString());
                    }
                }

                this.startService = function(servicename) {
                    var service = child_process.spawnSync('sudo', ['service', servicename, 'start']);
                }


                this.stopService = function(servicename) {
                    var service = child_process.spawnSync('sudo', ['service', servicename, 'stop']);
                    service = child_process.spawnSync('sudo', ['systemctl', 'stop', servicename]);
                }


                function procParse(data) {
                    let columns = data.split(/\s+/);

                    return {
                        pid: columns[0],
                        rss: helpers.formatBytes(columns[1] * 1024),
                        pmem: columns[2],
                        vsize: helpers.formatBytes(columns[3] * 1024),
                        uname: columns[4],
                        pcpu: columns[5],
                        cmd: columns.slice(6).join(' ')
                    };
                }
            }
        ]);
})();