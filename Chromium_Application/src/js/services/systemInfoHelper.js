(function() {
    'use strict';

    const si = require('systeminformation');
    const getIP = require('external-ip')();
    const child_process = require('child_process');

    var rm = require('electron-remote');

    var sysinfo = rm.requireTaskPool(require.resolve('./js/modules/sysinfo'));

    angular.module('aware')
        .service('systemInfohelper', ['$log', '$http', '$q',
            function($log, $http, $q) {
                $log.log('systemInfohelper called .................');

                var information = {};
                var timeZones = [];
                var timeZone = moment.tz.guess();

                child_process.exec('timedatectl list-timezones', function(error, stdout, stderr) {
                    $log.log(error);
                    $log.log(stdout);
                    if (error == null) {
                        timeZones = stdout.split('\n');
                    }
                });

                // This method will run synchronously, but in a background BrowserWindow process
                // so that your app will not block

                this.getAll = function(callback) {
                    sysinfo().then(function(info) {
                        callback(info);
                    });
                    /*
                    sysinfo.getAllData('','wlp6s0').then(function(data) {
                        callback(data);
                    });*/
                }

                this.getOS = function(callback) {
                    return si.osInfo(o => {
                        information.os = o;
                        callback(information);
                    })
                }

                this.getCpu = function(callback) {
                    return si.cpu(c => {
                        information.cpu = c;
                        callback(information);
                    })
                }

                this.getTime = function(callback) {
                    information.time = si.time();
                    return callback(information);
                }

                this.getCpuCurrentSpeed = function(callback) {
                    return si.cpuCurrentspeed(c => {
                        information.cpuCurrentspeed = c;
                        callback(information);
                    })
                }

                this.getCpuTemperature = function(callback) {
                    return si.cpuTemperature(c => {
                        information.cpuTemperature = c;
                        callback(information);
                    })
                }

                this.getMem = function(callback) {
                    return si.mem(c => {
                        information.mem = c;
                        callback(information);
                    })
                }

                this.getCurrentLoad = function(callback) {
                    return si.currentLoad(c => {
                        information.currentLoad = c;
                        callback(information);
                    })
                }

                this.networkInterfaceDefault = function(callback) {
                    return si.networkInterfaceDefault(c => {
                        information.networkInterfaceDefault = c;
                        callback(information);
                    })
                }

                this.getNetworkInterfaces = function(callback) {
                    return si.networkInterfaces(c => {
                        information.networkInterfaces = c;
                        callback(information);
                    })
                }

                this.getNetworkStats = function(callback) {
                    return si.networkStats('', c => {
                        information.networkStats = c;
                        callback(information);
                    })
                }
                this.getFsSize = function(callback) {
                    return si.fsSize(c => {
                        information.fsSize = c;
                        callback(information);
                    })
                }

                this.getTimeZone = function() {
                    return timeZone;
                }

                this.getTimeZones = function(cb) {
                    return timeZones;
                }

                this.setClock = function(time, cb) {
                    child_process.exec('sudo timedatectl set-time "' + time + '"', cb);
                }

                this.setTimeZone = function(timezone, cb) {
                    child_process.exec('sudo timedatectl set-timezone  "' + timezone + '"', cb);
                }

                this.setHostname = function(hostname, cb) {
                    child_process.exec('sudo hostnamectl set-hostname "' + hostname + '"', cb);
                }

                this.getIp = function(callback) {
                    getIP(function(err, ip) {
                        callback(err, ip);
                    });
                }

                this.getLocation = function() {
                    var deferred = $q.defer();
                    var url = 'http://ipinfo.io/json';
                    $http({
                        method: 'GET',
                        url: url
                    }).then(function(json) {

                        json.data.lat = json.data.loc.split(',')[0];
                        json.data.lan = json.data.loc.split(',')[1];
                        $log.log(json.data);
                        deferred.resolve(json.data);
                    }, function(error) {
                        $log.log(error);
                        deferred.reject(error)
                    });
                    return deferred.promise;

                }



                return this;
            }
        ]);
})();