﻿var loggerString = "";
(function() {
    'use strict';
    var remote = require('electron').remote;          
    var config = {};
    var logger = {};
    try {
       config = remote.getGlobal('config');
       logger = remote.getGlobal('logger');
       console.log(config);
       console.log(logger);
    }catch(err) {

    }
    
    angular.module('aware')
        .run(['$rootScope', '$templateCache', function($rootScope, $templateCache) {

            $templateCache.get('templates/dashboard.html');
            $templateCache.get('templates/menu.html');
            $templateCache.get('templates/aware-service.html');
            $templateCache.get('templates/clock.html');
            $templateCache.get('templates/weather.html');
            $templateCache.get('templates/hub-service.html');
            $templateCache.get('templates/settings.html');
            $templateCache.get('templates/settings-general.html');
            $templateCache.get('templates/settings-ethernet.html');
            $templateCache.get('templates/settings-wifi.html');
            $templateCache.get('templates/settings-time.html');

        }])
        .config(['NotificationProvider', '$stateProvider', '$urlRouterProvider', '$httpProvider', '$mdKeyboardProvider','$provide',
            function(NotificationProvider, $stateProvider, $urlRouterProvider, $httpProvider, $mdKeyboardProvider, $provide) {
                NotificationProvider.setOptions({
                    delay: 3000,
                    startTop: 20,
                    startRight: 10,
                    verticalSpacing: 20,
                    horizontalSpacing: 20,
                    positionX: 'center',
                    positionY: 'bottom'
                });

                $mdKeyboardProvider.defaultLayout('English');

                $provide.decorator('$log', ['$delegate', function ($delegate)
                {
                        var originals = {};
                        var methods = ['log', 'info' , 'debug' , 'warn' , 'error'];

                        angular.forEach(methods , function(method)
                        {
                            originals[method] = $delegate[method];
                            $delegate[method] = function()
                            {
                                //if (config.debug) {
                                    //var args = [].slice.call(  || 'localhost'arguments);
                                    //var timestamp = new Date().toString();
                                    //rgs[0] = [timestamp.substring(4 , 24), ': ', args[0]].join('');
                                    originals[method].apply(null , arguments);
                                    console.log(arguments);
                                    loggerString  = arguments.length >0 && typeof arguments[0] == 'string' ? arguments[0] : JSON.stringify(arguments[0]);
                                    if (method == 'log'){
                                        logger.debug(loggerString);    
                                    } else if (method in logger && typeof logger[method] === "function") {
                                        logger[method](loggerString);
                                    } else {
                                        logger.debug(loggerString);    
                                    }   
                                //}
                            };
                       });

                       return $delegate;
                }]);
            }
        ])
        .factory('$exceptionHandler', ['$log', function($log) {
            return function exceptionHandler(exception, cause) {
              logger.error('Exception occured');
              logger.error(exception, cause);
              return exception;
            };
        }])
        .constant('AWARE_SERVICE', config.aware_service || 'awareservice')
        .constant('WIFI_INTERFACE',  config.wifi_interface || 'wlan0') //wlp6s0
        .constant('ETHERNET_INTERFACE',  config.ethernet_interface || 'eth0')
        .constant('MONGO_HOST', config.mongo_host || '127.0.0.1')
        .constant('MONGO_PORT',  config.mongo_port || '27017') //wlp6s0
        .constant('MONGO_COLLECTION',  config.mongo_collection || 'awaregui')
        .constant('URL_LAST',  config.url_last || 'http://127.0.0.1:6001/recent_sensor_data/');

})();