const electron = require('electron')
// Module to control application life.
const app = electron.app;
// Module to create native browser window.
const BrowserWindow = electron.BrowserWindow;

const path = require('path');
const url = require('url');
require('electron-debug')({enabled : true});
const log4js = require('log4js');
const shell = require('shelljs');

// Keep a global reference of the window object, if you don't, the window will
// be closed automatically when the JavaScript object is garbage collected.
let mainWindow, splashScreen, windowParams;

global.config = {};
try{

  var fs = require('fs');
  var fpath = path.join(__dirname, '/awaregui.config');
  var contents = fs.readFileSync(fpath, 'utf8');
  global.config =JSON.parse(contents);    


  shell.mkdir('-p', config.logfile.substring(0, config.logfile.lastIndexOf("/")));


  log4js.configure({
    appenders: {
      everything: { type: 'file', filename: config.logfile, maxLogSize: config.logsize, backups: 10, compress: false }
    },
    categories: {
      default: { appenders: ['everything'], level: config.loglevel}
    }
  });

  global.logger = log4js.getLogger();

} catch(err){

}


function createWindow () {

  // Create the browser window.
  mainWindow = new BrowserWindow(windowParams)

  // and load the index.html of the app.
  mainWindow.loadURL(url.format({
    pathname: path.join(__dirname, '/src/index.html'),
    protocol: 'file:',
    slashes: true
  }))

  // Open the DevTools.
  //mainWindow.webContents.openDevTools()

  // Emitted when the window is closed.
  mainWindow.on('closed', function () {
    // Dereference the window object, usually you would store windows
    // in an array if your app supports multi windows, this is the time
    // when you should delete the corresponding element.
    mainWindow = null
  })

  // Show window when page is ready

 mainWindow.webContents.on('did-finish-load', () => {
      mainWindow.show();

      if (splashScreen) {
          splashScreen.close();
      }
  });
}


function createsplashScreen() {
    splashScreen = new BrowserWindow(Object.assign(windowParams, {parent: mainWindow,backgroundColor: '#000'}));
    splashScreen.loadURL(url.format({
      pathname: path.join(__dirname, '/src/splashscreen.html'),
      protocol: 'file:',
      slashes: true
    }));
    splashScreen.on('closed', () => splashScreen = null);
    splashScreen.webContents.on('did-finish-load', () => {
        splashScreen.show();
    });
}

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.on('ready', () => {

  var screen = electron.screen;
  var size = screen.getPrimaryDisplay().size;

   windowParams =  { 
    //width:1280, height:720, frame: false, 
     fullscreen: true,
     x: 0,     y: 0,     width: size.width,     height: size.height,
     // Don't show the window until it ready, this prevents any white flickering
    show: false,
    // Don't allow the window to be resized.
    //resizable: false,
    //resizable: false, movable: false, minimizable: false, maximizable: true, closable: false,
    frame: false, kiosk: true, title: 'AwareGUI', icon: path.join(__dirname, '/src/images/aware_logo_small.png')
  };
    createsplashScreen();
    createWindow();
});

// Quit when all windows are closed.
app.on('window-all-closed', function () {
  // On OS X it is common for applications and their menu bar
  // to stay active until the user quits explicitly with Cmd + Q
  if (process.platform !== 'darwin') {
    app.quit()
  }
})

app.on('activate', function () {
  // On OS X it's common to re-create a window in the app when the
  // dock icon is clicked and there are no other windows open.
  if (mainWindow === null) {
    createWindow()
  }
})

// In this file you can include the rest of your app's specific main process
// code. You can also put them in separate files and require them here.
