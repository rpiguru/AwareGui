
# Aware Gui Application


## Installation

Check **ReleaseMgmt_UnitTests** repository for detailed instruction.


## Test

There are some unit test modules in `Application/tests` directory.

    cd Application/tests
    sudo python3 -m unittest <module>.py
